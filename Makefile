# Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
#
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#    Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

MPM_ROOT = $(shell pwd)
MPM_SRC = $(MPM_ROOT)/src
MPM_TEST = $(MPM_ROOT)/test
export MPM_LIB = $(MPM_ROOT)/lib
export MPM_BIN ?= $(MPM_ROOT)/bin
export MPM_INC = $(MPM_ROOT)/include
export MPM_LIB_C66X = $(MPM_ROOT)/lib_c66x

ifeq ($(BUILD_LOCAL),true)
ifdef LOCAL_SYSROOT
SYSROOT_OPT = --sysroot=$(LOCAL_SYSROOT)
STAGING_KERNEL_DIR ?= $(LOCAL_SYSROOT)/usr/src/kernel
endif
ARCH = arm
CROSS_COMPILE ?= arm-linux-gnueabihf-
ARFLAGS = crus
LDFLAGS ?= -Wl,--hash-style=gnu
CFLAGS ?= -march=armv7-a -marm -mthumb-interwork -mfloat-abi=hard -mfpu=neon -mtune=cortex-a15 -D __ARMv7 -D DEVICE_K2H
EXTRA_CFLAGS = -g -ggdb2 -D_GNU_SOURCE -DARCH_$(ARCH)
CC = $(CROSS_COMPILE)gcc $(SYSROOT_OPT) $(CFLAGS) $(EXTRA_CFLAGS)
CC += -I$(STAGING_KERNEL_DIR)/include
AR = $(CROSS_COMPILE)ar
endif

TARGET_PLATFORM = $(ARCH)

export TARGET_PLATFORM
export LDFLAGS
export ARFLAGS
export CC
export AR
export LD

MAKEFLAGS += --no-print-directory

export MPM_DAEMON = $(MPM_SRC)/daemon
export MPM_COMMON = $(MPM_SRC)/common
export MPM_CLIENT  = $(MPM_SRC)/client
export MPM_MAILBOX  = $(MPM_SRC)/mailbox
export MPM_SYNC  = $(MPM_SRC)/sync

export MPMDAEMON_BIN = $(MPM_BIN)/mpmsrv
export MPMCLIENT_BIN = $(MPM_BIN)/mpmcl
export MPMCLIENT_LIB = libmpmclient.a
export MPMCLIENT_LIB_SO = libmpmclient.so
export MPMCLIENT_LIB_OPT = -lmpmclient
export MPM_MAILBOX_LIB = libmpmmailbox.a
export MPM_MAILBOX_LIB_SO = libmpmmailbox.so
export MPM_MAILBOX_LIB_C66X = mpmmailbox.ae66
export MPM_MAILBOX_LIB_OPT = -lmpmmailbox
export MPM_SYNC_LIB = libmpmsync.a
export MPM_SYNC_LIB_SO = libmpmsync.so
export MPM_SYNC_LIB_C66X = mpmsync.ae66
export MPM_SYNC_LIB_OPT = -lmpmsync

export MPM_FILETESTDEMO = $(MPM_TEST)/filetestdemo
export MPM_SYNC_TEST = $(MPM_TEST)/sync_test
export MPM_DSP_BOOT = $(MPM_TEST)/k2x_dsp_boot

.PHONY: all build clean distclean daemon jtest mkdir mailbox sync test filetestdemo sync_test

all: mkdir daemon clientlib clientlib_so client mailbox mailbox_so sync sync_so

test: all filetestdemo sync_test

c66x: mkdir_c66x mailbox_c66x sync_c66x k2x_dsp_boot

test_c66x: c66x filetestdemo_c66x sync_test_c66x k2x_dsp_boot

mkdir:
	mkdir -p $(MPM_BIN)
	mkdir -p $(MPM_LIB)

mkdir_c66x:
	mkdir -p $(MPM_LIB_C66X)

daemon:
	$(MAKE) -C $(MPM_DAEMON) $(MPMDAEMON_BIN)

clientlib:
	$(MAKE) -C $(MPM_CLIENT) $(MPM_LIB)/$(MPMCLIENT_LIB)

clientlib_so:
	$(MAKE) -C $(MPM_CLIENT) $(MPM_LIB)/$(MPMCLIENT_LIB_SO)

client: clientlib clientlib_so
	$(MAKE) -C $(MPM_CLIENT) $(MPMCLIENT_BIN)

mailbox: mailbox_so
	$(MAKE) -C $(MPM_MAILBOX) $(MPM_LIB)/$(MPM_MAILBOX_LIB)

mailbox_so:
	$(MAKE) -C $(MPM_MAILBOX) $(MPM_LIB)/$(MPM_MAILBOX_LIB_SO)

mailbox_c66x:
	$(MAKE) -C $(MPM_MAILBOX) $(MPM_LIB_C66X)/$(MPM_MAILBOX_LIB_C66X)

sync: sync_so
	$(MAKE) -C $(MPM_SYNC) $(MPM_LIB)/$(MPM_SYNC_LIB)

sync_so:
	$(MAKE) -C $(MPM_SYNC) $(MPM_LIB)/$(MPM_SYNC_LIB_SO)

sync_c66x:
	$(MAKE) -C $(MPM_SYNC) $(MPM_LIB_C66X)/$(MPM_SYNC_LIB_C66X)

filetestdemo:
	$(MAKE) -C $(MPM_FILETESTDEMO) host

filetestdemo_c66x:
	$(MAKE) -C $(MPM_FILETESTDEMO) dsp

sync_test: sync_so
	$(MAKE) -C $(MPM_SYNC_TEST) host

sync_test_c66x: sync_c66x
	$(MAKE) -C $(MPM_SYNC_TEST) dsp

k2x_dsp_boot:
	$(MAKE) -C $(MPM_DSP_BOOT)
clean:
	$(MAKE) -C $(MPM_DAEMON) clean
	$(MAKE) -C $(MPM_COMMON) clean
	$(MAKE) -C $(MPM_CLIENT) clean
	$(MAKE) -C $(MPM_MAILBOX) clean
	$(MAKE) -C $(MPM_SYNC) clean
	$(MAKE) -C $(MPM_FILETESTDEMO) clean
	$(MAKE) -C $(MPM_SYNC_TEST) clean
	$(MAKE) -C $(MPM_DSP_BOOT) clean
	rm -rf $(MPM_BIN) $(MPM_LIB) $(MPM_LIB_C66X)

jtest:
	$(MAKE) -C $(MPM_DAEMON) jtest

distclean: clean
	$(MAKE) -C $(MPM_DAEMON) distclean
