/*
 * Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#define UART0_ADDR	(0x02530c00)

struct uart_regs {
	unsigned int rbr;		/* 0 */
	unsigned int ier;		/* 1 */
	unsigned int fcr;		/* 2 */
	unsigned int lcr;		/* 3 */
	unsigned int mcr;		/* 4 */
	unsigned int lsr;		/* 5 */
	unsigned int msr;		/* 6 */
	unsigned int spr;		/* 7 */
};


void skern_putc(unsigned char c)
{
    struct uart_regs *uart0 = (struct uart_regs *)UART0_ADDR;
    while((uart0->lsr & 0x20) == 0);
	uart0->rbr = c;
}

void skern_puts(char *str)
{
	for (; *str != '\0'; str++ )
		skern_putc(*str);
}

static char dig[] = "0123456789abcdef";
void skern_putbyte(unsigned char b)
{
	skern_putc(dig[(b >> 4) & 0xf]);
	skern_putc(dig[b & 0xf]);
}

void skern_putui(unsigned int ul)
{
	skern_putbyte((ul >> 24) & 0xff);
	skern_putbyte((ul >> 16) & 0xff);
	skern_putbyte((ul >>  8) & 0xff);
	skern_putbyte( ul        & 0xff);
}

void skern_putsui(char *s, unsigned int ui, int crlf)
{
	skern_puts(s);
	skern_putui(ui);
	if (crlf)
		skern_puts("\r\n");
}
