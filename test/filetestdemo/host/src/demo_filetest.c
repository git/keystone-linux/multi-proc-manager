/*
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
/**
 *  FILE:  demo_filetest.c
 *         This software demonstrates the PCIe APIs using a simple file test.
 *         It reads data from input file in chunks defined by 
 *         demo_payload_size.  Then sends the data to DSP 
 *         through PCIE using pcie driver and mailbox APIs.
 *         DSP in turn copies the date into the output buffer and sends message
 *         back to the Host.  The received data is written to the output file.
 *         This supports 3 different mechanisms to pass data from and to DSP.
 *         a) DMA transfer to DSP DDR 
 *         b) Mapping of Host buffer to DSP memory directly.
 *         c) Direct mem copy into DDR through pcie_drv_read & pcie_drv_write APIs.
 *         d) Statically mapped Huge buffer to DSP (This test uses only transfers
 *            per DSP. not per core)
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>

#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>

#include <ti/cmem.h>

/* SDK Module header files */
#include <mpm_transport.h>
#include "mpm_mailbox.h"
#include "bufmgr.h"

/* Demo header files */
#include "demo.h"
#include "filetestdemo.h"


#define MAILBOX_MAKE_HOST_NODE_ID(a) (0x8000 | a)


/******************************************************************************/
/* Stores rx and tx mail ids for all the cores                                */
void  *rx_mailbox_handle[DEMO_FILETEST_MAX_NUM_CORES],
        *tx_mailbox_handle[DEMO_FILETEST_MAX_NUM_CORES];

mpm_transport_h mpm_transport_handle[DEMO_FILETEST_MAX_NUM_CORES];
void *rx_mailbox_map_addr[DEMO_FILETEST_MAX_NUM_CORES],
     *tx_mailbox_map_addr[DEMO_FILETEST_MAX_NUM_CORES];

bufmgrDesc_t *input_buf_map;
bufmgrDesc_t *output_buf_map;

bufmgrDesc_t  rx_mailbox_mapping[DEMO_FILETEST_MAX_NUM_CHIPS];
bufmgrDesc_t  tx_mailbox_mapping[DEMO_FILETEST_MAX_NUM_CHIPS];
bufmgrDesc_t  input_buf_pool_mapping[DEMO_FILETEST_MAX_NUM_CHIPS];
bufmgrDesc_t  output_buf_pool_mapping[DEMO_FILETEST_MAX_NUM_CHIPS];
bufmgrDesc_t  cmem_buf_desc[DEMO_FILETEST_MAX_NUM_CHIPS];

/******************************************************************************/
/* Temporary buffer for input                                                 */
int8_t inputBuf[DEMO_FILETEST_MAX_PAYLOAD_BUFFER_SIZE];

/******************************************************************************/
/* Temporary buffer for output                                                */
int8_t outputBuf[DEMO_FILETEST_MAX_PAYLOAD_BUFFER_SIZE];

/* Buffer pool handles for Input and Output buffers */
void *c66InputBufPool[DEMO_FILETEST_MAX_NUM_CHIPS];
void *c66OutputBufPool[DEMO_FILETEST_MAX_NUM_CHIPS];

uint32_t demo_payload_size=DEMO_FILETEST_MAX_PAYLOAD_BUFFER_SIZE;
uint32_t num_of_dsps;

/* mailbox locations */
uint32_t dsp2hostmailbox, host2dspmailbox;

/**
 *  Variables used for profiling on Desktop
 */
static int64_t total_write_time = 0;
static int32_t total_write_time_cnt = 0;
static int64_t total_read_time = 0;
static int32_t total_read_time_cnt = 0;
static int64_t total_dma_write_time = 0;
static int32_t total_dma_write_time_cnt = 0;
static int64_t total_dma_read_time = 0;
static int32_t total_dma_read_time_cnt = 0;
static int64_t memcpy_read_time=0, memcpy_write_time=0;
static int64_t total_mailbox_write_time=0, mailbox_write_min_time=0x7fffffffffffffffll, mailbox_write_max_time=0;
static int32_t total_mailbox_write_cnt=0, total_mailbox_read_cnt=0;
static int64_t total_mailbox_read_time=0, mailbox_read_min_time=0x7fffffffffffffffll, mailbox_read_max_time=0;

static int64_t file_read_time= 0, file_write_time=0;
static int32_t file_read_time_cnt=0, file_write_time_cnt=0;
/**
 *  @brief Function clock_diff returns difference between two timeval
 *  @param[in]     start            start time
 *  @param[in]     end              end time
 *  @retval        deltaTime
 */
static int64_t clock_diff (struct timespec *start, struct timespec *end)
{
    return (end->tv_sec - start->tv_sec) * 1000000000 
             + end->tv_nsec - start->tv_nsec;
}


typedef struct {
  int fd;
  int64_t offset;
} demo_file_t;

void *demo_fopen(char *fname, char *opt)
{
#if 1
  return((void *)fopen(fname,opt));
#else
  demo_file_t *file_ptr = malloc(sizeof(demo_file_t));

  if ( file_ptr != NULL ) {

    memset(file_ptr,0,sizeof(demo_file_t));

    file_ptr->fd = open(fname,O_RDWR|O_SYNC);
    if ( file_ptr->fd < 0 ) {
      free(file_ptr);
      file_ptr = NULL;
    }
  }
  return ((void *)file_ptr);
#endif
}

int32_t demo_fread( uint8_t *buf, int32_t size, int32_t count, void *fp)
{
#if 1
  return((size_t)fread((void *)buf,(size_t)size,(size_t)count,(FILE *)fp));
#else
  demo_file_t *file_ptr = (demo_file_t *)fp;
  uint8_t *va;

  va = mmap(NULL, size*count, PROT_READ, MAP_SHARED, file_ptr->fd, file_ptr->offset);
  if ( va == (void *)-1 ) {
    perror("demo_fread() @ mmap");
    getchar();
  }

  memcpy( buf, va, size*count);

  if ( munmap( va, size*count) ) {
    perror("demo_fread() @ munmap");
    getchar();
  }

  file_ptr->offset += size*count;

  return (size*count);
#endif
}

int32_t demo_fwrite( uint8_t *buf, int32_t size, int32_t count, void *fp)
{
#if 1
  return((size_t)fwrite((void *)buf,(size_t)size,(size_t)count,(FILE *)fp));
#else
  demo_file_t *file_ptr = (demo_file_t *)fp;
  uint8_t *va;

  va = mmap(NULL, size*count, PROT_WRITE, MAP_SHARED, file_ptr->fd, file_ptr->offset);
  if ( va == (void *)-1 ) {
    perror("demo_fread() @ mmap");
    getchar();
  }

  memcpy( va, buf, size*count);

  if ( munmap( va, size*count) ) {
    perror("demo_fread() @ munmap");
    getchar();
  }

  file_ptr->offset += size*count;

  return (size*count);
#endif
}


/**
 *  @brief Function demoK2H_assert Routine to trap exceptions
 *  @param[in]         statement    Boolean statement
 *  @param[in]         node_id      Node Id
 *  @param[in]         errorMsg     Error string
 *  @retval        None         
 *  @pre  
 *  @post 
 */
void demoK2H_assert(int32_t statement, int32_t node_id, const int8_t *errorMsg)
{
  volatile int32_t dbg_halt = 1;

  if(!statement) {
    printf("%s (%d)\n",errorMsg, node_id);
    while(dbg_halt);
  }
}


/**
 *  @brief Function demoK2H_initDspBufs : Initialise buffer manager 
 *                  for all chips 
 *  @param             none
 *  @retval            none       
 *  @pre  
 *  @post 
 */

void demoK2H_initDspBufs(void)
{
  int32_t i;
  int32_t ret_val;

  /* Allocate buffer pools from DSP memory for Input and Output */
  for ( i = 0; i < num_of_dsps; i++)
  {
    ret_val = bufmgrCreate2(
      &c66InputBufPool[i], 
      (void *) DEMO_FILETEST_INPUT_POOL_BASEADDR, 
      DEMO_FILETEST_POOL_SIZE, 
      demo_payload_size, 
      0);
    demoK2H_assert( (ret_val == 0), i,
        "ERROR: Buffer Create2 : input pool) ");
    
    ret_val = bufmgrCreate2(
      &c66OutputBufPool[i], 
      (void *) DEMO_FILETEST_OUTPUT_POOL_BASEADDR, 
      DEMO_FILETEST_POOL_SIZE, 
      demo_payload_size, 
      0);
    demoK2H_assert( (ret_val == 0), i,
        "ERROR: Buffer Create2 : output pool) ");
  }
}

void demoK2H_initDspBufsMapped(void)
{
  bufmgrDesc_t *input_descs, *output_descs;
  mpm_transport_mmap_t        mpm_transport_mmap_cfg;
  uint64_t      num_descs;
  int32_t i, j, ret_val;

  num_descs = DEMO_FILETEST_POOL_SIZE/demo_payload_size;

  input_descs   = (bufmgrDesc_t *)malloc(num_descs * sizeof(bufmgrDesc_t));
  output_descs  = (bufmgrDesc_t *)malloc(num_descs * sizeof(bufmgrDesc_t));

  input_buf_map   = (bufmgrDesc_t *)malloc(num_of_dsps * num_descs * sizeof(bufmgrDesc_t));
  output_buf_map  = (bufmgrDesc_t *)malloc(num_of_dsps * num_descs * sizeof(bufmgrDesc_t));

  mpm_transport_mmap_cfg.mmap_prot = (PROT_READ|PROT_WRITE);
  mpm_transport_mmap_cfg.mmap_flags = MAP_SHARED;

  for ( i = 0; i < num_of_dsps; i++)
  {
    input_buf_pool_mapping[i].physAddr  = (uint64_t)DEMO_FILETEST_INPUT_POOL_BASEADDR;
    input_buf_pool_mapping[i].length    = DEMO_FILETEST_POOL_SIZE;
    input_buf_pool_mapping[i].userAddr  = (uint8_t *)mpm_transport_mmap(
                                                       mpm_transport_handle[i*DEMO_CONFIG_CORES_PER_CHIP],
                                                       (uint32_t)input_buf_pool_mapping[i].physAddr,
                                                       input_buf_pool_mapping[i].length,
                                                       &mpm_transport_mmap_cfg);

    output_buf_pool_mapping[i].physAddr = (uint64_t)DEMO_FILETEST_OUTPUT_POOL_BASEADDR;
    output_buf_pool_mapping[i].length   = DEMO_FILETEST_POOL_SIZE;
    output_buf_pool_mapping[i].userAddr = (uint8_t *)mpm_transport_mmap(
                                                       mpm_transport_handle[i*DEMO_CONFIG_CORES_PER_CHIP],
                                                       (uint32_t)output_buf_pool_mapping[i].physAddr,
                                                       output_buf_pool_mapping[i].length,
                                                       &mpm_transport_mmap_cfg);

    for ( j = 0; j < num_descs; j++)
    {
      input_descs[j].physAddr   = input_buf_pool_mapping[i].physAddr + j*demo_payload_size;
      input_descs[j].userAddr   = input_buf_pool_mapping[i].userAddr + j*demo_payload_size;
      input_descs[j].length     = demo_payload_size;

      printf("Input  Buffer : 0x%08X mapped to %p for length %d\n", (uint32_t)input_descs[j].physAddr, input_descs[j].userAddr, input_descs[j].length);

      output_descs[j].physAddr  = output_buf_pool_mapping[i].physAddr + j*demo_payload_size;
      output_descs[j].userAddr  = output_buf_pool_mapping[i].userAddr + j*demo_payload_size;
      output_descs[j].length    = demo_payload_size;

      printf("Output Buffer : 0x%08X mapped to %p for length %d\n", (uint32_t)output_descs[j].physAddr, output_descs[j].userAddr, output_descs[j].length);
    }
    ret_val = bufmgrCreate( 
      &c66InputBufPool[i],
      num_descs,
      input_descs);
    demoK2H_assert( (ret_val == 0), i, "ERROR: Buffer Create : input pool ");

    ret_val = bufmgrCreate( 
      &c66OutputBufPool[i],
      num_descs,
      output_descs);
    demoK2H_assert( (ret_val == 0), i, "ERROR: Buffer Create : output pool ");

    memcpy( &input_buf_map[i*num_descs], input_descs, num_descs * sizeof(bufmgrDesc_t));
    memcpy( &output_buf_map[i*num_descs], output_descs, num_descs * sizeof(bufmgrDesc_t));
  }
}


void demoK2H_initCmemBufs(int cached)
{
  bufmgrDesc_t     *input_desc, *output_desc;
  CMEM_AllocParams  alloc_params;
  int32_t           j, num_input_bufs = 0, num_output_bufs = 0,  max_num_descs, ret_val;
  uint64_t          phys_addr;

  max_num_descs = DEMO_CONFIG_CORES_PER_CHIP * 2;

  input_desc   = (bufmgrDesc_t *)malloc(max_num_descs * sizeof(bufmgrDesc_t));
  output_desc  = (bufmgrDesc_t *)malloc(max_num_descs * sizeof(bufmgrDesc_t));

  input_buf_map   = (bufmgrDesc_t *)malloc(num_of_dsps * max_num_descs * sizeof(bufmgrDesc_t));
  output_buf_map  = (bufmgrDesc_t *)malloc(num_of_dsps * max_num_descs * sizeof(bufmgrDesc_t));

  alloc_params.type = CMEM_POOL;
  alloc_params.alignment = 0;

  if ( cached == DEMO_BUFS_CACHED )
    alloc_params.flags = CMEM_CACHED;
  else
    alloc_params.flags = CMEM_NONCACHED;

  demoK2H_assert( (CMEM_init() == 0), 0, "ERROR: CMEM_init() ");

  cmem_buf_desc[0].physAddr = CMEM_allocPhys(2 * max_num_descs * demo_payload_size, &alloc_params);
  demoK2H_assert( (cmem_buf_desc[0].physAddr != 0), 0, "ERROR: CMEM_allocPhys() ");

  cmem_buf_desc[0].length = 2*max_num_descs*demo_payload_size;

  cmem_buf_desc[0].userAddr = CMEM_map( cmem_buf_desc[0].physAddr, cmem_buf_desc[0].length);
  demoK2H_assert( (cmem_buf_desc[0].userAddr != NULL), 0, "ERROR: CMEM_map() ");

  for ( j = 0; j < max_num_descs; j++)
  {
    input_desc[j].length   = demo_payload_size;
    input_desc[j].userAddr = (uint8_t *)((uint32_t)cmem_buf_desc[0].userAddr + (2*j+0)*demo_payload_size);
    input_desc[j].physAddr = (uint64_t)((cmem_buf_desc[0].physAddr + (2*j+0)*demo_payload_size) - 0x800000000LL + 0x80000000);

    num_input_bufs++;
    printf("Input  Buffer : 0x%llX (0x%08X) mapped to %p for length %d\n", (cmem_buf_desc[0].physAddr + (2*j+0)*demo_payload_size), (uint32_t)input_desc[j].physAddr, input_desc[j].userAddr, input_desc[j].length);

    output_desc[j].length   = demo_payload_size;
    output_desc[j].userAddr = (uint8_t *)((uint32_t)cmem_buf_desc[0].userAddr + (2*j+1)*demo_payload_size);
    output_desc[j].physAddr = (uint64_t)((cmem_buf_desc[0].physAddr + (2*j+1)*demo_payload_size) - 0x800000000LL + 0x80000000);

    num_output_bufs++;
    printf("Output Buffer : 0x%llX (0x%08X) mapped to %p for length %d\n", (cmem_buf_desc[0].physAddr + (2*j+1)*demo_payload_size), (uint32_t)output_desc[j].physAddr, output_desc[j].userAddr, output_desc[j].length);
  }
  
  demoK2H_assert( (j == max_num_descs), 0, "ERROR: Could not allocate required number of buffers ");

  ret_val = bufmgrCreate( 
    &c66InputBufPool[0],
    num_input_bufs,
    input_desc);
  demoK2H_assert( (ret_val == 0), 0, "ERROR: Buffer Create : input pool ");

  ret_val = bufmgrCreate( 
    &c66OutputBufPool[0],
    num_output_bufs,
    output_desc);
  demoK2H_assert( (ret_val == 0), 0, "ERROR: Buffer Create : output pool ");

  memcpy( &input_buf_map[0], input_desc, num_input_bufs * sizeof(bufmgrDesc_t));
  memcpy( &output_buf_map[0], output_desc, num_output_bufs * sizeof(bufmgrDesc_t));
}


/**
 *  @brief Function demoK2H_deletePools Delete allocated pools
 *  @param          None
 *  @retval         None
 */
void demoK2H_deletePools(int32_t demo_mode)
{
  int32_t i;


  printf("deletePools() : num_dsps = %d, demo_mode = %d\n", num_of_dsps, demo_mode);
  for ( i = 0; i < num_of_dsps; i++) {
    switch (demo_mode)
    {
      case DEMO_MODE_CMEM_CACHED_TEST:
      case DEMO_MODE_CMEM_UNCACHED_TEST:
      {
         CMEM_AllocParams alloc_params;
         bufmgrDesc_t buf_desc;

         alloc_params.type = CMEM_POOL;  // Other parameters are "don't cares".

         while( bufmgrAlloc(c66InputBufPool[i], 1, &buf_desc) == 0 )
         {
           printf("Free Input  Buffer : 0x%08X mapped to %p for length %d\n", (uint32_t)buf_desc.physAddr, buf_desc.userAddr, buf_desc.length);
         }
         while( bufmgrAlloc(c66OutputBufPool[i], 1, &buf_desc) == 0 )
         {
           printf("Free Output Buffer : 0x%08X mapped to %p for length %d\n", (uint32_t)buf_desc.physAddr, buf_desc.userAddr, buf_desc.length);
         }
         demoK2H_assert( (CMEM_unmap(cmem_buf_desc[0].userAddr, cmem_buf_desc[0].length) == 0), 0, "CMEM_unmap() returned error ");
         demoK2H_assert( (CMEM_freePhys(cmem_buf_desc[0].physAddr, &alloc_params) == 0), 0, "ERROR: CMEM_freePhys() ");

         CMEM_exit();
         break;
      }
    }

    bufmgrDelete(&c66InputBufPool[i]);
    bufmgrDelete(&c66OutputBufPool[i]);
  }
}

/**
 *  @brief Function demoK2H_Config : Configure and initialise 
 *                  required components for demo
 *  @param[in]         nodeBits   Node bit map
 *  @retval                       0: success ; -1: fail       
 *  @pre  
 *  @post 
 */

uint32_t demoK2H_Config(uint32_t nodeBits)
{
  int32_t node_id, ret_val;
  uint32_t size, trans_id = 0xC0DE0000, size_rx, trans_id_rx;
  uint32_t core_id, dsp_id;
  mpm_transport_open_t mpm_transport_open_cfg;
  mpm_transport_mmap_t mpm_transport_mmap_cfg;
  mpm_mailbox_config_t mpm_mailbox_config;
  uint32_t mailboxallocsize;
  char     node_name[128];

  /* Initialize mpm_transport configurations */
  mpm_transport_open_cfg.open_mode = (O_SYNC|O_RDWR);

  mpm_transport_mmap_cfg.mmap_prot = (PROT_READ|PROT_WRITE);
  mpm_transport_mmap_cfg.mmap_flags = MAP_SHARED;

  /*** Create mailboxes ***/
  for ( node_id = 0; node_id < num_of_dsps*DEMO_CONFIG_CORES_PER_CHIP; node_id++)
  {
    if( nodeBits & (((uint32_t) 1) << (node_id%32)) )
    {
      core_id = node_id%(DEMO_CONFIG_CORES_PER_CHIP);
      dsp_id = node_id/DEMO_CONFIG_CORES_PER_CHIP;

      /* Open mpm_transport handle */
      sprintf(node_name,"dsp%d",node_id);
      mpm_transport_handle[node_id] = mpm_transport_open(node_name,&mpm_transport_open_cfg);
      demoK2H_assert( (mpm_transport_handle[node_id] != NULL), node_id, 
        "ERROR: mpm_transport_open() ");
      printf("Opened mpm_transport handle (%p) for node id %d\n", mpm_transport_handle[node_id],node_id);
#if 1
      if ( core_id == 0 )
      {
        tx_mailbox_mapping[dsp_id].physAddr  = (uint64_t)host2dspmailbox;
        tx_mailbox_mapping[dsp_id].length    = DEMO_PER_MAILBOX_MEM_SIZE * DEMO_CONFIG_CORES_PER_CHIP;
        tx_mailbox_mapping[dsp_id].userAddr  = (uint8_t *)mpm_transport_mmap(
                                                            mpm_transport_handle[node_id], 
                                                            (uint32_t)tx_mailbox_mapping[dsp_id].physAddr,
                                                            tx_mailbox_mapping[dsp_id].length,
                                                            &mpm_transport_mmap_cfg);

        demoK2H_assert( (tx_mailbox_mapping[dsp_id].userAddr != (void *)-1), node_id,
          "ERROR: mpm_transport_mmap() for tx mailboxes");
        printf("Mapped TX mailboxes to %p\n", tx_mailbox_mapping[dsp_id].userAddr);

        rx_mailbox_mapping[dsp_id].physAddr  = (uint64_t)dsp2hostmailbox;
        rx_mailbox_mapping[dsp_id].length    = DEMO_PER_MAILBOX_MEM_SIZE * DEMO_CONFIG_CORES_PER_CHIP;
        rx_mailbox_mapping[dsp_id].userAddr  = (uint8_t *)mpm_transport_mmap(
                                                            mpm_transport_handle[node_id], 
                                                            (uint32_t)rx_mailbox_mapping[dsp_id].physAddr,
                                                            rx_mailbox_mapping[dsp_id].length,
                                                            &mpm_transport_mmap_cfg);

        demoK2H_assert( (rx_mailbox_mapping[dsp_id].userAddr != (void *)-1), node_id,
          "ERROR: mpm_transport_mmap() for rx mailbox");
        printf("Mapped RX mailbox to %p\n", rx_mailbox_mapping[dsp_id].userAddr);
      }
#endif

#ifdef DEMO_VERBOSE
      printf("Creating Mailbox (%x --> %x)\n", MAILBOX_MAKE_HOST_NODE_ID(0),node_id);
#endif
      mailboxallocsize = mpm_mailbox_get_alloc_size();
      tx_mailbox_handle[node_id] = (void *)malloc(mailboxallocsize);
      demoK2H_assert( (tx_mailbox_handle[node_id] != NULL), node_id, "ERROR: malloc Tx handle ");

      /* map memory for mailboxes */
      mpm_mailbox_config.mem_size = DEMO_PER_MAILBOX_MEM_SIZE;
      mpm_mailbox_config.max_payload_size = DEMO_MAILBOX_MAX_PAYLOAD_SIZE;
#if 1
      mpm_mailbox_config.mem_start_addr = (uint32_t)(tx_mailbox_mapping[dsp_id].userAddr + core_id*DEMO_PER_MAILBOX_MEM_SIZE);

      ret_val = mpm_mailbox_create(tx_mailbox_handle[node_id], NULL,
        MPM_MAILBOX_MEMORY_LOCATION_LOCAL, MPM_MAILBOX_DIRECTION_SEND, &mpm_mailbox_config);
#else
      mpm_mailbox_config.mem_start_addr = (uint32_t)(host2dspmailbox + core_id*DEMO_PER_MAILBOX_MEM_SIZE);

      ret_val = mpm_mailbox_create(tx_mailbox_handle[node_id], "dsp0",
        MPM_MAILBOX_MEMORY_LOCATION_REMOTE, MPM_MAILBOX_DIRECTION_SEND, &mpm_mailbox_config);
#endif

      demoK2H_assert( (ret_val == 0 ), node_id,
        "ERROR: mpm_mailbox_create(host --> dsp) ");
      printf("Created Mailbox Tx id: 0x%8x Handle %p\n",node_id, tx_mailbox_handle[node_id] );
#ifdef DEMO_VERBOSE
      printf("Creating Mailbox (%x --> %x)\n",node_id,MAILBOX_MAKE_HOST_NODE_ID(0));
#endif
      rx_mailbox_handle[node_id] = (void *)malloc(mailboxallocsize);
      demoK2H_assert( (rx_mailbox_handle[node_id] != NULL), node_id, "ERROR: malloc Rx handle ");
#if 1
      mpm_mailbox_config.mem_start_addr = (uint32_t)rx_mailbox_mapping[dsp_id].userAddr + core_id*DEMO_PER_MAILBOX_MEM_SIZE;

      ret_val = mpm_mailbox_create(rx_mailbox_handle[node_id], NULL,
        MPM_MAILBOX_MEMORY_LOCATION_LOCAL, MPM_MAILBOX_DIRECTION_RECEIVE, &mpm_mailbox_config);
#else
      mpm_mailbox_config.mem_start_addr = (uint32_t)(dsp2hostmailbox + core_id*DEMO_PER_MAILBOX_MEM_SIZE);

      ret_val = mpm_mailbox_create(rx_mailbox_handle[node_id], "dsp0",
        MPM_MAILBOX_MEMORY_LOCATION_REMOTE, MPM_MAILBOX_DIRECTION_RECEIVE, &mpm_mailbox_config);
#endif

      demoK2H_assert( (ret_val == 0), node_id, 
        "ERROR: mpm_mailbox_create(dsp --> host) ");
      printf("Created Mailbox Rx id: 0x%8x, Handle %p\n",node_id, rx_mailbox_handle[node_id] );
    }
  }
  /***  Open Mailboxes ***/
  for ( node_id=0; node_id < (num_of_dsps*DEMO_CONFIG_CORES_PER_CHIP); node_id++)
  {
    if( nodeBits & (((uint32_t) 1) << (node_id%32)) )
    {
      printf("nodeBits %x decision%x \n",(((uint32_t) 1) << node_id),  
        nodeBits & (((uint32_t) 1) << node_id) );
      printf("Opening Mailbox(%x --> %x)\n", MAILBOX_MAKE_HOST_NODE_ID(0),node_id);
      demoK2H_assert( (mpm_mailbox_open(tx_mailbox_handle[node_id]) == 0), 
        node_id, "ERROR: mpm_mailbox_init(host --> dsp) ");
      printf("Opening Mailbox(%x --> %x)\n",node_id, MAILBOX_MAKE_HOST_NODE_ID(0));
      demoK2H_assert( (mpm_mailbox_open(rx_mailbox_handle[node_id]) == 0),
        node_id, "ERROR: mpm_mailbox_init(dsp --> host) ");
    }
  }

  printf("\t\t\tMailboxed Created!\n");

  sleep(1);

  return 0;
}
/**
 *  @brief Function demoK2H_Config_free : Free memory allocated by demoK2H_Config
 *  @param[in]         nodeBits   Node bit map
 *  @retval            none      
 *  @pre  
 *  @post 
 */

void demoK2H_Config_free(uint32_t nodeBits)
{
  int32_t node_id;

  /*** Create mailboxes ***/
  for ( node_id = 0; node_id < (num_of_dsps*DEMO_CONFIG_CORES_PER_CHIP); node_id++)
  {
    if( nodeBits & (((uint32_t) 1) << (node_id%32)) )
    {
      free(rx_mailbox_handle[node_id]);
      free(tx_mailbox_handle[node_id]);
    }
  }
}

/**
 *  @brief Function read_and_send_data_to_dsp_through_memcpy : This routine reads a block of data from input file
 *         and writes to Input buffer in DSP address space through pciedrv_dsp_write api. 
 *         Also sends a mail to the DSP through Mailbox
 *  @param[in]         node_id   Node id
 *  @param[in]         trans_id  Transaction id to be used with mailbox 
 *  @param[in]         inFile    Input file descriptor 
 *  @retval            none       
 *  @pre  
 *  @post 
 */
void read_and_send_data_to_dsp_through_memcpy(int32_t node_id, uint32_t trans_id, FILE *inFile)
{
  void *inBufHandle, *outBufHandle;
  demoTestMsg_t dspMsg;
  int32_t ret_val;
  struct timespec tp_start, tp_end;
  bufmgrDesc_t bufDesc;
  mpm_transport_write_t mpm_transport_write_cfg;
  int64_t tmp_value;

  inBufHandle = c66InputBufPool[(node_id / DEMO_CONFIG_CORES_PER_CHIP)];
  outBufHandle = c66OutputBufPool[(node_id / DEMO_CONFIG_CORES_PER_CHIP)];

  /* Read Buffer from file and send to Mailbox */
  dspMsg.input_size = fread(inputBuf,1,demo_payload_size,inFile);
  /* Allocate Input buffer from pool */
  ret_val = bufmgrAlloc(inBufHandle, 1, &bufDesc);
  demoK2H_assert( (ret_val == BUF_MGR_ERROR_NONE), node_id, 
    "ERROR: bufmgrAlloc() did not find free input buffer.");
  printf("bufmgrAlloc(%d) = 0x%08X\n", node_id, (uint32_t)(intptr_t)(bufDesc.userAddr));
  dspMsg.input_addr = (uint32_t)(intptr_t)(bufDesc.userAddr);
  /* Allocate Output buffer from pool */
  ret_val = bufmgrAlloc(outBufHandle, 1, &bufDesc);
  demoK2H_assert( (ret_val == BUF_MGR_ERROR_NONE), node_id, 
    "ERROR: bufmgrAlloc() did not find free output buffer.");
  printf("bufmgrAlloc(%d) = 0x%08X\n", node_id, (uint32_t)(intptr_t)(bufDesc.userAddr));
  dspMsg.output_addr = (uint32_t)(intptr_t)(bufDesc.userAddr);
  /* Write data from temporary buffer to Input buffer */
  clock_gettime(CLOCK_MONOTONIC, &tp_start);
  ret_val = mpm_transport_write(
              mpm_transport_handle[0],
              dspMsg.input_addr,
              dspMsg.input_size,
              inputBuf,
              &mpm_transport_write_cfg);

  demoK2H_assert( (ret_val == 0), node_id, "ERROR: mpm_transport_write()");

  clock_gettime(CLOCK_MONOTONIC, &tp_end);
  if(dspMsg.input_size ==demo_payload_size) {
    total_write_time += clock_diff (&tp_start, &tp_end);
    total_write_time_cnt ++;
  }

  printf(
    "\nMsg (%08X) to --> %2d : input_addr = 0x%08X, input_size = %10d, output_addr = 0x%08X, output_size = %10d \n\n",
    trans_id,node_id,dspMsg.input_addr,dspMsg.input_size,dspMsg.output_addr,
    dspMsg.output_size);
  printf("\ntx_mailbox node_id %x, handle (%p)\n",node_id, tx_mailbox_handle[node_id]);
  clock_gettime(CLOCK_MONOTONIC, &tp_start);
   /* Send message through Mailbox */ 
  ret_val = mpm_mailbox_write(tx_mailbox_handle[node_id], (uint8_t *)&dspMsg, sizeof(demoTestMsg_t), 
              trans_id);
  clock_gettime(CLOCK_MONOTONIC, &tp_end);
  tmp_value =  clock_diff(&tp_start, &tp_end);
  if(tmp_value < mailbox_write_min_time)
    mailbox_write_min_time = tmp_value;
  if(tmp_value > mailbox_write_max_time)
    mailbox_write_max_time = tmp_value;
  total_mailbox_write_time += tmp_value;
  total_mailbox_write_cnt++;

  demoK2H_assert( (ret_val == 0), node_id, "ERROR: mpm_mailbox_write(host --> dsp) ");

}

/**
 *  @brief Function read_and_send_data_to_dsp_through_mapping : This routine reads a block of data from input file
 *         and writes to Input buffer in Host address space and maps to DSP memory range. 
 *         Also sends a mail to the DSP through Mailbox
 *  @param[in]         node_id   Node id
 *  @param[in]         trans_id  Transaction id to be used with mailbox 
 *  @param[in]         inFile    Input file descriptor 
 *  @retval            none       
 *  @pre  
 *  @post 
 */
void read_and_send_data_to_dsp_through_mapping(int32_t node_id, uint32_t trans_id, FILE *inFile, int32_t cached)
{
  uint32_t i, num_buffers, remaining_size, read_size, write_size ;
  struct timespec tp_start, tp_inter, tp_end;
  bufmgrDesc_t *inbufDesc_list, input_desc;
  bufmgrDesc_t *outbufDesc_list, output_desc;
  demoTestMsg_t dspMsg;
  int32_t ret_val;
  uint32_t fread_size;
  int64_t tmp_value;

  /* Allocate input buffers */
  ret_val = bufmgrAlloc(c66InputBufPool[(node_id/DEMO_CONFIG_CORES_PER_CHIP)], 1, &input_desc);
  demoK2H_assert( (ret_val == BUF_MGR_ERROR_NONE), node_id, 
    "ERROR: bufmgrAlloc() did not find free input buffer.");
  printf("bufmgrAlloc(%d) = %p\n", node_id, (input_desc.userAddr));

  /* Read data into input buffers */
  dspMsg.input_size = 0;

  clock_gettime(CLOCK_MONOTONIC, &tp_start);
  /* Read Buffer from file and send to Mailbox */
  fread_size = fread(input_desc.userAddr, 1,demo_payload_size,inFile);
  clock_gettime(CLOCK_MONOTONIC, &tp_inter);
//  memcpy(inbufDesc_list[i].userAddr,inputBuf, read_size);
  clock_gettime(CLOCK_MONOTONIC, &tp_end);
  if(fread_size == demo_payload_size)
  {
    file_read_time += clock_diff (&tp_start, &tp_inter);
    memcpy_read_time += clock_diff(&tp_inter, &tp_end);
    file_read_time_cnt ++;
  }
  if ( cached == DEMO_BUFS_CACHED )
    CMEM_cacheWb( input_desc.userAddr, fread_size);

  dspMsg.input_size += fread_size;

  dspMsg.input_addr =  (uint32_t)input_desc.physAddr;

  /* Allocate output buffers */
  ret_val = bufmgrAlloc(c66OutputBufPool[(node_id/DEMO_CONFIG_CORES_PER_CHIP)], 1, &output_desc);
  demoK2H_assert( (ret_val == BUF_MGR_ERROR_NONE), node_id, 
    "ERROR: bufmgrAlloc() did not find free output buffer.");
  printf("bufmgrAlloc(%d) = %p\n", node_id, (output_desc.userAddr));

  dspMsg.output_addr = (uint32_t)output_desc.physAddr;

  printf("\nMsg (%08X) to --> %2d : input_addr = 0x%08X, input_size = %10d, output_addr = 0x%08X, output_size = %10d \n\n",trans_id,node_id,dspMsg.input_addr,dspMsg.input_size,dspMsg.output_addr,dspMsg.output_size);

  clock_gettime(CLOCK_MONOTONIC, &tp_start);
  ret_val = mpm_mailbox_write(tx_mailbox_handle[node_id], (uint8_t *)&dspMsg, sizeof(demoTestMsg_t), trans_id);
  clock_gettime(CLOCK_MONOTONIC, &tp_end);
tmp_value =  clock_diff(&tp_start, &tp_end);
  if(tmp_value < mailbox_write_min_time)
    mailbox_write_min_time = tmp_value;
  if(tmp_value > mailbox_write_max_time)
    mailbox_write_max_time = tmp_value;
  total_mailbox_write_time += tmp_value;
  total_mailbox_write_cnt++;

  demoK2H_assert( (ret_val == 0), node_id, "ERROR: mpm_mailbox_write(host --> dsp) ");
}


/**
 *  @brief Function get_data_through_mapped_memory_and_write_to_file : This routine gets message from  
 *         mpm_mailbox from the specified DSP node, and gets the data from the Host buffer mapped to dsp space
 *         And write the data to a file 
 *  @param[in]         node_id   Node id
 *  @param[in]         outFile   Output file descriptor 
 *  @retval            none       
 *  @pre  
 *  @post 
 */
void get_data_through_mapped_memory_and_write_to_file(int32_t node_id,  FILE *outFile, int32_t cached)
{
  uint32_t i, map_idx, num_descs, remaining_size, write_size ;
  struct timespec tp_start, tp_end, tp_inter;
  demoTestMsg_t dspMsg;
  int32_t ret_val;
  uint32_t    size_rx, trans_id_rx;
  int64_t tmp_value;
  uint8_t *input_buf, *output_buf;

  /* Get message from mailbox */
  printf("\nrx_mailbox node_id %x, handle (%p)\n",node_id, rx_mailbox_handle[node_id]);
  clock_gettime(CLOCK_MONOTONIC, &tp_start);
  ret_val = mpm_mailbox_read(rx_mailbox_handle[node_id], (uint8_t *)&dspMsg, &size_rx, &trans_id_rx);
  clock_gettime(CLOCK_MONOTONIC, &tp_end);
  tmp_value =  clock_diff(&tp_start, &tp_end);
  if(tmp_value < mailbox_read_min_time)
    mailbox_read_min_time = tmp_value;
  if(tmp_value > mailbox_read_max_time)
    mailbox_read_max_time = tmp_value;
  total_mailbox_read_time += tmp_value;
  total_mailbox_read_cnt++;

  demoK2H_assert( (ret_val == 0), node_id, "ERROR: mpm_mailbox_read(host <-- dsp) ");

  printf("\nMsg (%08X) from --> %2d : input_addr = 0x%08X, input_size = %10d, output_addr = 0x%08X, output_size = %10d \n\n",trans_id_rx,node_id,dspMsg.input_addr,dspMsg.input_size,dspMsg.output_addr,dspMsg.output_size);

  /* Find mapped user address */
  num_descs = DEMO_FILETEST_POOL_SIZE/demo_payload_size;
  output_buf = input_buf = NULL;
  for ( i = 0, map_idx = num_descs*(node_id/DEMO_CONFIG_CORES_PER_CHIP); i < num_descs; i++, map_idx++)
  {
    if ( input_buf_map[map_idx].physAddr == dspMsg.input_addr )
      input_buf = input_buf_map[map_idx].userAddr;

    if ( output_buf_map[map_idx].physAddr == dspMsg.output_addr )
      output_buf = output_buf_map[map_idx].userAddr;

    if ( (input_buf != NULL) && (output_buf != NULL) )
      break;
  }
  demoK2H_assert( (i < num_descs), node_id, "Error finding mapped address");

  if ( cached == DEMO_BUFS_CACHED )
    CMEM_cacheInv( output_buf, dspMsg.output_size);

  /* Write data from output buffers to file */
   clock_gettime(CLOCK_MONOTONIC, &tp_start);
  /* Read from Buffer  and write to file  */
  clock_gettime(CLOCK_MONOTONIC, &tp_inter);
  fwrite(output_buf, 1,dspMsg.output_size,outFile);
  clock_gettime(CLOCK_MONOTONIC, &tp_end);
 
  file_write_time += clock_diff (&tp_inter, &tp_end);
  memcpy_write_time += clock_diff (&tp_start, &tp_inter);
  file_write_time_cnt ++;

  /* Free the input buffers */
  ret_val = bufmgrFreeUserAddr(c66InputBufPool[(node_id/DEMO_CONFIG_CORES_PER_CHIP)], input_buf);
  demoK2H_assert( (ret_val == BUF_MGR_ERROR_NONE), node_id, "ERROR: bufmgrFreeDesc() did not free input buffer.");
  printf("bufmgrFreeDesc(%d) = %p\n", node_id, input_buf);

  ret_val = bufmgrFreeUserAddr(c66OutputBufPool[(node_id/DEMO_CONFIG_CORES_PER_CHIP)], output_buf);
  demoK2H_assert( (ret_val == BUF_MGR_ERROR_NONE), node_id, "ERROR: bufmgrFreeDesc() did not free output buffer.");
  printf("bufmgrFreeDesc(%d) = %p\n", node_id, output_buf);
}


void get_data_through_memcpy_and_write_to_file(int32_t node_id,  FILE *outFile)
{
  void *inBufHandle, *outBufHandle;
  demoTestMsg_t dspMsg;
  int32_t ret_val;
  bufmgrDesc_t bufDesc;
  mpm_transport_read_t mpm_transport_read_cfg;
  struct timespec tp_start, tp_end;
  uint32_t  size_rx, trans_id_rx;
  int64_t tmp_value;

  inBufHandle = c66InputBufPool[(node_id / DEMO_CONFIG_CORES_PER_CHIP)];
  outBufHandle = c66OutputBufPool[(node_id / DEMO_CONFIG_CORES_PER_CHIP)];

  printf("\nrx_mailbox node_id %x handle (%p)\n",node_id, rx_mailbox_handle[node_id]);
  clock_gettime(CLOCK_MONOTONIC, &tp_start);
  ret_val = mpm_mailbox_read(rx_mailbox_handle[node_id], (uint8_t *)&dspMsg, &size_rx, &trans_id_rx);
  clock_gettime(CLOCK_MONOTONIC, &tp_end);
  tmp_value =  clock_diff(&tp_start, &tp_end);
  if(tmp_value < mailbox_read_min_time)
    mailbox_read_min_time = tmp_value;
  if(tmp_value > mailbox_read_max_time)
    mailbox_read_max_time = tmp_value;
  total_mailbox_read_time += tmp_value;
  total_mailbox_read_cnt++;
   demoK2H_assert( (ret_val == 0), node_id, "ERROR: mpm_mailbox_read(host <-- dsp) ");

  printf("\nMsg (%08X) from --> %2d : input_addr = 0x%08X, input_size = %10d, output_addr = 0x%08X, output_size = %10d \n\n",
    trans_id_rx,node_id,dspMsg.input_addr,dspMsg.input_size,dspMsg.output_addr,dspMsg.output_size);

  clock_gettime(CLOCK_MONOTONIC, &tp_start);
  ret_val = mpm_transport_read(
              mpm_transport_handle[0], 
              dspMsg.output_addr, 
              dspMsg.output_size, 
              outputBuf,
              &mpm_transport_read_cfg);

  clock_gettime(CLOCK_MONOTONIC, &tp_end);
  demoK2H_assert( (ret_val == 0), node_id, "ERROR: mpm_transport_read()");
  if(dspMsg.output_size ==demo_payload_size) {
    total_read_time += clock_diff (&tp_start, &tp_end);
    total_read_time_cnt ++;
  }
  fwrite(outputBuf,1,dspMsg.output_size,outFile);

  printf("bufmgrFree(%d) = 0x%08X\n", node_id, dspMsg.input_addr);
  ret_val = bufmgrFreeUserAddr( inBufHandle, (void *)(intptr_t)dspMsg.input_addr);
  demoK2H_assert( (ret_val == BUF_MGR_ERROR_NONE), node_id, "ERROR: bufmgrFreeUserAddr() for input buffer.");

  printf("bufmgrFree(%d) = 0x%08X\n", node_id, dspMsg.output_addr);
  ret_val = bufmgrFreeUserAddr( outBufHandle, (void *)(intptr_t)dspMsg.output_addr);
  demoK2H_assert( (ret_val == BUF_MGR_ERROR_NONE), node_id, "ERROR: bufmgrFreeUserAddr() for output buffer.");

 }

/**
 *  @brief Function do_filetest :  Do file test: Demo to send data and 
 *                  receive data to DSP
 *  @param[in]         nodeBits   Node bit map
 *  @param[in]         inFile     Input file pointer
 *  @param[in]         outFile    Output file pointer
 *  @param[in]         flag       0 : Memcopy test 1: DSP Map test 2: DMA test
 *  @retval            none 
 *  @pre  
 *  @post 
 */

void do_filetest(uint32_t nodeBits, FILE *inFile, FILE *outFile, uint32_t flag)
{
  int32_t node_id, ret_val;
  uint32_t trans_id = 0xC0DE0000 ;
  uint32_t read_cnt, write_cnt;

  printf("Starting Test...\n");
  for ( node_id = 0; node_id < (num_of_dsps*DEMO_CONFIG_CORES_PER_CHIP); node_id++)
  {
    if( nodeBits & (((uint32_t) 1) << (node_id%32)) )
    {
      /* Send message through the mechanism chosen for demo */
      switch(flag)
      {
        case DEMO_MODE_CMEM_UNCACHED_TEST:
        case DEMO_MODE_CMEM_CACHED_TEST:
        case DEMO_MODE_DSPMAP_TEST:
/*         NO Priming for map test */
//          read_and_send_data_to_dsp_through_mapping(node_id, trans_id++, inFile);
          break;
        case DEMO_MODE_MEMCPY_TEST:
        default:
          read_and_send_data_to_dsp_through_memcpy(node_id, trans_id++, inFile);
          break;
      }
    } 
  }
  
  printf("Priming done.\n");

  while (!feof(inFile))
  {

    /* Read next buffer from file and send to Maibox */
    for ( node_id = 0; node_id < (num_of_dsps*DEMO_CONFIG_CORES_PER_CHIP); node_id++)
    {
      if( nodeBits & (((uint32_t) 1) << (node_id%32)) )
      {
        /* Send message through the mechanism chosen for demo */
        switch(flag)
        {
          case DEMO_MODE_CMEM_UNCACHED_TEST:
          case DEMO_MODE_DSPMAP_TEST:
            read_and_send_data_to_dsp_through_mapping(node_id, trans_id++, inFile, DEMO_BUFS_UNCACHED);
            break;
          case DEMO_MODE_CMEM_CACHED_TEST:
            read_and_send_data_to_dsp_through_mapping(node_id, trans_id++, inFile, DEMO_BUFS_CACHED);
            break;
          case DEMO_MODE_MEMCPY_TEST:
          default:
            read_and_send_data_to_dsp_through_memcpy(node_id, trans_id++, inFile);
          break;
        }
      }
    }

    /* Read any processed frames back from DSP and write to file */ 
    for ( node_id = 0; node_id < (num_of_dsps*DEMO_CONFIG_CORES_PER_CHIP); node_id++)
    {
      if( nodeBits & (((uint32_t) 1) << (node_id%32)) )
      {     
        /* Wait for message from  DSP node, through mailBOx */   
        do {
          ret_val = mpm_mailbox_query(rx_mailbox_handle[node_id]);
        } while(!ret_val);

        /* Read mpm_mailbox and read data through mechanism chosen for demo */
        switch(flag)
        {
          case DEMO_MODE_CMEM_CACHED_TEST:
            get_data_through_mapped_memory_and_write_to_file(node_id, outFile, DEMO_BUFS_CACHED);
            break;
          case DEMO_MODE_CMEM_UNCACHED_TEST:
          case DEMO_MODE_DSPMAP_TEST:
            get_data_through_mapped_memory_and_write_to_file(node_id, outFile, DEMO_BUFS_UNCACHED);
            break;
          case DEMO_MODE_MEMCPY_TEST:
          default:
            get_data_through_memcpy_and_write_to_file(node_id, outFile);
          break;
        }
      }
    }
  }

  /* Read any frames still pending to be received and write to file */
  for ( node_id = 0; node_id < (num_of_dsps*DEMO_CONFIG_CORES_PER_CHIP); node_id++)
  {
    if( nodeBits & (((uint32_t) 1) << (node_id%32)) )
    {
      /* Read mpm_mailbox and read data through mechanism chosen for demo */
      switch(flag)
      {
        case DEMO_MODE_CMEM_UNCACHED_TEST:
        case DEMO_MODE_CMEM_CACHED_TEST:
        case DEMO_MODE_DSPMAP_TEST:
//          get_data_through_mapped_memory_and_write_to_file(node_id, outFile);
          break;
        case 0:
        default:
          /* Wait for message from  DSP node, through mailBOx */   
          do {
          ret_val = mpm_mailbox_query(rx_mailbox_handle[node_id]);
          } while(!ret_val);
          get_data_through_memcpy_and_write_to_file(node_id, outFile);
          break;
      }
    }
  }
  printf(" End of Test...\n");fflush(stdout);

}


#ifdef BENCHMARK_MAILBOX_ENABLE
void do_null_mailbox_test()
{
  int node_id=0;
  int trans_id_tx,trans_id_rx;
  int size_rx;
  int32_t i, num_buffers, remaining_size, read_size, write_size ;
  struct timespec tp_start, tp_inter, tp_end;
  bufmgrDesc_t *inbufDesc_list, input_desc;
  bufmgrDesc_t *outbufDesc_list, output_desc;
  demoTestMsg_t dspMsg;
  int32_t ret_val;
  uint32_t fread_size;
  int64_t total_null_mailbox_time=0,null_mailbox_time;

  printf("\n NULL Kernel Profiling- Start");
  fflush(stdout);

  dspMsg.input_size = 0;
  dspMsg.input_addr =  (uint32_t)0;
  dspMsg.output_addr = (uint32_t)0;

for(i=0;i<10;i++) {

  clock_gettime(CLOCK_MONOTONIC, &tp_start);

  ret_val = mpm_mailbox_write(tx_mailbox_handle[node_id], (uint8_t *)&dspMsg, sizeof(demoTestMsg_t), trans_id_tx);
  demoK2H_assert( (ret_val == 0), node_id, "ERROR: mpm_mailbox_write(host <-- dsp) ");
  do {
      ret_val = mpm_mailbox_query(rx_mailbox_handle[node_id]);
  } while(!ret_val);

  ret_val = mpm_mailbox_read(rx_mailbox_handle[node_id], (uint8_t *)&dspMsg, &size_rx, &trans_id_rx);

  clock_gettime(CLOCK_MONOTONIC, &tp_end);
  demoK2H_assert( (ret_val == 0), node_id, "ERROR: mpm_mailbox_read(host <-- dsp) ");

    null_mailbox_time = clock_diff (&tp_start, &tp_end);
    total_null_mailbox_time+=null_mailbox_time;

  printf("\n NULL Profiling- Mailbox Post-Receive time(%d) = %lld",i,null_mailbox_time);
}
  
  printf("\n NULL Kernel Profiling- Average Null mailbox send/recv latency = %lld",(total_null_mailbox_time/10));

}
#endif

/**
 *  @brief Function main :  Main function
 *  @param[in]         argc       Command line argument count
 *  @param[in]         argv       Command line arguments
 *  @retval            none 
 *  @pre  
 *  @post 
 */


void main(int32_t argc, int8_t **argv)
{
  FILE *inFile, *outFile;
  uint32_t dspBits = 0;
  int32_t  i, core_id;
  bufmgrDesc_t buf_desc[MAX_NUM_HOST_DSP_BUFFERS];
  uint32_t flag = 0;
  int32_t ret_val;
  uint32_t max_payload_size;
  void *dsp_image_handle;
  uint32_t dsp_entry_point;

  /* Read parameters from command line */
   if(argc < 7)
  {
    printf("Usage: %s <input_file> <output_file> \
<test type flag: 0: Memcpy; 1: DSP Map; 2: CMEM Cached; 3: CMEM Noncached > \
        <dsp_bits> <num_of_dsps> < payload size in hex >\n",argv[0]);
    exit(-1);
  }
  /* Open input file for reading */
  inFile = fopen(argv[1],"rb");
  if(inFile == NULL)
  {
    printf("Error opening input file: %s\n", argv[1]);
    exit(-1);
  }
  /* Open output file */
  outFile = fopen(argv[2],"wb");
  if(outFile == NULL)
  {
    fclose(inFile);
    printf("Error opening output file: %s\n", argv[2]);
    exit(-1);
  }

  /* Record flag to indicate type of test*/
  flag = atoi(argv[3]);

  sscanf (argv[4], "%x", &dspBits);
  printf("dspBits = %08X \n",dspBits);

  if(dspBits == 0)
    dspBits = 1;

  printf("dspBits = %08X \n",dspBits);

  sscanf(argv[5], "%d", &num_of_dsps);
  if(num_of_dsps > DEMO_FILETEST_MAX_NUM_CHIPS)
  {
    num_of_dsps = DEMO_FILETEST_MAX_NUM_CHIPS;
    printf("\n Number of dsps limited to %d",num_of_dsps);
  }

  sscanf(argv[6], "%x", &demo_payload_size);

  host2dspmailbox = 0xa0ff8000;
  dsp2hostmailbox = 0xa0ff0000;

  /* Check limits on payload size */
  max_payload_size = DEMO_FILETEST_MAX_PAYLOAD_BUFFER_SIZE;
  if(demo_payload_size > max_payload_size)
  {
    demo_payload_size = max_payload_size;
    printf("\n Payload size limited to 0x%x",demo_payload_size);
  }

  /* Configure required sdk modules */
  demoK2H_Config(dspBits);
  printf("\t\t\tConfiguration Complete!\n");

    switch(flag) 
    {
      case DEMO_MODE_DSPMAP_TEST:
        demoK2H_initDspBufsMapped();
        break;
      case DEMO_MODE_CMEM_CACHED_TEST:
        demoK2H_initCmemBufs(DEMO_BUFS_CACHED);
        break;
      case DEMO_MODE_CMEM_UNCACHED_TEST:
        demoK2H_initCmemBufs(DEMO_BUFS_UNCACHED);
        break;
      case DEMO_MODE_MEMCPY_TEST:
         /* Initialise Buffer Manager */
        demoK2H_initDspBufs();
        break;

      default:
	/*Nothing to do for this case */
       break;

     }  

   /* Run test based on flag */
  switch(flag) {
    default:
     /* Run the actual test */
      do_filetest(dspBits,inFile,outFile, flag);
      break;
  }

#ifdef BENCHMARK_MAILBOX_ENABLE
  do_null_mailbox_test();
#endif

  /* Free resources */
  demoK2H_Config_free(dspBits);
  printf("\n Resources free complete\n");fflush(stdout);
  
  /* Delete buffers and pools, based on flag */
  demoK2H_deletePools(flag);

  /* Print stats */
  printf("\n Payload size : %d bytes ",demo_payload_size); 
  if(total_dma_read_time_cnt && total_dma_write_time_cnt)
  {
   printf ("\nTotal_dma_read_time=%lld ns total_dma_read_cnt=%d", (long long)total_dma_read_time, total_dma_read_time_cnt);
   printf ("\nTotal_dma_write_time=%lld ns total_dma_write_cnt=%d", (long long)total_dma_write_time, total_dma_write_time_cnt);
   printf("\n dma_read_time %lld ns , dma write_time %lld ns ", 
      (long long)(total_dma_read_time/total_dma_read_time_cnt),
      (long long)(total_dma_write_time/total_dma_write_time_cnt));
   printf("\n Transfer rate in Mbytes /sec Read %d, Write %d\n", (uint32_t)((((uint64_t)1000 * demo_payload_size)/(total_dma_read_time/total_dma_read_time_cnt))),
     (uint32_t)((((uint64_t)1000 * demo_payload_size)/(total_dma_write_time/total_dma_write_time_cnt))));
  }
  if(total_read_time_cnt && total_write_time_cnt)
  {

    printf ("\nTotal_read_time=%lld ns total_read_cnt=%d", (long long)total_read_time, total_read_time_cnt);
    printf ("\nTotal_write_time=%lld ns total_write_cnt=%d", (long long)total_write_time, total_write_time_cnt);
    printf("\n read_time %lld ns , total write_time %lld ns ", 
      (long long)(total_read_time/total_read_time_cnt),
      (long long)(total_write_time/total_write_time_cnt));
    printf("\n Transfer rate in Mbytes /sec Read %d, Write %d\n", 
        (uint32_t)((((uint64_t)1000 * demo_payload_size)/(total_read_time/total_read_time_cnt))),
        (uint32_t)((((uint64_t)1000 * demo_payload_size)/(total_write_time/total_write_time_cnt))));
  }
  if( file_read_time_cnt && file_write_time_cnt)
  {
    printf ("\ntotal file_read_time=%lld ns total file_read_cnt=%d", (long long)file_read_time, file_read_time_cnt);
    printf ("\nfile_write_time=%lld ns file_write_cnt=%d", (long long)file_write_time, file_write_time_cnt);
    printf("\n file read_time %lld ns , file write_time %lld ns ", 
        (long long)(file_read_time/file_read_time_cnt),
        (long long)(file_write_time/file_write_time_cnt));

    printf ("\nmemcpy_read_time=%lld ns memcpy_write_time=%lld ns\n", (long long)(memcpy_read_time/file_read_time_cnt), (long long)(memcpy_write_time/file_write_time_cnt));
  }
  if(total_mailbox_read_cnt && total_mailbox_write_cnt)
  {
    printf ("\ntotal mailbox_read_time=%lld ns total_mailbox_write_time=%lld ns",
      (long long)(total_mailbox_read_time), (long long)(total_mailbox_write_time));
    printf ("\ntotal mailbox_read_cnt=%lld  total_mailbox_write_cnt=%lld",
        (long long)(total_mailbox_read_cnt), (long long)(total_mailbox_write_cnt));
    printf ("\n mailbox_read_min_time=%lld ns mailbox_write_min_time=%lld ns",
      (long long)(mailbox_read_min_time), (long long)(mailbox_write_min_time));
    printf ("\n mailbox_read_max_time=%lld ns mailbox_write_max_time=%lld ns",
      (long long)(mailbox_read_max_time), (long long)(mailbox_write_max_time));
    printf ("\n mailbox_read_time=%lld ns mailbox_write_time=%lld ns",
      (long long)(total_mailbox_read_time/total_mailbox_read_cnt), (long long)(total_mailbox_write_time/total_mailbox_write_cnt));
  }
  printf("\nTest Complete\n");
close_files_and_exit:




err_driver_open:
  fclose(inFile);
  fclose(outFile); 
exit(0);

}
/*** nothing past this point ***/
