#!/bin/bash

numcores=8
if [ $# -gt 0 ]; then
	numcores=$1
fi
i=0
while [ $i -lt $numcores ]; do
	echo "dsp$i"
	mpmcl reset dsp$i
	let i=i+1
done

i=0

while [ $i -lt $numcores ]; do
	echo "demo_loopback.out -> dsp$i"
	mpmcl load dsp$i ../c66x/demo_loopback/build/bin/demo_loopback.out
	let i=i+1
done

i=0

while [ $i -lt $numcores ]; do
	echo "dsp$i"
	mpmcl run dsp$i
	let i=i+1
done
