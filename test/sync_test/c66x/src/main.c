#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <mpm_sync.h>
#include "sync_test.h"

extern cregister volatile unsigned int DNUM;

sync_test_shmem_t *sync_test_shmem;

int32_t  sync_test_barr_size    = 0;
void    *sync_test_barr_mem     = NULL;
void    *sync_test_barr_handle  = NULL;

int32_t  sync_test_lock_size    = 0;
void    *sync_test_lock_mem     = NULL;
void    *sync_test_lock_handle  = NULL;


int32_t sync_test_transport_open(void)
{
    volatile unsigned int *MAR = (volatile unsigned int *)0x1848000;

    /*  A value of 1 indicates cacheable, 0 non-cachable */
    /* 0xA0000000 - 0xBfffffff  non-cachable */

   MAR[160] = 0x0;
   MAR[161] = 0x0;
   MAR[162] = 0x0;
   MAR[163] = 0x0;
   MAR[164] = 0x0;
   MAR[165] = 0x0;
   MAR[166] = 0x0;
   MAR[167] = 0x0;
   MAR[168] = 0x0;
   MAR[169] = 0x0;
   MAR[170] = 0x0;
   MAR[171] = 0x0;
   MAR[172] = 0x0;
   MAR[173] = 0x0;
   MAR[174] = 0x0;
   MAR[175] = 0x0;
   MAR[176] = 0x0;
   MAR[177] = 0x0;
   MAR[178] = 0x0;
   MAR[179] = 0x0;
   MAR[180] = 0x0;
   MAR[181] = 0x0;
   MAR[182] = 0x0;
   MAR[183] = 0x0;
   MAR[184] = 0x0;
   MAR[185] = 0x0;
   MAR[186] = 0x0;
   MAR[187] = 0x0;
   MAR[188] = 0x0;
   MAR[189] = 0x0;
   MAR[190] = 0x0;
   MAR[191] = 0x0;

   return 0;
}

void sync_test_transport_close(void)
{
  return;
}

int32_t sync_test_get_cfg(int32_t *num_dsps)
{
  sync_test_cfg_t *cfg = (sync_test_cfg_t *)SYNC_TEST_CFG_BASE;
  volatile uint32_t cfg_magic = 0;

  memset ( cfg, 0, sizeof(sync_test_cfg_t));

  while ( cfg_magic == 0 )
  {
    cfg_magic = cfg->num_dsps;
  }

  *num_dsps = cfg->num_dsps;

  return 0;
}


int32_t sync_test_shmem_create(int32_t num_dsps)
{
  if ( sizeof(sync_test_shmem_t) > SYNC_TEST_SHMEM_SIZE )
  {
    fprintf( stderr, "Error: sync_test_shmem_t does not fit in memory map. (%d > %d)\n", sizeof(sync_test_shmem_t), SYNC_TEST_SHMEM_SIZE);
    return -1;
  }

  sync_test_shmem = (sync_test_shmem_t *)SYNC_TEST_SHMEM_BASE;

  if ( !sync_test_shmem )
  {
    fprintf( stderr, "Error: Could not map shmem.\n");
    return -2;
  }

  return 0;
}

void sync_test_shmem_destroy(void)
{
  if ( sync_test_shmem )
  {
    sync_test_shmem = NULL;
  }
}


int32_t sync_test_barr_create(int32_t num_dsps)
{
  sync_test_barr_size = mpm_sync_barr_get_sizes(num_dsps + 1);

  if ( sync_test_barr_size > SYNC_TEST_BARR_SIZE )
  {
    fprintf( stderr, "Error: Barrier does not fit into memory map. (%d > %d).\n", sync_test_barr_size, SYNC_TEST_BARR_SIZE);
    return -1;
  }

  sync_test_barr_handle = sync_test_barr_mem = (void *)SYNC_TEST_BARR_BASE;

  if ( !sync_test_barr_mem ) 
  {
    fprintf ( stderr, "Error: Could not mmap barrier (%08X)\n", (uint32_t)SYNC_TEST_BARR_BASE);
    return -2;
  }

  /* Initialization done by host */
  sync_test_barr_handle = sync_test_barr_mem;

  if ( !sync_test_barr_handle )
  {
    fprintf ( stderr, "Error: Could not initialize barrier.\n");
    return -3;
  }

  return 0;
}


void sync_test_barr_destroy(void)
{
  if ( sync_test_barr_mem )
  {
    sync_test_barr_mem = NULL;
    sync_test_barr_handle = NULL;
  }
}

int32_t sync_test_barr(void)
{
  int32_t ret_val;

  ret_val = mpm_sync_barr_wait( sync_test_barr_handle, DNUM + 1);

  return (ret_val);
}
  

int32_t sync_test_lock_create(int32_t num_dsps)
{
  sync_test_lock_size = mpm_sync_lock_get_sizes(num_dsps + 1);

  if ( sync_test_lock_size > SYNC_TEST_LOCK_SIZE )
  {
    fprintf( stderr, "Error: Lock does not fit into memory map. (%d > %d).\n", sync_test_lock_size, SYNC_TEST_LOCK_SIZE);
    return -1;
  }

  sync_test_lock_handle = sync_test_lock_mem = (void *)SYNC_TEST_LOCK_BASE;

  if ( !sync_test_lock_mem ) 
  {
    fprintf ( stderr, "Error: Could not mmap lock (%08X)\n", (uint32_t)SYNC_TEST_LOCK_BASE);
    return -2;
  }

  /* Initialization done by host */
  sync_test_lock_handle = sync_test_lock_mem;

  if ( !sync_test_lock_handle )
  {
    fprintf ( stderr, "Error: Could not initialize lock.\n");
    return -3;
  }

  return 0;
}

void sync_test_lock_destroy(void)
{
  if ( sync_test_lock_mem )
  {
    sync_test_lock_mem = NULL;
    sync_test_lock_handle = NULL;
  }
}

int32_t sync_test_lock(void)
{
  int32_t ret_val;

  ret_val = mpm_sync_lock_acquire( sync_test_lock_handle, DNUM + 1);

  sync_test_shmem->user[sync_test_shmem->num++] = DNUM + 1;

  ret_val = mpm_sync_lock_release( sync_test_lock_handle, DNUM + 1);

  return (ret_val);
}

void sync_test_print_results(void)
{
  int32_t i;

  for ( i = 0; i < sync_test_shmem->num; i++)
  {

    if ( sync_test_shmem->user[i] > SYNC_TEST_HOST_ID )
      printf("\t%2d : SLAVE %d\n", i, sync_test_shmem->user[i] - 1);
    else
      printf("\t%2d : HOST\n", i);

  }
}

volatile int32_t dsp_kick = 0;

int main(int argc, char **argv)
{
  int32_t num_dsps;

  if ( sync_test_transport_open() ) goto end;

  if ( sync_test_get_cfg(&num_dsps) ) goto end;

  if ( sync_test_shmem_create(num_dsps) ) goto end;

  if ( sync_test_barr_create(num_dsps) ) goto end;

  if ( sync_test_lock_create(num_dsps) ) goto end;

  if ( sync_test_barr() ) goto end;

  if ( sync_test_lock() ) goto end;

  if ( sync_test_barr() ) goto end;

  sync_test_print_results();

end:
  sync_test_barr_destroy();
  sync_test_lock_destroy();

  sync_test_shmem_destroy();
  sync_test_transport_close();

  return 0;
}

