#ifndef _SYNC_TEST_H
#define _SYNC_TEST_H

#define SYNC_TEST_HOST_ID 0

#define SYNC_TEST_TRANSPORT_SLAVE "dsp0"
#define SYNC_TEST_MAX_NUM_SLAVES  (8)

#define SYNC_TEST_CFG_BASE     0x00800000

#define SYNC_TEST_SHMEM_BASE   0xA0000000
#define SYNC_TEST_SHMEM_SIZE   0x00001000

#define SYNC_TEST_BARR_BASE    0xA0001000
#define SYNC_TEST_BARR_SIZE    0x00001000

#define SYNC_TEST_LOCK_BASE    0xA0002000
#define SYNC_TEST_LOCK_SIZE    0x00001000


typedef struct {
  int32_t num_dsps;
} sync_test_cfg_t;


typedef struct {
  int32_t num;
  int32_t user[SYNC_TEST_MAX_NUM_SLAVES + 1];
} sync_test_shmem_t;

#endif 

