#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <mpm_transport.h>
#include <mpm_sync.h>

#include "sync_test.h"

mpm_transport_h *sync_test_transport_handle;

sync_test_shmem_t *sync_test_shmem;

int32_t  sync_test_barr_size    = 0;
void    *sync_test_barr_mem     = NULL;
void    *sync_test_barr_handle  = NULL;

int32_t  sync_test_lock_size    = 0;
void    *sync_test_lock_mem     = NULL;
void    *sync_test_lock_handle  = NULL;


int32_t sync_test_transport_open(int32_t num_dsps)
{
  mpm_transport_open_t transport_open_cfg;
  int32_t i;

  sync_test_transport_handle = (mpm_transport_h *)malloc(num_dsps*sizeof(mpm_transport_h));
  if ( !sync_test_transport_handle )
  {
    fprintf( stderr, "Error: Could not malloc memory for transport handles.");
    return -1;
  }
  memset( sync_test_transport_handle, 0, num_dsps*sizeof(mpm_transport_h));

  transport_open_cfg.open_mode = (O_SYNC|O_RDWR);
  transport_open_cfg.msec_timeout = 10;

  for ( i = 0; i < num_dsps; i++)
  {
    char dsp_slave_name[16];

    sprintf( dsp_slave_name, "dsp%d", i);

    sync_test_transport_handle[i] = mpm_transport_open( dsp_slave_name, &transport_open_cfg);

    if ( !sync_test_transport_handle )
    {
      fprintf ( stderr, "Error: Could not obtain transport handle for \"%s\"\n", dsp_slave_name);
      return -2;
    }
  }

  return 0;
}

void sync_test_transport_close(int32_t num_dsps)
{
  int32_t i;

  if ( sync_test_transport_handle )
  {
    for ( i = 0; i < num_dsps; i++ )
    {
      if ( sync_test_transport_handle[i] )
      {
        mpm_transport_close(sync_test_transport_handle[i]);
        sync_test_transport_handle[i] = NULL;
      }
    }
    free ( sync_test_transport_handle );
  }
}

int32_t sync_test_slave_config(int32_t num_dsps)
{
  mpm_transport_write_t transport_write_cfg;
  sync_test_cfg_t test_cfg;
  int32_t i;

  test_cfg.num_dsps = num_dsps;

  for ( i = 0; i < num_dsps; i++)
  {
    if ( mpm_transport_write( sync_test_transport_handle[i], SYNC_TEST_CFG_BASE, sizeof(sync_test_cfg_t), (char *)&test_cfg, &transport_write_cfg) )
    {
      fprintf( stderr, "Error : Could not write config to slave %d.", i);
      break;
    }
  }

  if ( i < num_dsps )
  {
    fprintf( stderr, "Error : Could not write config to all slaves.");
    return -1;
  }

  return 0;
}

int32_t sync_test_shmem_create(int32_t num_dsps)
{
  mpm_transport_mmap_t transport_mmap_cfg;

  if ( sizeof(sync_test_shmem_t) > SYNC_TEST_SHMEM_SIZE )
  {
    fprintf( stderr, "Error: sync_test_shmem_t does not fit in memory map. (%d > %d)\n", sizeof(sync_test_shmem_t), SYNC_TEST_SHMEM_SIZE);
    return -1;
  }

  transport_mmap_cfg.mmap_prot = PROT_READ | PROT_WRITE;
  transport_mmap_cfg.mmap_flags = MAP_SHARED;

  sync_test_shmem = (sync_test_shmem_t *)mpm_transport_mmap( sync_test_transport_handle[0], SYNC_TEST_SHMEM_BASE, sizeof(sync_test_shmem_t), &transport_mmap_cfg);

  if ( !sync_test_shmem )
  {
    fprintf( stderr, "Error: Could not map shmem.\n");
    return -2;
  }

  memset( sync_test_shmem, 0, sizeof(sync_test_shmem_t));

  return 0;
}

void sync_test_shmem_destroy(void)
{
  if ( sync_test_shmem )
  {
    mpm_transport_munmap( sync_test_transport_handle[0], sync_test_shmem, sizeof(sync_test_shmem_t));
    sync_test_shmem = NULL;
  }
}


int32_t sync_test_barr_create(int32_t num_dsps)
{
  mpm_transport_mmap_t transport_mmap_cfg;

  sync_test_barr_size = mpm_sync_barr_get_sizes(num_dsps + 1);

  if ( sync_test_barr_size > SYNC_TEST_BARR_SIZE )
  {
    fprintf( stderr, "Error: Barrier does not fit into memory map. (%d > %d).\n", sync_test_barr_size, SYNC_TEST_BARR_SIZE);
    return -1;
  }

  transport_mmap_cfg.mmap_prot  = PROT_READ | PROT_WRITE;
  transport_mmap_cfg.mmap_flags = MAP_SHARED;

  sync_test_barr_mem = mpm_transport_mmap(sync_test_transport_handle[0], SYNC_TEST_BARR_BASE, sync_test_barr_size, &transport_mmap_cfg);

  if ( !sync_test_barr_mem ) 
  {
    fprintf ( stderr, "Error: Could not mmap barrier (%08X)\n", (uint32_t)SYNC_TEST_BARR_BASE);
    return -2;
  }

  sync_test_barr_handle = mpm_sync_barr_init( sync_test_barr_mem, num_dsps + 1 );

  if ( !sync_test_barr_handle )
  {
    fprintf ( stderr, "Error: Could not initialize barrier.\n");
    return -3;
  }

  return 0;
}


void sync_test_barr_destroy(void)
{
  if ( sync_test_barr_mem )
  {
    mpm_transport_munmap( sync_test_transport_handle[0], sync_test_barr_mem, sync_test_barr_size);
    sync_test_barr_mem = NULL;
    sync_test_barr_handle = NULL;
  }
}

int32_t sync_test_barr(void)
{
  int32_t ret_val;

  ret_val = mpm_sync_barr_wait( sync_test_barr_handle, SYNC_TEST_HOST_ID);

  return (ret_val);
}
  

int32_t sync_test_lock_create(int32_t num_dsps)
{
  mpm_transport_mmap_t transport_mmap_cfg;

  sync_test_lock_size = mpm_sync_lock_get_sizes(num_dsps + 1);

  if ( sync_test_lock_size > SYNC_TEST_LOCK_SIZE )
  {
    fprintf( stderr, "Error: Lock does not fit into memory map. (%d > %d).\n", sync_test_lock_size, SYNC_TEST_LOCK_SIZE);
    return -1;
  }

  transport_mmap_cfg.mmap_prot  = PROT_READ | PROT_WRITE;
  transport_mmap_cfg.mmap_flags = MAP_SHARED;

  sync_test_lock_mem = mpm_transport_mmap(sync_test_transport_handle[0], SYNC_TEST_LOCK_BASE, sync_test_lock_size, &transport_mmap_cfg);

  if ( !sync_test_lock_mem ) 
  {
    fprintf ( stderr, "Error: Could not mmap lock (%08X)\n", (uint32_t)SYNC_TEST_LOCK_BASE);
    return -2;
  }

  sync_test_lock_handle = mpm_sync_lock_init( sync_test_lock_mem, num_dsps + 1 );

  if ( !sync_test_lock_handle )
  {
    fprintf ( stderr, "Error: Could not initialize lock.\n");
    return -3;
  }

  return 0;
}

void sync_test_lock_destroy(void)
{
  if ( sync_test_lock_mem )
  {
    mpm_transport_munmap( sync_test_transport_handle[0], sync_test_lock_mem, sync_test_lock_size);
    sync_test_lock_mem = NULL;
    sync_test_lock_handle = NULL;
  }
}

int32_t sync_test_lock(void)
{
  int32_t ret_val;

  ret_val = mpm_sync_lock_acquire( sync_test_lock_handle, SYNC_TEST_HOST_ID);

  sync_test_shmem->user[sync_test_shmem->num++] = SYNC_TEST_HOST_ID;

  ret_val = mpm_sync_lock_release( sync_test_lock_handle, SYNC_TEST_HOST_ID);

  return (ret_val);
}

void sync_test_print_results(void)
{
  int32_t i;

  for ( i = 0; i < sync_test_shmem->num; i++)
  {

    if ( sync_test_shmem->user[i] > SYNC_TEST_HOST_ID )
      printf("\t%2d : SLAVE %d\n", i, sync_test_shmem->user[i] - 1);
    else
      printf("\t%2d : HOST\n", i);

  }
}


int main(int argc, char **argv)
{
  int32_t num_dsps;

  if ( argc != 2 )
  {
    printf("Usage: %s <num_threads>\n", argv[0]);
    return -1;
  }

  num_dsps = atoi(argv[1]);
  if ( num_dsps < 0 || num_dsps > 8 )
  {
    fprintf( stderr, "Error: num_dsps must be between 1 and 8\n");
    return -2;
  }

  if ( sync_test_transport_open(num_dsps) ) goto end;

  if ( sync_test_shmem_create(num_dsps) ) goto end;

  if ( sync_test_barr_create(num_dsps) ) goto end;

  if ( sync_test_lock_create(num_dsps) ) goto end;

  if ( sync_test_slave_config(num_dsps) ) goto end;

  if ( sync_test_barr() ) goto end;

  if ( sync_test_lock() ) goto end;

  if ( sync_test_barr() ) goto end;

  sync_test_print_results();

end:
  sync_test_barr_destroy();
  sync_test_lock_destroy();

  sync_test_shmem_destroy();
  sync_test_transport_close(num_dsps);

  return 0;
}

