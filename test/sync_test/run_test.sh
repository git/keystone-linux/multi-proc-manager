#!/bin/bash
i=0

while [ $i -lt 8 ]; do
	echo "dsp$i"
	mpmcl reset dsp$i
	let i=i+1
done

i=0

while [ $i -lt 8 ]; do
	echo "sync_test.out -> dsp$i"
	mpmcl load dsp$i ./c66x/bin/sync_test.out
	let i=i+1
done

i=0

while [ $i -lt 8 ]; do
	echo "dsp$i"
	mpmcl run dsp$i
	let i=i+1
done

sleep 1

./host/bin/sync_test 8


