/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __MPMCLIENT_H__
#define __MPMCLIENT_H__

#include <errno.h>

#define MPM_ERROR_NOK		1001
#define MPM_ERROR_FILENAME	1002
#define MPM_ERROR_SLAVE_NAME	1003
#define MPM_ERROR_SSM		1004
#ifdef __cplusplus
extern "C" {
#endif
/*
   ping MPM server
   return: <0 error, 0 OK
*/
int mpm_ping(void);

/*
   load image "file_name" to slave "slave_name" core's memory
   return: <0 error, 0 success
*/
int mpm_load(const char *slave_name, const char *file_name, int *error_code);

/*
    Preload image filename : "preload_file_name" and 
    then load image "file_name" to slave "slave_name" core's memory
    return: <0 error, 0 success
*/
int mpm_load_withpreload(const char *slave_name, const char *preload_file_name,
 const char *file_name, int *error_code);
/*
   run the slave "slave_name" core already preloaded and loaded
   return: <0 error, 0 success
*/
int mpm_run_withpreload(const char *slave_name, int *error_code);
/*
   run the slave "slave_name" core
   return: <0 error, 0 success
*/
int mpm_run(const char *slave_name, int *error_code);

typedef enum mpm_state {
	mpm_slave_state_idle,
	mpm_slave_state_reset,
	mpm_slave_state_loaded,
	mpm_slave_state_running,
	mpm_slave_state_crashed,
	mpm_slave_state_error,

	mpm_slave_state_undefined,
} mpm_slave_state_e;

/*
   return the state of slave "slave_name" core
   return: <0 error, 0 success
*/
int mpm_state(const char *slave_name, mpm_slave_state_e *state);

/*
   reset the slave "slave_name" core
   return: <0 error, 0 success
*/
int mpm_reset(const char *slave_name, int *error_code);

/*
   write coredump of "slave_name" to "file_name" file 
   return: <0 error, 0 success
*/
int mpm_coredump(const char *slave_name, const char *file_name, int *error_code);

/*
   Opens the transport profile associated with slave_name in the JSON file
   operation = [open|open_tx_only|close]
   return: <0 error, 0 success
*/
int mpm_transport(const char *slave_name, const char *operation, int *error_code);

#ifdef __cplusplus
}
#endif
#endif /* __MPMCLIENT_H__ */

