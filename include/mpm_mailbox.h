/*
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 
 * 
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef _MPM_MAILBOX_H
#define _MPM_MAILBOX_H

/* Mailbox is meant to exchange message between the Host and individual DSP 
   cores.  A mailbox is unidirectional either host -> DSP or DSP to Host */
/* Mailbox can also be used to send messages from DSP to DSP, but still each
 * mailbox will be unidirectional DSP1 -> DSP2 or DSP2 -> DSP1. Bidirectional
 * communication requires 2 mailboxes one on each direction.
 */

typedef struct mpm_mailbox_config_s
{
  uint32_t mem_start_addr;     /* Memory start address */
  uint32_t mem_size;           /* Size of memory space allocated for the mailbox */
  uint32_t max_payload_size;   /* Maximum Payload size */
} mpm_mailbox_config_t;

/**
 *  @brief Function mpm_mailbox_get_size() Get size needed for Mailbox instance
 *  @retval        size in bytes needed for mailbox instance
 *  @pre
 *  @post
 */
uint32_t mpm_mailbox_get_alloc_size(void);

/**
 *  @brief Function mpm_mailbox_get_mem_size() Get size needed for Mailbox memory
 *  @param[in]     max_payload_size   Maximum size of a mpm_mailbox message.
 *  @param[in]     mpm_mailbox_depth      Maximum number of messages that the 
 *                                    mpm_mailbox can hold at one time.
 *  @retval        size in bytes needed for mailbox memory
 *  @pre
 *  @post
 */
uint32_t mpm_mailbox_get_mem_size(uint32_t max_payload_size, uint32_t mpm_mailbox_depth);

/* LOCATION: Indicates whether the mailbox is located in the local or remote memory */
#define MPM_MAILBOX_MEMORY_LOCATION_LOCAL 0
#define MPM_MAILBOX_MEMORY_LOCATION_REMOTE 1

/* DIRECTION: Indicates from the local perspective whether the mailbox is used to
 * send messages or Receive messages
 */
#define MPM_MAILBOX_DIRECTION_RECEIVE  0
#define MPM_MAILBOX_DIRECTION_SEND     1

/**
 *  @brief Function mpm_mailbox_create() Creates a mpm_mailbox
 *  @param[out]    mpm_mailbox  Returned mailbox handle pointer
 *  @param[in]     remote_slave_name Slave name as found in mpm_config.json
 *  @param[in]     mem_location   memory location local or remote
 *  @param[in]     direction      send or receive
 *  @param[in]     mpm_mailbox_config MailBox configuration
 *  @retval        0 for success, -1 for failure
 *  @pre  
 *  @post 
 */
int32_t mpm_mailbox_create(void *mpm_mailbox, char *remote_slave_name,
  uint32_t mem_location, uint32_t direction, mpm_mailbox_config_t *mpm_mailbox_config);

/**
 *  @brief Function mpm_mailbox_open() Opens a mpm_mailbox; This is a blocking call, wait till the remote is ready
 *  @param[in]     mpm_mailbox  mpm_mailbox Handle
 *  @retval        0 for success, -1 for failure
 *  @pre
 *  @post
 */
int32_t mpm_mailbox_open(void *mpm_mailbox);

#define MPM_MAILBOX_ERR_FAIL          -1
#define MPM_MAILBOX_ERR_MAIL_BOX_FULL -2
#define MPM_MAILBOX_ERR_EMPTY         -3
#define MPM_MAILBOX_READ_ERROR        -4


/**
 *  @brief Function mpm_mailbox_write() Writes into a mpm_mailbox to deliver to remote : Non blocking call
 *  @param[in]     mpm_mailbox  mpm_mailbox Handle
 *  @param[in]     *buf          Mailbox Payload buffer pointer
 *  @param[in]     size          Mailbox Payload buffer size
 *  @param[in]     trans_id      transaction ID for the mail
 *  @retval        0 for success, -1 for failure -2 FULL
 *  @pre  
 *  @post 
 */
int32_t mpm_mailbox_write (void *mpm_mailbox, uint8_t *buf, uint32_t size, uint32_t trans_id);

/**
 *  @brief Function mpm_mailbox_read() Reads from a mpm_mailbox. This is a non blocking call
 *  @param[in]     mpm_mailbox  mpm_mailbox Handle
 *  @param[in]     *buf          Mailbox Payload buffer pointer
 *  @param[in]     *size         Mailbox Payload buffer size
 *  @param[in]     *trans_id     transaction ID for the mail
 *  @retval        0 for success, -1 for failure -3 Empty
 *  @pre  
 *  @post 
 */
int32_t mpm_mailbox_read (void *mpm_mailbox, uint8_t *buf, uint32_t *size, uint32_t *trans_id);

/**
 *  @brief Function mpm_mailbox_query() Polls mpm_mailboxes for any available messages. Non-blocking
 *  @param[in]     mpm_mailbox  mpm_mailbox Handle
 *  @retval        Number of messages in mailbox; negative error on failure
 *  @pre  
 *  @post 
 */
int32_t mpm_mailbox_query (void *mpm_mailbox);


#endif /* _MPM_MAILBOX_H */
