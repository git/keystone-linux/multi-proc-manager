#! /bin/sh

mpmsrv_daemon=/usr/bin/mpmsrv

test -x "$mpmsrv_daemon" || exit 0

case "$1" in
  start)
    echo -n "Starting mpmsrv daemon"
    start-stop-daemon --start --quiet --exec $mpmsrv_daemon
    echo "."
    ;;
  stop)
    echo -n "Stopping mpmsrv daemon"
    start-stop-daemon --stop --quiet --pidfile /var/run/mpm/pid
    echo "."
    ;;
  *)
    echo "Usage: /etc/init.d/mpmsrv-daemon.sh {start|stop}"
    exit 1
esac

exit 0
