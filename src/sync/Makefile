# Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
#
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#    Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

MPM_SYNC_OBJS = mpm_sync_barr.o mpm_sync_lock.o

MPM_SYNC_OBJS_SO = mpm_sync_barr.so mpm_sync_lock.so

MPM_SYNC_OBJS_C66X = mpm_sync_barr.oe66 mpm_sync_lock.oe66

MPM_SYNC_INC = -I. -I$(MPM_COMMON) -I$(MPM_INC)

CC_C66X = $(C6X_GEN_INSTALL_PATH)/bin/cl6x -c -mo -o3 -q -k -eo.oe66 -mv6600 --abi=eabi -fr=$(@D) -fs=$(@D)
AR_C66X = $(C6X_GEN_INSTALL_PATH)/bin/ar6x rq

.PHONY: all clean distclean

all: $(MPM_SYNC_LIB) $(MPM_SYNC_LIB_C66X)

$(MPM_LIB)/$(MPM_SYNC_LIB): $(MPM_SYNC_OBJS)
	$(AR) ${ARFLAGS} $@ $^

$(MPM_LIB)/$(MPM_SYNC_LIB_SO): $(MPM_SYNC_OBJS_SO)
	$(CC) -Wl,-soname=$(MPM_SYNC_LIB_SO).1 -shared -fPIC -o $(MPM_SYNC_LIB_SO).1.0.0 $^
	@ln -s $(MPM_SYNC_LIB_SO).1.0.0 $(MPM_SYNC_LIB_SO).1
	@ln -s $(MPM_SYNC_LIB_SO).1     $(MPM_SYNC_LIB_SO)
	@mv -f $(MPM_SYNC_LIB_SO)* $(MPM_LIB)

$(MPM_LIB_C66X)/$(MPM_SYNC_LIB_C66X): $(MPM_SYNC_OBJS_C66X)
	$(AR_C66X) $(ARFLAGS_C66X) $@ $^

%.o: %.c
	$(CC) $(MPMSRV_INC) $(MPM_SYNC_INC) -c -o $@ $<

%.so: %.c
	$(CC) $(MPMSRV_INC) $(MPM_SYNC_INC) -fPIC -c -o $@ $<

%.oe66: %.c
	$(CC_C66X) $(MPM_SYNC_INC) -I$(C6X_GEN_INSTALL_PATH)/include -c $<

clean: 
	rm -rf $(MPM_SYNC_OBJS) $(MPM_SYNC_OBJS_SO)
	rm -rf $(MPM_SYNC_OBJS_C66X) ./*.asm

