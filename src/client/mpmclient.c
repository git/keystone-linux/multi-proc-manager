/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <stdarg.h>
#include <limits.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>

#include "sockmsg.h"
#include "sockutils.h"
#include "mpmlog.h"
#include "mpmclient.h"

#define MPM_CLIENT_SOCKET_NAME_MAX_SIZE (128)
#define MPM_CLIENT_SOCKET_NAME_BASE "/tmp/mpm/mpm_client."

#define SOCK_TIMEOUT (60)

sock_h mpm_client_sock_open(void)
{
	sock_h h;
	sock_name_t sn;
	char rand_string[16];
	char sock_name[MPM_CLIENT_SOCKET_NAME_MAX_SIZE];

	srand(time(NULL));

	strncpy(sock_name, MPM_CLIENT_SOCKET_NAME_BASE, MPM_CLIENT_SOCKET_NAME_MAX_SIZE);
	snprintf(rand_string, 16, "%x", rand());
	strncat(sock_name, rand_string, MPM_CLIENT_SOCKET_NAME_MAX_SIZE - strlen(sock_name));

	sn.type = sock_name_e;
	sn.s.name = sock_name;

	h = sock_open(&sn);
	if (!h) {
		error_msg("socket open failed");
		return 0;
	}
	return h;
}

int mpm_client_sock_close(sock_h h)
{
	if (sock_close(h) < 0) {
		error_msg("socket close failed");
		return -EIO;
	}
	return 0;
}

int get_error_code(mpm_status_e s)
{
	if (s == mpm_status_ok)		return 0;
	if (s == mpm_status_nofile)	return -MPM_ERROR_FILENAME;
	if (s == mpm_status_invalid_slave_name)	return -MPM_ERROR_SLAVE_NAME;
	if (s == mpm_status_error_ssm)	return -MPM_ERROR_SSM;

	return -MPM_ERROR_NOK;
}

void mpm_log(const int loglevel, const char* functionName, const char* fileName, const int lineNo, const char* format, ...)
{
	va_list args;

	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);
	fprintf(stderr, "\n");
	fflush(stderr);
}

/*
	Note: This function will do a malloc for recv_data,
	user must to free it if the recv_data != NULL
*/

int send_n_recv_msg(char *send_data, int send_data_langth,
			char **recv_data, int *recv_data_langth)
{
	int length;
	sock_h h = 0;
	int retval = -ENOSYS;
	sock_name_t srv_name;
	struct timeval tv = {SOCK_TIMEOUT, 0};
	char mpm_daemon_socket_name[] = MPM_DAEMON_SOCKET_NAME;

	*recv_data = NULL;
	*recv_data_langth = 0;

	if (!send_data || !send_data_langth)
		return -1;

	h = mpm_client_sock_open();
	if (!h) {
		retval = -EIO;
		goto close_n_exit;
	}

	srv_name.type = sock_name_e;
	srv_name.s.name = mpm_daemon_socket_name;
	if (sock_send(h, send_data, send_data_langth, &srv_name) < 0) {
		retval = -EIO;
		goto close_n_exit;
	}

	retval = sock_wait(h, recv_data_langth, &tv, -1);
	if (retval == -2) {
		error_msg("Timeout in reading from socket");
		retval = -ETIME;
		goto close_n_exit;
	}

	if (*recv_data_langth < sizeof(server_to_client_msg_t)) {
		error_msg("Invalid length, expected minumum %d, received %d",
				sizeof(server_to_client_msg_t), *recv_data_langth);
		retval = -EIO;
		goto close_n_exit;
	}

	*recv_data = calloc(1, *recv_data_langth);
	if (!*recv_data) {
		retval = -ENOMEM;
		goto close_n_exit;
	}

	length = sock_recv(h, *recv_data, *recv_data_langth, NULL);
	if (length < *recv_data_langth) {
		error_msg("socket receive error");
		retval = -EIO;
		free(*recv_data);
		*recv_data = NULL;
		*recv_data_langth = 0;
		goto close_n_exit;
	}

	mpm_client_sock_close(h);
	retval = 0;

close_n_exit:
	return retval;

}

/*
   ping MPM server 
   return: <0 error, 0 OK
*/
int mpm_ping(void)
{
	int retval = -ENOSYS;
	int recv_data_langth;
	client_to_server_msg_t *c2s = NULL;
	server_to_client_msg_t *s2c = NULL;

	msg_alloc(c2s, 0);
	if (!c2s) {
		retval = -ENOMEM;
		goto close_n_exit;
	}
	c2s->cmd = mpm_cmd_ping;

	if (send_n_recv_msg((char *)c2s, msg_length(c2s),
			(char **)&s2c, &recv_data_langth)) {
		goto close_n_exit;
	}
	if (!s2c || s2c->status != mpm_status_ok) {
		error_msg("invalid status (%d) from daemon", s2c->status);
		retval = -ENOMSG;
		goto close_n_exit;
	}
	retval = (*((uint32_t *)msg_data(s2c)) == mpm_status_ok) ? 0 : -EIO;

close_n_exit:
	if(c2s) free(c2s);
	if(s2c) free(s2c);
	return retval;
}

/*
   load image "file_name" to slave "slave_name" core's memory
   return: <0 error, 0 success
*/
int mpm_load(const char *slave_name, const char *file_name, int *error_code)
{
	uint32_t *data32;
	int retval = -ENOSYS;
	int recv_data_langth;
	char *resolved_path = NULL;
	client_to_server_msg_t *c2s = NULL;
	server_to_client_msg_t *s2c = NULL;

	if (!slave_name || !file_name) {
		retval = -EINVAL;
		goto close_n_exit;
	}

	if (strlen(slave_name) > MPM_MAX_NAME_LENGTH - 1) {
		error_msg("slave name should be less than %d", MPM_MAX_NAME_LENGTH);
		retval = -EOVERFLOW;
		goto close_n_exit;
	}

	resolved_path = realpath(file_name, NULL);
	if (!resolved_path) {
		error_msg("realpath returned error %s", strerror(errno));
		retval = -ENOENT;
		goto close_n_exit;
	}

	msg_alloc(c2s, strlen(resolved_path) + 1);
	if (!c2s) {
		retval = -ENOMEM;
		goto close_n_exit;
	}
	c2s->cmd = mpm_cmd_load;
	strncpy(c2s->slave_name, slave_name, MPM_MAX_NAME_LENGTH - 1);
	strncpy(msg_data(c2s), resolved_path, strlen(resolved_path));

	if (send_n_recv_msg((char *)c2s, msg_length(c2s),
			(char **)&s2c, &recv_data_langth)) {
		goto close_n_exit;
	}
	if (!s2c || s2c->status != mpm_status_ok) {
		error_msg("invalid status (%d) from daemon", s2c->status);
		retval = -ENOMSG;
		goto close_n_exit;
	}
	data32 = (uint32_t *) msg_data(s2c);
	retval = get_error_code((mpm_status_e) data32[0]);
	if (retval && error_code) {
		*error_code = data32[1];
	}

close_n_exit:
	if(c2s) free(c2s);
	if(s2c) free(s2c);
	if (resolved_path) free(resolved_path);
	return retval;
}


/*
    Preload image filename : "preload_file_name" and 
    then load image "file_name" to slave "slave_name" core's memory
    return: <0 error, 0 success
*/
int mpm_load_withpreload(const char *slave_name, const char *preload_file_name,
 const char *file_name, int *error_code)
{
	uint32_t *data32;
	int retval = -ENOSYS;
	int recv_data_langth;
	char *resolved_path = NULL;
	char *resolved_preload_path = NULL;
	client_to_server_msg_t *c2s = NULL;
	server_to_client_msg_t *s2c = NULL;

	if (!slave_name || !file_name) {
		retval = -EINVAL;
		goto close_n_exit;
	}

	if (strlen(slave_name) > MPM_MAX_NAME_LENGTH - 1) {
		error_msg("slave name should be less than %d", MPM_MAX_NAME_LENGTH);
		retval = -EOVERFLOW;
		goto close_n_exit;
	}

	resolved_path = realpath(file_name, NULL);
	if (!resolved_path) {
		error_msg("realpath returned error %s", strerror(errno));
		retval = -ENOENT;
		goto close_n_exit;
	}

	resolved_preload_path = realpath(preload_file_name, NULL);
	if (!resolved_preload_path) {
		error_msg("realpath of preload image returned error %s", strerror(errno));
		retval = -ENOENT;
		goto close_n_exit;
	}

	msg_alloc(c2s, strlen(resolved_path) + strlen(resolved_preload_path) + 2);
	if (!c2s) {
		retval = -ENOMEM;
		goto close_n_exit;
	}
	c2s->cmd = mpm_cmd_load_withpreload;
	strncpy(c2s->slave_name, slave_name, MPM_MAX_NAME_LENGTH - 1);
	strncpy(msg_data(c2s), resolved_preload_path, strlen(resolved_preload_path));
	strncat(msg_data(c2s), " ",1);
	strncat(msg_data(c2s), resolved_path, strlen(resolved_path));

	if (send_n_recv_msg((char *)c2s, msg_length(c2s),
			(char **)&s2c, &recv_data_langth)) {
		goto close_n_exit;
	}
	if (!s2c || s2c->status != mpm_status_ok) {
		error_msg("invalid status (%d) from daemon", s2c->status);
		retval = -ENOMSG;
		goto close_n_exit;
	}
	data32 = (uint32_t *) msg_data(s2c);
	retval = get_error_code((mpm_status_e) data32[0]);
	if (retval && error_code) {
		*error_code = data32[1];
	}

close_n_exit:
	if(c2s) free(c2s);
	if(s2c) free(s2c);
	if (resolved_path) free(resolved_path);
	return retval;
}

/*
   run the slave "slave_name" core
   return: <0 error, 0 success
*/
int mpm_run(const char *slave_name, int *error_code)
{
	uint32_t *data32;
	int retval = -ENOSYS;
	int recv_data_langth;
	char *resolved_path = NULL;
	client_to_server_msg_t *c2s = NULL;
	server_to_client_msg_t *s2c = NULL;

	if (!slave_name) {
		retval = -EINVAL;
		goto close_n_exit;
	}

	if (strlen(slave_name) > MPM_MAX_NAME_LENGTH - 1) {
		error_msg("slave name should be less than %d", MPM_MAX_NAME_LENGTH);
		retval = -EOVERFLOW;
		goto close_n_exit;
	}

	msg_alloc(c2s, 0);
	if (!c2s) {
		retval = -ENOMEM;
		goto close_n_exit;
	}
	c2s->cmd = mpm_cmd_run;
	strncpy(c2s->slave_name, slave_name, MPM_MAX_NAME_LENGTH - 1);

	if (send_n_recv_msg((char *)c2s, msg_length(c2s),
			(char **)&s2c, &recv_data_langth)) {
		goto close_n_exit;
	}
	if (!s2c || s2c->status != mpm_status_ok) {
		error_msg("invalid status (%d) from daemon", s2c->status);
		retval = -ENOMSG;
		goto close_n_exit;
	}
	data32 = (uint32_t *) msg_data(s2c);
	retval = get_error_code((mpm_status_e) data32[0]);
	if (retval && error_code) {
		*error_code = data32[1];
	}

close_n_exit:
	if(c2s) free(c2s);
	if(s2c) free(s2c);
	return retval;

}

/*
   run the slave "slave_name" core already preloaded 
   return: <0 error, 0 success
*/
int mpm_run_withpreload(const char *slave_name, int *error_code)
{
	uint32_t *data32;
	int retval = -ENOSYS;
	int recv_data_langth;
	char *resolved_path = NULL;
	client_to_server_msg_t *c2s = NULL;
	server_to_client_msg_t *s2c = NULL;

	if (!slave_name) {
		retval = -EINVAL;
		goto close_n_exit;
	}

	if (strlen(slave_name) > MPM_MAX_NAME_LENGTH - 1) {
		error_msg("slave name should be less than %d", MPM_MAX_NAME_LENGTH);
		retval = -EOVERFLOW;
		goto close_n_exit;
	}

	msg_alloc(c2s, 0);
	if (!c2s) {
		retval = -ENOMEM;
		goto close_n_exit;
	}
	c2s->cmd = mpm_cmd_run_withpreload;
	strncpy(c2s->slave_name, slave_name, MPM_MAX_NAME_LENGTH - 1);

	if (send_n_recv_msg((char *)c2s, msg_length(c2s),
			(char **)&s2c, &recv_data_langth)) {
		goto close_n_exit;
	}
	if (!s2c || s2c->status != mpm_status_ok) {
		error_msg("invalid status (%d) from daemon", s2c->status);
		retval = -ENOMSG;
		goto close_n_exit;
	}
	data32 = (uint32_t *) msg_data(s2c);
	retval = get_error_code((mpm_status_e) data32[0]);
	if (retval && error_code) {
		*error_code = data32[1];
	}

close_n_exit:
	if(c2s) free(c2s);
	if(s2c) free(s2c);
	return retval;

}

/*
   return the state of slave "slave_name" core
   return: <0 error, 0 success
*/
int mpm_state(const char *slave_name, mpm_slave_state_e *state)
{
	int retval = -ENOSYS;
	int recv_data_langth;
	uint32_t *data32;
	char *resolved_path = NULL;
	client_to_server_msg_t *c2s = NULL;
	server_to_client_msg_t *s2c = NULL;

	if (!slave_name) {
		retval = -EINVAL;
		goto close_n_exit;
	}

	if (strlen(slave_name) > MPM_MAX_NAME_LENGTH - 1) {
		error_msg("slave name should be less than %d", MPM_MAX_NAME_LENGTH);
		retval = -EOVERFLOW;
		goto close_n_exit;
	}

	msg_alloc(c2s, 0);
	if (!c2s) {
		retval = -ENOMEM;
		goto close_n_exit;
	}
	c2s->cmd = mpm_cmd_state;
	strncpy(c2s->slave_name, slave_name, MPM_MAX_NAME_LENGTH - 1);

	if (send_n_recv_msg((char *)c2s, msg_length(c2s),
			(char **)&s2c, &recv_data_langth)) {
		goto close_n_exit;
	}
	if (!s2c || s2c->status != mpm_status_ok) {
		error_msg("invalid status (%d) from daemon", s2c->status);
		retval = -ENOMSG;
		goto close_n_exit;
	}
	data32 = (uint32_t *) msg_data(s2c);

	retval = get_error_code((mpm_status_e) data32[0]);
	if (retval < 0)
		goto close_n_exit;

	switch(data32[1]) {
		case idle_state:
			*state = mpm_slave_state_idle;
			break;
		case reset_state:
			*state = mpm_slave_state_reset;
			break;
		case loaded_state:
			*state = mpm_slave_state_loaded;
			break;
		case running_state:
			*state = mpm_slave_state_running;
			break;
		case crashed_state:
			*state = mpm_slave_state_crashed;
			break;
		case error_state:
			*state = mpm_slave_state_error;
			break;
		default:
			*state = mpm_slave_state_undefined;
			break;
	}
close_n_exit:
	if(c2s) free(c2s);
	if(s2c) free(s2c);
	return retval;
}

/*
   reset the slave "slave_name" core
   return: <0 error, 0 success
*/
int mpm_reset(const char *slave_name, int *error_code)
{
	uint32_t *data32;
	int retval = -ENOSYS;
	int recv_data_langth;
	char *resolved_path = NULL;
	client_to_server_msg_t *c2s = NULL;
	server_to_client_msg_t *s2c = NULL;

	if (!slave_name) {
		retval = -EINVAL;
		goto close_n_exit;
	}

	if (strlen(slave_name) > MPM_MAX_NAME_LENGTH - 1) {
		error_msg("slave name should be less than %d", MPM_MAX_NAME_LENGTH);
		retval = -EOVERFLOW;
		goto close_n_exit;
	}

	msg_alloc(c2s, 0);
	if (!c2s) {
		retval = -ENOMEM;
		goto close_n_exit;
	}
	c2s->cmd = mpm_cmd_reset;
	strncpy(c2s->slave_name, slave_name, MPM_MAX_NAME_LENGTH - 1);

	if (send_n_recv_msg((char *)c2s, msg_length(c2s),
			(char **)&s2c, &recv_data_langth)) {
		goto close_n_exit;
	}
	if (!s2c || s2c->status != mpm_status_ok) {
		error_msg("invalid status (%d) from daemon", s2c->status);
		retval = -ENOMSG;
		goto close_n_exit;
	}
	data32 = (uint32_t *) msg_data(s2c);
	retval = get_error_code((mpm_status_e) data32[0]);
	if (retval && error_code) {
		*error_code = data32[1];
	}

close_n_exit:
	if(c2s) free(c2s);
	if(s2c) free(s2c);
	return retval;
}

/*
   write coredump of "slave_name" to "file_name" file 
   return: <0 error, 0 success
*/
int mpm_coredump(const char *slave_name, const char *file_name, int *error_code)
{
	char *p;
	char *datap;
	size_t length = 0;
	uint32_t *data32;
	int retval = -ENOSYS;
	int recv_data_langth;
	char *resolved_path = NULL;
	client_to_server_msg_t *c2s = NULL;
	server_to_client_msg_t *s2c = NULL;

	if (!slave_name || !file_name) {
		retval = -EINVAL;
		goto close_n_exit;
	}

	if (strlen(slave_name) > MPM_MAX_NAME_LENGTH - 1) {
		error_msg("slave name should be less than %d", MPM_MAX_NAME_LENGTH);
		retval = -EOVERFLOW;
		goto close_n_exit;
	}

	p = strrchr(file_name, '/');
	if (!p) {
		resolved_path = (char *) getcwd(NULL, length);
		if (!resolved_path) {
			error_msg("getcwd returned error %s", strerror(errno));
			retval = -ENOENT;
			goto close_n_exit;
		}
		msg_alloc(c2s, strlen(resolved_path) + 1 + strlen(file_name) + 1);
		if (!c2s) {
			retval = -ENOMEM;
			goto close_n_exit;
		}
		datap = msg_data(c2s);
		strncpy(datap, resolved_path, strlen(resolved_path));
		datap[strlen(resolved_path)] = '/';
		strncpy(&datap[strlen(resolved_path) + 1], file_name, strlen(file_name));
	} else {
		if (file_name == p) {
			msg_alloc(c2s, strlen(file_name) + 1);
			if (!c2s) {
				retval = -ENOMEM;
				goto close_n_exit;
			}
			datap = msg_data(c2s);
			strncpy(datap, file_name, strlen(file_name));

		} else {
			p[0] = 0;
			resolved_path = realpath(file_name, NULL);
			if (!resolved_path) {
				error_msg("realpath returned error %s for %s", strerror(errno),
					file_name);
				retval = -ENOENT;
				goto close_n_exit;
			}
			msg_alloc(c2s, strlen(resolved_path) + 1 + strlen(&p[1]) + 1);
			if (!c2s) {
				retval = -ENOMEM;
				goto close_n_exit;
			}
			datap = msg_data(c2s);
			strncpy(datap, resolved_path, strlen(resolved_path));
			datap[strlen(resolved_path)] = '/';
			strncpy(&datap[strlen(resolved_path) + 1], &p[1], strlen(&p[1]));
		}
	}
	c2s->cmd = mpm_cmd_coredump;
	strncpy(c2s->slave_name, slave_name, MPM_MAX_NAME_LENGTH - 1);

	if (send_n_recv_msg((char *)c2s, msg_length(c2s),
			(char **)&s2c, &recv_data_langth)) {
		goto close_n_exit;
	}
	if (!s2c || s2c->status != mpm_status_ok) {
		error_msg("invalid status (%d) from daemon", s2c->status);
		retval = -ENOMSG;
		goto close_n_exit;
	}
	data32 = (uint32_t *) msg_data(s2c);
	retval = get_error_code((mpm_status_e) data32[0]);
	if (retval && error_code) {
		*error_code = data32[1];
	}

close_n_exit:
	if(c2s) free(c2s);
	if(s2c) free(s2c);
	if (resolved_path) free(resolved_path);
	return retval;

}

/*
   Opens the transport profile associated with slave_name in the JSON file
   operation = [open|close]
   return: <0 error, 0 success
*/
int mpm_transport(const char *slave_name, const char *operation, int *error_code)
{
	uint32_t *data32;
	int retval = -ENOSYS;
	int recv_data_langth;
	char *resolved_op = NULL;
	client_to_server_msg_t *c2s = NULL;
	server_to_client_msg_t *s2c = NULL;

	if (!slave_name || !operation) {
		retval = -EINVAL;
		goto close_n_exit;
	}

	if (strcmp(operation, "open")==0)
		resolved_op = "open";
	else if (strcmp(operation, "open_tx_only")==0)
		resolved_op = "open_tx_only";
	else if (strcmp(operation, "close")==0)
		resolved_op = "close";
	else {
		retval = -EINVAL;
		goto close_n_exit;
	}

	if (strlen(slave_name) > MPM_MAX_NAME_LENGTH - 1) {
		error_msg("slave name should be less than %d", MPM_MAX_NAME_LENGTH);
		retval = -EOVERFLOW;
		goto close_n_exit;
	}

	msg_alloc(c2s, strlen(resolved_op) + 1);
	if (!c2s) {
		retval = -ENOMEM;
		goto close_n_exit;
	}
	c2s->cmd = mpm_cmd_transport;
	strncpy(c2s->slave_name, slave_name, MPM_MAX_NAME_LENGTH - 1);
	strncpy(msg_data(c2s), resolved_op, strlen(resolved_op));

	if (send_n_recv_msg((char *)c2s, msg_length(c2s),
			(char **)&s2c, &recv_data_langth)) {
		goto close_n_exit;
	}
	if (!s2c || s2c->status != mpm_status_ok) {
		error_msg("invalid status (%d) from daemon", s2c->status);
		retval = -ENOMSG;
		goto close_n_exit;
	}
	data32 = (uint32_t *) msg_data(s2c);
	retval = get_error_code((mpm_status_e) data32[0]);
	if (retval && error_code) {
		*error_code = data32[1];
	}

close_n_exit:
	if(c2s) free(c2s);
	if(s2c) free(s2c);
	return retval;
}
