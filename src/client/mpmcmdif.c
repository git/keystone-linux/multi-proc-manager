/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <string.h>
#include "mpmclient.h"

void print_help(char *progname)
{
	printf("Usage: %s <command> [slave name] [options]\n", progname);
	printf("Multiproc manager CLI to manage slave processors\n");
	printf("\n");
	printf("  <command>           Commands for the slave processor\n");
	printf("                      Supported commands: ping, load, run, reset, status, coredump, transport,\n");
	printf("                                          load_withpreload, run_withpreload\n");
	printf("  [slave name]        Name of the slave processor as specified in MPM config file\n");
	printf("  [options]           In case of load, the option field need to have image file name\n");
	printf("                      In case of transport, the option is to open or close\n");
	printf("\n");
	printf("Example:\n");
	printf("   %s load dsp0 SolveUltimateQuestion.out\n", progname);
	printf("   %s run dsp0\n", progname);
	printf("   The above commands will load and run the slave core (dsp0)\n");

}

int main(int argc, char *argv[])
{
	int retval;
	int error_code = 0;

	if (argc < 2) {
		print_help(argv[0]);
		return -1;
	}

	if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
		print_help(argv[0]);
		return -1;
	}

	if (!strcmp(argv[1], "ping")) {
		if (mpm_ping() < 0) {
			printf("ping failed\n");
			return -1;
		}
		printf("ping succeeded\n");
		return 0;
	}

	if (!strcmp(argv[1], "load")) {
		if (argc < 4) {
			printf("insufficient parameters for load\n");
			print_help(argv[0]);
			return -1;
		}
		retval = mpm_load(argv[2], argv[3], &error_code);
		if ( retval < 0) {
			printf("load failed (error: %d)\n", error_code);
			return -1;
		}
		printf("load succeeded\n");
		return 0;
	}

	if (!strcmp(argv[1], "load_withpreload")) {
		if (argc < 5) {
			printf("insufficient parameters for load_with preload\n");
			print_help(argv[0]);
			return -1;
		}
		retval = mpm_load_withpreload(argv[2], argv[3], argv[4], &error_code);
		if ( retval < 0) {
			printf("load with preload failed (error: %d)\n", error_code);
			return -1;
		}
		printf("load with preload succeeded\n");
		return 0;
	}

	if (!strcmp(argv[1], "run")) {
		if (argc < 3) {
			printf("insufficient parameters for run\n");
			print_help(argv[0]);
			return -1;
		}
		retval = mpm_run(argv[2], &error_code);
		if (retval < 0) {
			printf("run failed (error: %d)\n", error_code);
			return -1;
		}
		printf("run succeeded\n");
		return 0;
	}

	if (!strcmp(argv[1], "run_withpreload")) {
		if (argc < 3) {
			printf("insufficient parameters for run\n");
			print_help(argv[0]);
			return -1;
		}
		retval = mpm_run_withpreload(argv[2], &error_code);
		if (retval < 0) {
			printf("run failed (error: %d)\n", error_code);
			return -1;
		}
		printf("run succeeded\n");
		return 0;
	}

	if (!strcmp(argv[1], "reset")) {
		if (argc < 3) {
			printf("insufficient parameters for reset\n");
			print_help(argv[0]);
			return -1;
		}
		retval = mpm_reset(argv[2], &error_code);
		if ( retval < 0) {
			printf("reset failed (error: %d)\n", error_code);
			return -1;
		}
		printf("reset succeeded\n");
		return 0;
	}

	if (!strcmp(argv[1], "status")) {
		mpm_slave_state_e state = mpm_slave_state_undefined;
		if (argc < 3) {
			printf("insufficient parameters for status\n");
			print_help(argv[0]);
			return -1;
		}
		retval = mpm_state(argv[2], &state);
		if ( retval < 0) {
			printf("status failed\n");
			return -1;
		}

		switch (state) {
			case mpm_slave_state_idle:
				printf("%s is in idle state\n", argv[2]);
				break;
			case mpm_slave_state_reset:
				printf("%s is in reset state\n", argv[2]);
				break;
			case mpm_slave_state_loaded:
				printf("%s is in loaded state\n", argv[2]);
				break;
			case mpm_slave_state_running:
				printf("%s is in running state\n", argv[2]);
				break;
			case mpm_slave_state_crashed:
				printf("%s is in crashed state\n", argv[2]);
				break;
			case mpm_slave_state_error:
				printf("%s is in error state\n", argv[2]);
				break;
			default:
				printf("%s is in unknown state\n", argv[2]);
				break;
		}
		return 0;
	}

	if (!strcmp(argv[1], "coredump")) {
		if (argc < 4) {
			printf("insufficient parameters for load\n");
			print_help(argv[0]);
			return -1;
		}
		retval = mpm_coredump(argv[2], argv[3], &error_code);
		if ( retval < 0) {
			printf("coredump failed (error: %d)\n", error_code);
			return -1;
		}
		printf("coredump succeeded\n");
		return 0;
	}

	if (!strcmp(argv[1], "transport")) {
		if (argc < 4) {
			printf("insufficient parameters for transport\n");
			print_help(argv[0]);
			return -1;
		}
		retval = mpm_transport(argv[2], argv[3], &error_code);
		if ( retval < 0) {
			printf("transport %s failed (error: %d) retval %d\n", argv[2], error_code, retval);
			return -1;
		}
		printf("transport %s succeeded for %s\n", argv[3], argv[2]);
		return 0;
	}

	printf("invalid arguments\n");

	return 0;
}

