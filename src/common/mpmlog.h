/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __MPMLOG_H__
#define __MPMLOG_H__

#include <syslog.h>

/* the log levels */
#define mpmerror	1
#define mpmwarning	2
#define mpminfo		3
#define mpmdebug	4

/* set the default loglevel if none is set */
#ifndef mpmloglevel
#define mpmloglevel	mpminfo
#endif

/* logging errors */
#if mpmloglevel >= mpmerror
#define error_msg(...) mpm_log(LOG_ERR, __func__, __FILE__, __LINE__, __VA_ARGS__);
#else
#define error_msg(...)
#endif

/* logging warnings */
#if mpmloglevel >= mpmwarning
#define warning_msg(...) mpm_log(LOG_WARNING, __func__, __FILE__, __LINE__, __VA_ARGS__);
#else
#define warning_msg(...)
#endif

/* logging information */
#if mpmloglevel >= mpminfo
#define info_msg(...) mpm_log(LOG_INFO, __func__, __FILE__, __LINE__, __VA_ARGS__);
#else
#define info_msg(...)
#endif

/* logging debug information */
#if mpmloglevel >= mpmdebug
#define debug_msg(...) mpm_log(LOG_DEBUG, __func__, __FILE__, __LINE__, __VA_ARGS__);
#else
#define debug_msg(...)
#endif

void mpm_log (const int loglevel, const char* functionName,
	const char* fileName, const int lineNo, const char* format, ...);

#endif

