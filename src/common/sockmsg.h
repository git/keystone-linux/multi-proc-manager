/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __SOCKMSG_H__
#define __SOCKMSG_H__

#include <stdint.h>

#define MPM_DAEMON_SOCKET_NAME "/var/run/mpm/mpm_daemon"
#define MPM_MAX_NAME_LENGTH	32

/* Slave processor States */
typedef enum mpm_state_tag {
	idle_state,
	reset_state,
	loaded_state,
	running_state,
	crashed_state,
	error_state,

	last_state
} mpm_state_e;

typedef enum mpm_cmd {
	mpm_cmd_ping,
	mpm_cmd_load,
	mpm_cmd_run,
	mpm_cmd_reset,
	mpm_cmd_state,
	mpm_cmd_coredump,
	mpm_cmd_crash,
	mpm_cmd_transport,
	mpm_cmd_load_withpreload,
	mpm_cmd_run_withpreload
} mpm_cmd_e;

typedef enum mpm_status {
	mpm_status_ok = 1,
	mpm_status_nok = -2,
	mpm_status_nofile = -3,
	mpm_status_invalid_slave_name = -4,
	mpm_status_error_ssm = -5,
} mpm_status_e;

typedef struct client_to_server_msg {
	uint32_t	message_id;
	mpm_cmd_e	cmd;
	char		slave_name[MPM_MAX_NAME_LENGTH];
	int		length;
} client_to_server_msg_t;

typedef struct server_to_client_msg {
	uint32_t	message_id;
	mpm_status_e	status;
	int		length;
} server_to_client_msg_t;

#define msg_alloc(p, l) \
	do { \
		p = calloc(1, sizeof(*p) + l); \
		if (p) { \
			p->length = l; \
		} \
	} while (0)
		
#define msg_length(x) ((x) ? (sizeof(*x) + x->length) : 0)
#define msg_data(x) ((x->length) ? ((char *)x + sizeof(*x)) : NULL)

#endif /* __SOCKMSG_H__ */
