/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <strings.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <getopt.h>

#include <libdaemon/daemon.h>

#include "cJSON/cJSON.h"
#include "mpmloc.h"
#include "mpmdb.h"
#include <ti/cmem.h>

#define DEFAULT_CONFIG_FILE_NAME "/etc/mpm/mpm_config.json"
#define MAX_LINE_LENGTH 512

#define MAX_LOG_LEN (4096)
static char mpm_log_buf[MAX_LOG_LEN];

mpm_cfg_t mpm_cfg;
uint32_t mpm_errno = 0;

#define DEFAULT_SOCKET_PERM (0700)

#define MPM_SCRATCH_LENGTH 0x400

CMEM_AllocParams mpm_cmem_alloc_params = {
	CMEM_HEAP,	/* type */
	CMEM_NONCACHED,	/* flags */
	0x400		/* alignment */
};

int json_check_version (cJSON *json);
#if 0
int json_get_inputif (cJSON *json);
#endif
int json_set_outputif (cJSON *json);
int json_get_sockperm (cJSON *json);
int json_get_segments (cJSON *json);
int json_get_slaves (cJSON *json);
int json_get_cmdfile (cJSON *json);
#if 0
int json_get_scratchinfo(cJSON *json);
#endif

int mpm_process_cmdfile(void)
{
	mpm_slave_t *slavep;
	FILE *fpr = NULL;
	char *line = NULL;
	int retval = -1;
	int lineno = 0;
	char *tok;
	char *tok1, *tok2;
	int i;

	if (!mpm_cfg.cmdfile) {
		return 0;
	}

	fpr = fopen(mpm_cfg.cmdfile, "r");
	if (!fpr){
		error_msg("Error opening file %s (error: %s)",
				mpm_cfg.cmdfile, strerror(errno));
		goto close_n_exit;
	}

	line = malloc(MAX_LINE_LENGTH);
	if (!line) {
		error_msg("Error allocating memory %d (error: %s)",
				MAX_LINE_LENGTH, strerror(errno));
		goto close_n_exit;
	}

	while (fgets(line, MAX_LINE_LENGTH, fpr)) {

		lineno++;

		if(line[0] == '#') continue;

		tok = strtok (line, " \t\n");
		if (!tok) continue;

		find_slave_p(tok, slavep);
		if (!slavep) {
			error_msg("No slave processor found with name %s in line %d",
					tok, lineno);
			error_msg("Number in the list:  %d", mpm_cfg.num_slaves);
			error_msg("Slave processors in list: 0 %s", mpm_slave[0].cfgp->name);
			goto close_n_exit;
		}

		tok = strtok (NULL, " \t\n");
		if (!tok) {
			error_msg("Need a valid command for the line %d", lineno);
			goto close_n_exit;
		}

		if (!strcmp (tok, "load")) {
			tok = strtok (NULL, " \t\n");
			if (!tok) {
				error_msg("Image file name not present for load command in line %d",
						lineno);
				goto close_n_exit;
			}
			info_msg("loading %s with %s", slavep->cfgp->name, tok);
			mpm_ssm_event (slavep, load_ev, tok);
		} else if (!strcmp (tok, "run")) {
			info_msg("running %s", slavep->cfgp->name);
			mpm_ssm_event (slavep, run_ev, NULL);
		} else if (!strcmp (tok, "load_withpreload")) {
			tok = strtok (NULL, " \t\n");
			if (!tok) {
				error_msg("Preload Image file name not present for load command in line %d",
						lineno);
				goto close_n_exit;
			}
			tok1 = tok;
			tok = strtok (NULL, " \t\n");
			if (!tok) {
				error_msg("Load Image file name not present for load command in line %d",
						lineno);
				goto close_n_exit;
			}
			/* replace null string with space */
			tok2 = tok1+strlen(tok1);
			*tok2++ = ' ';
			strncpy(tok2, tok, strlen(tok));
			info_msg("loading with preload %s with %s", slavep->cfgp->name, tok1);
			mpm_ssm_event (slavep, load_withpreload_ev, tok1);
		} else if (!strcmp (tok, "run_withpreload")) {
			info_msg("running with preload %s", slavep->cfgp->name);
			mpm_ssm_event (slavep, run_withpreload_ev, NULL);
		} else if (!strcmp (tok, "reset")) {
			info_msg("resetting %s", slavep->cfgp->name);
			mpm_ssm_event (slavep, reset_ev, NULL);
		} else {
			error_msg("Invalid command %s in line %d",
					tok, lineno);
			goto close_n_exit;
		}
	}

	retval = 0;

close_n_exit:
	if (fpr) fclose(fpr);
	if (line) free(line);

	return retval;
}

int mpm_parse(char *file)
{
	FILE *fpr;
	char *config_buf = NULL;
	struct stat buf;
	int flen;
	int res;
	cJSON *json = NULL;
	int retval = -1;

	fpr = fopen(file, "r");
	if (!fpr){
		error_msg("Error opening file %s (error: %s)",
				file, strerror(errno));
		goto close_n_exit;
	}

	fstat(fileno(fpr), &buf);
	flen = buf.st_size;

	config_buf = (char *) malloc(flen + 1);
	if (!config_buf) {
		error_msg("Error in allocating cfg buffer (error: %s)",
				strerror(errno));
		goto close_n_exit;
	}
	config_buf[flen] = 0;

	res = fread (config_buf, 1, flen, fpr);
	if (res != flen) {
		error_msg("Error in reading cfg file (error: %s)",
				strerror(errno));
		goto close_n_exit;
	}

	json = cJSON_Parse(config_buf);
	if (!json) {
		error_msg("JSON parser error before: [%s]",cJSON_GetErrorPtr());
		goto close_n_exit;
	}
	debug_msg("cJSON_Parse Complete");

	if (json_check_version(json)) goto close_n_exit;
#if 0
	if (json_get_inputif(json)) goto close_n_exit;
#endif
	json_set_outputif(json);
	json_get_cmdfile(json);
	json_get_sockperm (json);
#if 0
	if (json_get_scratchinfo(json)) goto close_n_exit;
#endif
	if (json_get_segments(json)) goto close_n_exit;
	if (json_get_slaves(json)) goto close_n_exit;

	retval = 0;

close_n_exit:
	if(json) cJSON_Delete(json);
	if (config_buf) free(config_buf);
	if (fpr) fclose(fpr);

	return retval;
}

int mpm_initialize_scratch(void)
{
	void *vaddr;
	off_t paddr;

	if ( mpm_cfg.scratch_block_cfg.alias == 0) {
		CMEM_init();
		mpm_cfg.scratch_block_cfg.length = MPM_SCRATCH_LENGTH;
		vaddr = CMEM_alloc2(CMEM_CMABLOCKID, mpm_cfg.scratch_block_cfg.length,
				    &mpm_cmem_alloc_params);
		if ( vaddr == NULL ){
			error_msg("Error allocating CMEM buffer for Scratch of size",
				  mpm_cfg.scratch_block_cfg.length);
			return -1;
		}

		mpm_cfg.scratch_block_cfg.vaddr = vaddr;
		paddr = CMEM_getPhys(vaddr);

		if (( paddr >= 0x800000000ULL ) && ( paddr < 0x880000000ULL )) {
			/* Convert to DSP address based on default mapping */
			mpm_cfg.scratch_block_cfg.alias = (uint32_t)(paddr - 0x780000000ULL);
			mpm_cfg.scratch_block_cfg.paddr = paddr;
		} else {
			error_msg("Physical address %llx not within default mpax map",
				  paddr);
			return -1;
		}

		debug_msg("Scratch Physical address %llx; DSP address %x, length %x",
			  mpm_cfg.scratch_block_cfg.paddr,
			  mpm_cfg.scratch_block_cfg.alias,
			  mpm_cfg.scratch_block_cfg.length);
	}
	return 0;
}

int mpm_start_server(void)
{
	int retval = 0;
	pthread_t tid;
	pthread_attr_t attr;

	debug_msg("Starting mpm server");

	memset (&mpm_cfg, 0, sizeof(mpm_cfg_t));

	retval = mpm_parse(DEFAULT_CONFIG_FILE_NAME);

	if (retval) {
		error_msg("Error in config file parsing %d", retval);
		goto close_n_exit;
	}

	debug_msg("MPM parsing complete");

	retval = mpm_initialize_scratch();
	if ( retval ) {
		error_msg("Error in initializing scratch %d", retval);
		goto close_n_exit;
	}

	retval = mpm_ssm_init();

	if (retval) {
		error_msg("Error in starting slave state machine");
		goto close_n_exit;
	}
	debug_msg("MPM ssm init");

	retval = pthread_create(&tid, NULL, mpm_monitor, NULL);

	if (retval) {
		error_msg("Error:unable to create thread %d (err: %s)",
			retval, strerror(errno));
		goto close_n_exit;
	}

	/* Check and populate security device/mode */
	mpm_cfg.security_status = mpm_check_chip_security();
	if (mpm_cfg.security_status < 0) {
		error_msg("Error:unable to get security status");
		goto close_n_exit;		
	}

	retval = mpm_process_cmdfile();

	if (retval) {
		error_msg("Error in mpm_process_cmdfile");
		goto close_n_exit;
	}
	debug_msg("MPM process cmd file complete");

	retval = mpm_server();
	if (retval) {
		error_msg("Error in mpm_server");
		goto close_n_exit;
	}

	retval = mpm_ssm_close();

	if (retval) {
		error_msg("Error closing state machine");
		goto close_n_exit;
	}

close_n_exit:
	return (0);
}

void mpm_cleanup()
{
	/* Free scratch buffers */
	if(mpm_cfg.scratch_block_cfg.vaddr) {
		CMEM_free(mpm_cfg.scratch_block_cfg.vaddr, &mpm_cmem_alloc_params);
		debug_msg("Freed Physical address %llx; DSP address %x, length %x",
			  mpm_cfg.scratch_block_cfg.paddr,
			  mpm_cfg.scratch_block_cfg.alias,
			  mpm_cfg.scratch_block_cfg.length);
	}

	/* Clean up all mallocs and open files */
	if (mpm_cfg.logfile_p && mpm_cfg.outputif == log_file) {
		fclose(mpm_cfg.logfile_p);
	}
	if (mpm_cfg.cmdfile) free(mpm_cfg.cmdfile);
}

char *get_pid_file_name(void) {
	static char pid_file_name[] = MPM_DAEMON_PID_FILE_NAME;

	return pid_file_name;
}

int main(int argc, char *argv[])
{
	int rc, recvBufLength, dataLen;
	int retVal, signal;
	int index, argIndex = 0;
	int kill = 0, daemonize = 1;
	pid_t pid;

	const struct option longopts[] =
	{
		{"nodaemon",	no_argument,	0,	'n'},
		{"kill",	no_argument,	0,	'k'},
		{"help",	no_argument,	0,	'h'},
		{0,0,0,0},
	};

	mpm_cfg.outputif = log_daemon;

	opterr = 1;

	while(argIndex != -1)
	{
		argIndex = getopt_long(argc, argv, "nkh", longopts, &index);
		switch (argIndex)
		{
			case 'n':
				daemonize = 0;
				break;
			case 'k':
				kill = 1;
				break;
			case 'h':
			default:
				printf ("Usage: argv[0] [OPTION]...\n\n"
						" Following are the command line options\n\n"
						" -n, --nodaemon\t do not daemonize, run in foreground\n"
						" -k, --kill\t kill the existing daemon\n"
						" -h, --help\t print this message\n"
						"\n In default mode it will run as a daemon.\n\n");
				return 0;

			case -1:
				break;
		}
	}

	if (daemonize) {

		debug_msg("Starting %s", argv[0]);

		/* Reset signal handlers */
		if (daemon_reset_sigs(-1) < 0) {
			error_msg("Failed to reset all signal handlers: %s", strerror(errno));
			return 1;
		}

		/* Unblock signals */
		if (daemon_unblock_sigs(-1) < 0) {
			error_msg("Failed to unblock all signals: %s", strerror(errno));
			return 1;
		}

		if (check_and_create_path (get_pid_file_name()) < 0) {
			error_msg("Failed to create pid file path: %s", get_pid_file_name());
			return 1;
		}

		/* set daemon id string */
		daemon_log_ident = daemon_ident_from_argv0(argv[0]);
		daemon_pid_file_proc = (daemon_pid_file_proc_t) get_pid_file_name;

		if (kill) {
			if (daemon_pid_file_kill_wait(SIGTERM, 5) < 0)
				error_msg("Failed to kill daemon: %s", strerror(errno));
			return 0;
		}

		/* Single instance */
		if ((pid = daemon_pid_file_is_running()) >= 0) {
			error_msg("Daemon already running on PID file %u", pid);
			return 1;
		}

		if (daemon_retval_init() < 0) {
			error_msg("Failed to create pipe.");
			return 1;
		}

		/* Do the fork */
		if ((pid = daemon_fork()) < 0) {

			daemon_retval_done();
			error_msg("Error in daemon fork %s", strerror(errno));
			return 1;

		} else if (pid) { /* The parent */
			int ret;

			/* Wait for 20 seconds for the return value passed from the daemon process */
			if ((ret = daemon_retval_wait(20)) < 0) {
				error_msg("Could not recieve return value from daemon process: %s", strerror(errno));
				return -1;
			}
			if (!ret)
				debug_msg("Daemon returned %i as return value.", ret);

			return ret;
		}

		/* Close FDs */
		if (daemon_close_all(-1) < 0) {
			error_msg("Failed to close all file descriptors: %s", strerror(errno));

			/* Send the error condition to the parent process */
			daemon_retval_send(1);
			goto close_n_exit;
		}

		/* Create the PID file */
		if (daemon_pid_file_create() < 0) {
			error_msg("Could not create PID file (%s).", strerror(errno));
			daemon_retval_send(2);
			goto close_n_exit;
		}

		/* Initialize signal handling */
		if (daemon_signal_init(SIGINT, SIGTERM, SIGQUIT, SIGHUP, 0) < 0) {
			error_msg("Could not register signal handlers (%s).", strerror(errno));
			daemon_retval_send(3);
			goto close_n_exit;
		}

		/* Send OK to parent process */
		daemon_retval_send(0);
	}
	daemon_log_use = DAEMON_LOG_SYSLOG;

	/* Start MPM server */
	mpm_start_server();

close_n_exit:
	error_msg("Exiting %s daemon", argv[0]);
	if (daemonize) {
		daemon_retval_send(255);
		daemon_signal_done();
		daemon_pid_file_remove();
	}
	mpm_cleanup();

	return 0;
}

int json_check_version (cJSON *json)
{
	cJSON * pv_obj = NULL;

	pv_obj = cJSON_GetObjectItem(json,"parser-version");

	if (!pv_obj) {
		error_msg("No parser-version object in cfg file\n");
		return (-1);
	}

	if (strcmp(pv_obj->valuestring, JSON_PARSER_VERSION)) {
		error_msg("Incorrrect parser-version (%s) object in cfg file\n",
				pv_obj->valuestring);
		return (-1);
	}

	return (0);
}
#if 0
int json_get_inputif (cJSON *json)
{
	cJSON * ip_obj = NULL;
	int found = 0;

	ip_obj = cJSON_GetObjectItem(json,"inputif");
	if (!ip_obj) {
		error_msg("No inputif object in cfg file\n");
		return (-1);
	}

	if (!strcmp(ip_obj->valuestring, "stdin")) {
		mpm_cfg.inputif = std_input;
		found = 1;
	}

	if (!found) {
		error_msg("Un-supported input interface specified (%s)",
				ip_obj->valuestring);
		return (1);
	}

	return (0);
}
#endif

int json_set_outputif (cJSON *json)
{
	cJSON * op_obj = NULL;
	int found = 0;

	mpm_cfg.outputif = log_daemon;
	op_obj = cJSON_GetObjectItem(json,"outputif");
	if (!op_obj) {
		info_msg("No outputif object in cfg file\n");
		return (-1);
	}

	if (!strcmp(op_obj->valuestring, "stdout")) {
		daemon_log_use = DAEMON_LOG_STDOUT;
	}else if (!strcmp(op_obj->valuestring, "stderr")) {
		daemon_log_use = DAEMON_LOG_STDERR;
	} else if (!strcmp(op_obj->valuestring, "syslog")) {
		daemon_log_use = DAEMON_LOG_SYSLOG;
	} else if (!strcmp(op_obj->valuestring, "default")) {
		daemon_log_use = DAEMON_LOG_AUTO|DAEMON_LOG_STDERR;
	} else {
		mpm_cfg.logfile_p = fopen (op_obj->valuestring, "w+");
		if (!mpm_cfg.logfile_p) {
			error_msg("Error in opening log file %s (%s)",
					op_obj->valuestring, strerror(errno));
			return (-1);
		}
		mpm_cfg.outputif = log_file;
	}

	return (0);
}

int json_get_cmdfile (cJSON *json)
{
	cJSON * op_obj = NULL;
	int len;

	op_obj = cJSON_GetObjectItem(json,"cmdfile");
	if (!op_obj) {
		return (0);
	}

	mpm_cfg.cmdfile = malloc(strlen(op_obj->valuestring + 1));
	if (!mpm_cfg.cmdfile) {
		error_msg("Error in allocating memory for cmdfile name (%s)",
				strerror(errno));
		return (-1);
	}

	strcpy(mpm_cfg.cmdfile, op_obj->valuestring);

	return (0);
}
#if 0
int json_get_scratchinfo(cJSON *json)
{
	cJSON * op_obj = NULL;
	int len;

	op_obj = cJSON_GetObjectItem(json,"scratchaddr");
	if (!op_obj) {
		return (-1);
	}

	if (op_obj->type == cJSON_Number) {
		mpm_cfg.scratch_block_cfg.alias = op_obj->valueint;
	} else if (op_obj->type == cJSON_String) {
		mpm_cfg.scratch_block_cfg.alias = strtoull(op_obj->valuestring, NULL, 0);
	} else {
		error_msg("Invalid type (%d) for scratch memory address",
				op_obj->type);
		return (-1);
	}

	if (mpm_cfg.scratch_block_cfg.alias & 0x3ff) {
		error_msg("The scratch block address 0x%x is not 10bit aligned",
				mpm_cfg..scratch_block_cfg.alias);
		return (-1);
	}

	op_obj = cJSON_GetObjectItem(json,"scratchlength");
	if (!op_obj) {
		return (-1);
	}

	if (op_obj->type == cJSON_Number) {
		mpm_cfg.scratch_block_cfg.length = op_obj->valueint;
	} else if (op_obj->type == cJSON_String) {
		mpm_cfg.scratch_block_cfg.length = strtoull(op_obj->valuestring, NULL, 0);
	} else {
		error_msg("Invalid type (%d) for scratch memory address length",
				op_obj->type);
		return (-1);
	}

	if (mpm_cfg.scratch_block_cfg.length < MAX_INSTR_COUNT * sizeof(uint32_t)) {
		error_msg("The minimum scratch block length should be 0x%x, but received 0x%x",
			  MAX_INSTR_COUNT * sizeof(uint32_t),
			  mpm_cfg.scratch_block_cfg.alias);
		return (-1);
	}

	return (0);

}
#endif
#define json_obj_getnum(obj, item, iteme) \
	do { \
		if (obj->type == cJSON_Number) { \
			item.iteme = obj->valueint; \
		} else if (obj->type == cJSON_String) { \
			item.iteme = strtoull(obj->valuestring, NULL, 0); \
		} else { \
			error_msg("Invalid type (%d)", \
					obj->type); \
			return (-1); \
		} \
	} while(0)

int json_get_segments (cJSON *json)
{
	cJSON * segs;
	cJSON * segp;
	cJSON * sege;
	int len, i;

	segs = cJSON_GetObjectItem(json,"segments");
	if (!segs) {
		error_msg("No segments object array in cfg file\n");
		return (-1);
	}

	if (segs->type != cJSON_Array) {
		error_msg("Segments must be array type\n");
		return (-1);
	}

	len = cJSON_GetArraySize(segs);
	if (len > MPM_MAX_SEGMENTS) {
		error_msg("The number of segments (%d) must be less than %d\n",
				len, MPM_MAX_SEGMENTS);
		return (-1);
	}

	for (i = 0; i < len; i++) {
		segp = cJSON_GetArrayItem(segs, i);
		if (!segp) {
			error_msg("Could not read segment for index %d\n", i);
			return (-1);
		}
		sege = cJSON_GetObjectItem(segp, "name");
		if (!sege) {
			error_msg("Segment without a name, skipping\n");
			continue;
		}

		if (strlen(sege->valuestring) > MPM_MAX_NAME_LENGTH) {
			error_msg("Segment name is too big (%d), must be less than %d\n",
					(int) strlen(sege->valuestring), MPM_MAX_NAME_LENGTH);
			return (-1);
		}

		strncpy(mpm_cfg.segment[mpm_cfg.num_segments].name, sege->valuestring, MPM_MAX_NAME_LENGTH);

		sege = cJSON_GetObjectItem(segp, "globaladdr");
		if (!sege) {
			error_msg("No global address for segment %s\n",
					mpm_cfg.segment[mpm_cfg.num_segments].name);
			return (-1);
		}

		json_obj_getnum(sege, mpm_cfg.segment[mpm_cfg.num_segments], global_addr);

		sege = cJSON_GetObjectItem(segp, "length");
		if (!sege) {
			error_msg("No length for segment %s\n",
					mpm_cfg.segment[mpm_cfg.num_segments].name);
			return (-1);
		}

		json_obj_getnum(sege, mpm_cfg.segment[mpm_cfg.num_segments], length);

		sege = cJSON_GetObjectItem(segp, "localaddr");
		if (!sege) {
			mpm_cfg.segment[mpm_cfg.num_segments].local_addr
				= mpm_cfg.segment[mpm_cfg.num_segments].global_addr;
		} else {
			json_obj_getnum(sege, mpm_cfg.segment[mpm_cfg.num_segments], local_addr);
		}

		mpm_cfg.num_segments++;
	}

	return (0);
}

int json_get_slaves (cJSON *json)
{
	char *namep;
	int len, i, j;
	int lenm, m;
	mpm_slave_cfg_t *slv_cfg_p;
	cJSON *slvs, *slvp, *slve, *slvm;
	mpm_transport_e tr;

	slvs = cJSON_GetObjectItem(json,"slaves");
	if (!slvs) {
		error_msg("No slaves object array in cfg file\n");
		return (-1);
	}

	if (slvs->type != cJSON_Array) {
		error_msg("Slaves must be array type\n");
		return (-1);
	}

	len = cJSON_GetArraySize(slvs);

	debug_msg("Number of slaves %d ", len);

	for (i = 0; i < len; i++) {

		slvp = cJSON_GetArrayItem(slvs, i);
		if (!slvp) {
			error_msg("Could not read slave info for index %d", i);
			return (-1);
		}

		slv_cfg_p = &mpm_cfg.slave[mpm_cfg.num_slaves];

		if (mpm_cfg.num_slaves > MPM_MAX_SLAVES) {
			error_msg("The number of slave cores (%d) must be less than %d\n",
				  mpm_cfg.num_slaves, MPM_MAX_SLAVES);
			return (-1);
		}

#if 0
		slve = cJSON_GetObjectItem(slvp, "transport");
		if (slve) {
			if (!strcmp(slve->valuestring, "sharedmemory")) {
				tr = shared_memory;
			} else {
				error_msg("Skipping transport %s for mpm",
					  slve->valuestring);
				continue;
			}
		} else {
			error_msg("No transport type, assume shared memory");
			tr = shared_memory;
		}
#endif
		
		slve = cJSON_GetObjectItem(slvp, "name");
		if (!slve) {
			error_msg("Slave processor without a name, skipping");
			continue;
		}

		if (strlen(slve->valuestring) > MPM_MAX_NAME_LENGTH) {
			error_msg("Slave core name is too big (%d), must be less than %d\n",
					(int) strlen(slve->valuestring), MPM_MAX_NAME_LENGTH);
			return (-1);
		}

		strncpy(slv_cfg_p->name, slve->valuestring, MPM_MAX_NAME_LENGTH);
		debug_msg("adding core %s to the list", slv_cfg_p->name);

		//slv_cfg_p->transport = tr;

		slve = cJSON_GetObjectItem(slvp, "coreid");
		if (slve) {
			json_obj_getnum(slve, mpm_cfg.slave[mpm_cfg.num_slaves], core_id);
		}

		slve = cJSON_GetObjectItem(slvp, "clusterid");
		if (slve) {
			json_obj_getnum(slve, mpm_cfg.slave[mpm_cfg.num_slaves], cluster_id);
		}

		slve = cJSON_GetObjectItem(slvp, "crashcallback");
		if (slve) {
			if (strlen(slve->valuestring) > MAX_FILE_NAME_LEN) {
				error_msg("Callback script name is too big (%d), must be less than %d\n",
						(int) strlen(slve->valuestring), MAX_FILE_NAME_LEN);
				return (-1);
			}
			strncpy(slv_cfg_p->crash_callback, slve->valuestring, MAX_FILE_NAME_LEN);
		}

		slvm = cJSON_GetObjectItem(slvp, "memorymap");

		if (!slvm) {
			error_msg("No slave memory map in cfg file\n");
			return (-1);
		}

		if (slvm->type != cJSON_Array) {
			error_msg("Slave memory map must be array type\n");
			return (-1);
		}

		lenm = cJSON_GetArraySize(slvm);
		if (lenm > MPM_MAX_MMAP_SECTIONS) {
			error_msg("The number of memory map sections (%d) must be less than %d\n",
					lenm, MPM_MAX_MMAP_SECTIONS);
			return (-1);
		}

		for (m = 0; m < lenm; m++) {
			slvp = cJSON_GetArrayItem(slvm, m);
			if (!slvp) {
				debug_msg("Could not read slave info for index %d\n", m);
				return (-1);
			}

			namep = slvp->valuestring;

			for (j = 0; j < mpm_cfg.num_segments; j++) {
				if (!strcmp(namep, mpm_cfg.segment[j].name)) {
					slv_cfg_p->mmap[slv_cfg_p->num_mmap++] = &mpm_cfg.segment[j];
					break;
				}
			}

			if (j == mpm_cfg.num_segments) {
				error_msg("Invalid segment %s for slave core %s\n",
						namep, slv_cfg_p->name);
				return (-1);
			}

		}
		mpm_cfg.num_slaves++;
	}
	debug_msg("Number of slaves Configured %d ", mpm_cfg.num_slaves);

	debug_msg("Returning");
	return (0);
}


int json_get_sockperm (cJSON *json)
{
	cJSON * op_obj = NULL;

	mpm_cfg.socket_perm = DEFAULT_SOCKET_PERM;
	op_obj = cJSON_GetObjectItem(json, "socketperm");
	if (op_obj) {
		json_obj_getnum(op_obj, mpm_cfg, socket_perm);
	}

	info_msg("setting socket permission to 0%o ", mpm_cfg.socket_perm);

	return (0);
}

#define LOG_APPEND(x) \
do { \
	int len; \
	len = MAX_LOG_LEN - strlen(mpm_log_buf); \
if (len > 0) \
	strncat(mpm_log_buf, x, len); \
else \
	return; \
} while(0)

/* MPM logging utility (need to move it to another file) */
void mpm_log(const int loglevel, const char* functionName, const char* fileName, const int lineNo, const char* format, ...)
{
	int len;
	char lineno_a[32];
	va_list args;

	sprintf(lineno_a, "%d", lineNo);

	mpm_log_buf[0] = 0;

	LOG_APPEND(fileName);
	LOG_APPEND(":");
	LOG_APPEND(lineno_a);
	LOG_APPEND(":");
	LOG_APPEND(functionName);
	LOG_APPEND(": ");

	len = MAX_LOG_LEN - strlen(mpm_log_buf);
	if (len <= 0)
		return;

	va_start(args, format);
	vsnprintf(&mpm_log_buf[strlen(mpm_log_buf)], len, format, args);
	va_end(args);

	if (mpm_cfg.outputif == log_daemon) {
		daemon_log(loglevel, "%s\n", mpm_log_buf);
	} else if (mpm_cfg.outputif == log_file && mpm_cfg.logfile_p) {
		fprintf(mpm_cfg.logfile_p, "%s\n", mpm_log_buf);
		fflush(mpm_cfg.logfile_p);
	}

}

