/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <strings.h>
#include <fcntl.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include "mpmloc.h"
#include "c60_elf32.h"

#define roundup4(x) (((x) + 3) & ~0x03)

#define NOTES_SECTION_NAME ".note"

//#define DEBUG

static int file_write(char *map, uint32_t size, void *data)
{
	FILE *fpr = (FILE *) data;
	if (fwrite (map, size, 1, fpr) < 0) {
		error_msg("fwrite failed for size %d", size);
		return -1;
	}

	return 0;
}

static int data_read(char *map, uint32_t size, void *data)
{
	memcpy((char *) data, map, size);

	return 0;
}

int mpm_save_coredump (mpm_slave_t *sp, char *file_name)
{
	int i, j;
	FILE *ifp = NULL;
	FILE *cfp = NULL;
	int length;
	int ret_val = -1;
	int add_notes = 0;
	int section_offset;
	int string_table_offset;
	int num_no_load_sections=0;
	char *note_data = NULL;
	struct Elf32_Ehdr *ehdr_image = 0;
	struct Elf32_Ehdr *ehdr_core = 0;
	struct Elf32_Phdr *phdr_image = 0;
	struct Elf32_Phdr *phdr_core = 0;
	struct Elf32_Nhdr *nhdr_core = 0;
	struct Elf32_Shdr *shdr_image = 0;
	struct Elf32_Shdr *shdr = 0;

	info_msg("Save coredump file to %s", file_name);

	if ((sp->state != running_state) && (sp->state != crashed_state)) {
		error_msg("invalid state %d for coredump", sp->state);
		mpm_errno = MPM_ERROR_INVALID_COMMAND;
		return -1;
	}

#if 0
	if (sp->state == crashed_state) {
		add_notes = 1;
	} else {
		add_notes = 0;
	}
#endif
	ifp = fopen(sp->image_name, "rb");
	if (!ifp) {
		error_msg("unable to open loaded image %s for reading (err: %s)",
				sp->image_name, strerror(errno));
		mpm_errno = MPM_ERROR_FILE_OPEN;
		goto close_n_exit;
	}

	cfp = fopen(file_name, "wb");
	if (!cfp) {
		error_msg("unable to open core image %s (err: %s)",
				file_name, strerror(errno));
		mpm_errno = MPM_ERROR_FILE_OPEN;
		goto close_n_exit;
	}

	ehdr_image = calloc(1, sizeof(struct Elf32_Ehdr));
	if (!ehdr_image) {
		error_msg("unable allocate memory for image header (err: %s)",
				strerror(errno));
		goto close_n_exit;
	}
	
	ehdr_core = calloc(1, sizeof(struct Elf32_Ehdr));
	if (!ehdr_core) {
		error_msg("unable allocate memory for core header (err: %s)",
				strerror(errno));
		goto close_n_exit;
	}

	phdr_image = malloc(sizeof(struct Elf32_Phdr));
	if (!phdr_image) {
		error_msg("unable allocate memory for image ptable header (err: %s)",
				strerror(errno));
		goto close_n_exit;
	}

	length = fread(ehdr_image, 1, sizeof(struct Elf32_Ehdr), ifp);
	if (length != sizeof(struct Elf32_Ehdr)) {
		error_msg("unable to read image header (err: %s)",
				strerror(errno));
		goto close_n_exit;
	}

	memcpy(ehdr_core, ehdr_image, sizeof(struct Elf32_Ehdr));
	ehdr_core->e_type = ET_CORE;
	ehdr_core->e_entry = 0;
	ehdr_core->e_shnum = 0;
	ehdr_core->e_shoff = 0;
	ehdr_core->e_shentsize = 0;
	ehdr_core->e_shstrndx = SHN_UNDEF;
	ehdr_core->e_ehsize = sizeof(struct Elf32_Ehdr);
	ehdr_core->e_phentsize = sizeof(struct Elf32_Phdr);
	ehdr_core->e_phoff = ehdr_core->e_ehsize;

	/* check for a valid notes section */
	if (sp->elf_note.present) {
		note_data = calloc(1, sp->elf_note.size);
		if (!note_data) {
			error_msg("unable allocate memory for note section (err: %s)",
					strerror(errno));
			goto close_n_exit;
		}

		/* Map and copy segments from memory */
		if (map_and_copy_segment(sp, sp->elf_note.address, sp->elf_note.size, data_read, (void *) note_data)) {
			error_msg("unable to read note section from image");
			goto close_n_exit;
		}

		nhdr_core = (struct Elf32_Nhdr *) note_data;

		if ((nhdr_core->n_namesz > 0) && (nhdr_core->n_descsz > 0) && 
			((nhdr_core->n_namesz + nhdr_core->n_descsz + sizeof(struct Elf32_Nhdr)) <= sp->elf_note.size)) {
			add_notes = 1;
		} else {
			add_notes = 0;
		}
	}

	if (add_notes) {
		ehdr_core->e_phnum += 1;
	}
	info_msg(" Number of sections %d", ehdr_image->e_shnum);
	/* Get all the NOLOAD sections */
	/* Allocate section header local copy */
	shdr_image = calloc(ehdr_image->e_shnum, sizeof(struct Elf32_Shdr));
	if (!shdr_image) {
		error_msg("unable allocate memory for image section header (err: %s)",
				strerror(errno));
		goto close_n_exit;
	}
	/* Set starting location for section header */
	if (fseek(ifp, ehdr_image->e_shoff, SEEK_SET)) {
			error_msg("fseek to %d of input image failed (err: %s)",
				ehdr_image->e_shoff, strerror(errno));
		goto close_n_exit;
	}
	/* Read and check each section header to fine no load sections*/
	shdr = shdr_image;
	for ( j=0; j< ehdr_image->e_shnum; j++, shdr++) {

		length = fread(shdr, 1, sizeof(struct Elf32_Shdr), ifp);
		if (length != sizeof(struct Elf32_Shdr)) {
			error_msg("unable to read image Section header (err: %s)",
					strerror(errno));
			goto close_n_exit;
		}
		if((shdr->sh_type == SHT_NOBITS )  && (shdr->sh_size != 0)){
			/* Skip the note section */
			if (shdr->sh_addr == sp->elf_note.address)
				continue;
			ehdr_core->e_phnum += 1;
			num_no_load_sections++;
		}
	}
	info_msg(" Number of noload sections %d", num_no_load_sections);
	phdr_core = calloc(ehdr_core->e_phnum, sizeof(struct Elf32_Phdr));
	if (!phdr_core) {
		error_msg("unable allocate memory for core ptable header (err: %s)",
				strerror(errno));
		goto close_n_exit;
	}

	length = fwrite(ehdr_core, 1, sizeof(struct Elf32_Ehdr), cfp);
	if (length != sizeof(struct Elf32_Ehdr)) {
		error_msg("unable to write core image header (err: %s)",
				strerror(errno));
		goto close_n_exit;
	}

	section_offset = ehdr_core->e_phoff + ehdr_core->e_phnum * sizeof(struct Elf32_Phdr);

	if (fseek(ifp, ehdr_image->e_phoff, SEEK_SET)) {
		error_msg("fseek to %d of input image failed (err: %s)",
				ehdr_image->e_phoff, strerror(errno));
		goto close_n_exit;
	}

	for (i = 0; i < ehdr_image->e_phnum; i++) {
		length = fread(&phdr_core[i], 1, sizeof(struct Elf32_Phdr), ifp);
		if (length != sizeof(struct Elf32_Phdr)) {
			error_msg("unable to write phdr notes (err: %s)",
					strerror(errno));
			goto close_n_exit;
		}

		phdr_core[i].p_type = PT_LOAD;
		phdr_core[i].p_filesz = phdr_core[i].p_memsz;
		phdr_core[i].p_offset = section_offset;

		section_offset += phdr_core[i].p_filesz;
	}

	if (add_notes) {
		/* Add notes header */
		memset(&phdr_core[i], 0, sizeof(struct Elf32_Phdr));
		phdr_core[i].p_type = PT_NOTE;
		phdr_core[i].p_offset = section_offset;
		phdr_core[i].p_memsz = sizeof(struct Elf32_Nhdr)
					+ roundup4(nhdr_core->n_namesz)
					+ roundup4(nhdr_core->n_descsz);
		phdr_core[i].p_filesz = phdr_core[i].p_memsz;
		phdr_core[i].p_paddr  = sp->elf_note.address;

		section_offset += phdr_core[i].p_filesz;
		i++;
	}

	/* Populate with no load sections */
	shdr = shdr_image;
	for (j = 0; j < ehdr_image->e_shnum; j++, shdr++) {
#ifdef DEBUG
		info_msg(" Section %d: type %d, Address %x , Size %x ", j, shdr->sh_type, shdr->sh_addr,  shdr->sh_size);
#endif
		if((shdr->sh_type == SHT_NOBITS )  && (shdr->sh_size != 0)){
			/* Skip the note section */
			if (shdr->sh_addr == sp->elf_note.address)
				continue;
			/* Add notes header */
			memset(&phdr_core[i], 0, sizeof(struct Elf32_Phdr));
			phdr_core[i].p_type = PT_LOAD;
			phdr_core[i].p_offset = section_offset;
			phdr_core[i].p_memsz = shdr->sh_size;
			phdr_core[i].p_filesz = phdr_core[i].p_memsz;
			phdr_core[i].p_paddr  = shdr->sh_addr;

			section_offset += phdr_core[i].p_filesz;
			i++;
		}
	}

	length = fwrite(phdr_core, 1, sizeof(struct Elf32_Phdr) * ehdr_core->e_phnum, cfp);
	if (length != sizeof(struct Elf32_Phdr) * ehdr_core->e_phnum) {
		error_msg("unable to write phdr (err: %s)",
				strerror(errno));
		goto close_n_exit;
	}

	for (i = 0; i < ehdr_core->e_phnum; i++) {
		/* Map and copy segments from memory */
		if (map_and_copy_segment(sp, phdr_core[i].p_paddr, phdr_core[i].p_filesz, file_write, (void *) cfp)) {
			error_msg("unable to write %d segment in coredump file", i);
			goto close_n_exit;
		}

		if (phdr_core[i].p_type == PT_NOTE) {
			phdr_core[i].p_paddr = 0;
			phdr_core[i].p_vaddr = 0;
		}
	}
	ret_val = 0;

close_n_exit:

	check_n_fclose(ifp);
	check_n_fclose(cfp);
	check_n_free(ehdr_image);
	check_n_free(ehdr_core);
	check_n_free(phdr_image);
	check_n_free(phdr_core);
	check_n_free(note_data);
	check_n_free(shdr_image);

	return 0;
}
