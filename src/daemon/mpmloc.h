/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __MPMLOC_H__
#define __MPMLOC_H__

#include <syslog.h>
#include "mpmlog.h"
#include "sockmsg.h"
#include "mpmdb.h"
#include "mpm_transport.h"

#define MAX_DEVICE_NAME_LEN	(32)
#define MAX_INSTR_COUNT		(8)

#define MPM_DAEMON_PID_FILE_NAME "/var/run/mpm/pid"

typedef struct mpm_slave_tag mpm_slave_t;

/* Events to slave processors */
typedef enum mpm_event_tag {
	entry_ev,

	load_ev,
	run_ev,
	crash_ev,
	reset_ev,
	transport_ev,
	load_withpreload_ev,
	run_withpreload_ev,

	last_ev
} mpm_event_e;

/* Peripherals supported */
typedef enum mpm_peripheral_tag {
	hyperlink0,
	hyperlink1
} mpm_peripheral_e;

/* mpm slave state machine structures */
typedef void (*mpm_slave_state_func)(mpm_slave_t *sp, mpm_event_e event, void * data);

typedef struct mpm_state_entry_tag
{
	mpm_slave_state_func	func;
	int			timeout;
	mpm_state_e		state;
	const char*		name;
} ccup_state_entry;

typedef struct mpm_event_entry_tag
{
	mpm_event_e		event;
	const char*		name;
} mpm_event_entry_t;

#define MPM_UIO_DEVICE_STATUS_CLOSE  0
#define MPM_UIO_DEVICE_STATUS_OPEN   1

typedef struct mpm_uio_fd_info_tag{
   int fd;
   int misc_fd;
   int fd_status; 
} mpm_uio_fd_info_t;

typedef struct mpm_elf_note_info_tag{
   int present;
   uint32_t address;
   uint32_t size;
} mpm_elf_note_info_t;

typedef  struct mpm_slave_tag {
	mpm_state_e state; /* State of the slave core*/
	mpm_slave_cfg_t *cfgp;
	char image_name[MAX_FILE_NAME_LEN]; /* Image filename when loaded*/
	char uio_class[MPM_MAX_NAME_LENGTH];
	mpm_uio_fd_info_t uio_fd_info;
	mpm_elf_note_info_t elf_note;
	uint32_t entry_point;
	int image_handle;
	off_t image_size;
	void *dl_handle;
	char *filep;
	char *readp;
	void *rsc_table;
	uint32_t rsc_table_runaddr;
	uint32_t gdb_server_addr;
	int wrong_endian;
	mpm_transport_h tr_h;
} mpm_slave_t;

extern mpm_slave_t *mpm_slave;

int mpm_sm_start(void);

int mpm_ssm_event (mpm_slave_t *sp, mpm_event_e event, void *data);

int mpm_ssm_init(void);

int mpm_ssm_close(void);

int mpm_dlif_init(mpm_slave_t *sp);

int mpm_dlif_load (mpm_slave_t *sp);

int mpm_dlif_run (mpm_slave_t *sp);

int mpm_dlif_run_withpreload(mpm_slave_t *sp);

int mpm_dlif_close(mpm_slave_t *sp);

int mpm_dlif_stop(mpm_slave_t *sp);

int mpm_dlif_reset(mpm_slave_t *sp);

int mpm_save_coredump (mpm_slave_t *sp, char *file_name);

int mpm_get_global_addr (mpm_slave_t *sp, uint32_t laddr, uint32_t size, uint32_t *gaddr);

void *mpm_monitor (void *arg);

typedef int (*map_seg_func)(char *, uint32_t, void *);

int map_and_copy_segment (mpm_slave_t *sp, uint32_t addr, uint32_t size,
						map_seg_func fpr, void *data);

int mpm_service_transport(mpm_slave_t *p, char *op);

int mpm_check_chip_security();

int mpm_server(void);

extern uint32_t mpm_errno;

#define MPM_DOWNLOAD_TIMEOUT_MS 1000

#define MPM_ERROR_SSM_UNEXPECTED_EVENT	-100
#define MPM_ERROR_SSM_INVALID_EVENT	-101
#define MPM_ERROR_INVALID_NAME_LENGTH	-102
#define MPM_ERROR_FILE_OPEN		-103
#define MPM_ERROR_IMAGE_LOAD		-104
#define MPM_ERROR_UIO			-105
#define MPM_ERROR_IMAGE_INVALID_ENTRY_ADDRESS -106
#define MPM_ERROR_RESOURCE_TABLE_SETTING -107
#define MPM_ERROR_NO_ENTRY_POINT	-108
#define MPM_ERROR_INVALID_COMMAND	-109
#define MPM_ERROR_WRITE_ENTRY_POINT	-110
#define MPM_ERROR_READ_ENTRY_POINT	-111
#define MPM_ERROR_SECURITY_STATUS_READ  -112
#define MPM_ERROR_DOWNLOAD_TIMEOUT	-113
#define MPM_ERROR_TRANSPORT_OPERATION_FAIL -114
#define MPM_ERROR_SCRATCH_MEM_ERROR 	-115

#define check_n_free(x) if(x) free(x)
#define check_n_fclose(x) if(x) fclose(x)
#define check_n_close(x) if(x<0) close(x)
#endif
