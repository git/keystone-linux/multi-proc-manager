/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __UIOUTILS_H__
#define __UIOUTILS_H__

#include <stdint.h>

typedef struct uioutil_map_info_tag {
	int		index;
	int		offset;
	int		base;
	int		size;
} uioutil_map_info_t;

typedef enum uioutil_oper_tag {
	assign_op,
	and_op,
	or_op
} uioutil_oper_t;

int uioutil_get_int (char *filename, int *val);

int uioutil_get_string (char *filename, char *str, int str_len);

int uioutil_get_device (char *uio_name, char *class_name, int class_name_length);

int uioutil_mapinfo_from_addr (uioutil_map_info_t *p_info, char *class_name, int address, int size);

int uioutil_mapinfo_from_name (uioutil_map_info_t *p_info, char *dev_class, char *name);

int uioutil_write_reg (char *class_name, char *uio_name, int fd, int val, uioutil_oper_t op);

int uioutil_read_reg (char *class_name, char *uio_name, int fd, uint32_t *val);

#endif
