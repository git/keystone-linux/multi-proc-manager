/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <libdaemon/daemon.h>
#include "mpmloc.h"
#include "mpmdb.h"
#include "sockmsg.h"

int mpm_server(void)
{
	int retval;
	uint32_t *data;
	int length = 0;
	uint32_t status;
	uint32_t *data32;
	int signal_fd = 0;
	mpm_slave_t *slavep;
	sock_name_t serv_sock_name;
	sock_name_t client_sock_addr;
	client_to_server_msg_t *c2s = 0;
	server_to_client_msg_t *s2c = 0;
	struct sockaddr_un client_addr;
	char mpm_socket_name[] = MPM_DAEMON_SOCKET_NAME;

	serv_sock_name.type = sock_name_e;
	serv_sock_name.s.name = mpm_socket_name;
	mpm_cfg.serv_sock_h = sock_open (&serv_sock_name);
	if (!mpm_cfg.serv_sock_h) {
		error_msg("Error in opening socket %s", mpm_socket_name);
		return -1;
	}

	retval = chmod (mpm_socket_name, mpm_cfg.socket_perm);
	if (retval < 0) {
		error_msg("Error in changing permission for socket %s (err: %s)",
			mpm_socket_name, strerror(errno));
	}

	signal_fd = daemon_signal_fd();

	while(1) {
		info_msg("waiting for slave message");

		retval = sock_wait(mpm_cfg.serv_sock_h, &length, NULL, signal_fd);
		if (retval < 0) {
			if (errno == EINTR) {
				/* If server stopped end server loop */
				break;
			}
			error_msg("Error in reading from socket");
			goto loop_continue;
		}

		if (length < sizeof(client_to_server_msg_t)) {
			error_msg("invalid message length %s", length);
			goto loop_continue;
		}
		c2s = calloc(1, length);
		if (!c2s) {
			error_msg("can't malloc for recv message (err: %s)",
						strerror(errno));
			goto loop_continue;
		}

		client_sock_addr.type = sock_addr_e;
		client_sock_addr.s.addr = &client_addr;
		retval = sock_recv(mpm_cfg.serv_sock_h, (char *)c2s, length, &client_sock_addr);
		if (retval != length)
		{
			error_msg("recv data failed from socket, received = %d, expected = %d",
				retval, length);
			goto loop_continue;
		}

		info_msg("received message of size %d bytes for cmd %d", length, c2s->cmd);

		msg_alloc(s2c, sizeof(uint32_t) * 2);
		if (!s2c) {
			error_msg("can't malloc for send message (err: %s)",
						strerror(errno));
			goto loop_continue;
		}

		s2c->message_id = c2s->message_id;
		s2c->status = mpm_status_ok;
		data32 = (uint32_t *) (msg_data(s2c));

		if (!data32) goto loop_continue;

		switch (c2s->cmd) {
			case mpm_cmd_ping:
				info_msg("received ping command");
				data32[0] = (uint32_t) mpm_status_ok;
				break;

			case mpm_cmd_load:

				info_msg("received load command for %s filename %s",
					c2s->slave_name, msg_data(c2s));
				data32[0] = (uint32_t) mpm_status_ok;

				if (!msg_data(c2s)) {
					error_msg("no filename in load command");
					data32[0] = (uint32_t) mpm_status_nofile;
				}

				find_slave_p(c2s->slave_name, slavep);
				if (!slavep) {
					error_msg("invalid slave name %s", c2s->slave_name);
					data32[0] = (uint32_t) mpm_status_invalid_slave_name;
				} else {
					mpm_errno = 0;
					if (mpm_ssm_event (slavep, load_ev, msg_data(c2s))) {
						error_msg("ssm returned error");
						data32[0] = (uint32_t) mpm_status_error_ssm;
					}
					if (mpm_errno != 0) {
						data32[0] = (uint32_t) mpm_status_nok;
						data32[1] = mpm_errno;
					}
				}
				break;

			case mpm_cmd_load_withpreload:

				info_msg("received load with preload command for %s filename %s",
					c2s->slave_name, msg_data(c2s));
				data32[0] = (uint32_t) mpm_status_ok;

				if (!msg_data(c2s)) {
					error_msg("no filename in load with preload command");
					data32[0] = (uint32_t) mpm_status_nofile;
				}

				find_slave_p(c2s->slave_name, slavep);
				if (!slavep) {
					error_msg("invalid slave name %s", c2s->slave_name);
					data32[0] = (uint32_t) mpm_status_invalid_slave_name;
				} else {
					mpm_errno = 0;
					if (mpm_ssm_event (slavep, load_withpreload_ev, msg_data(c2s))) {
						error_msg("ssm returned error");
						data32[0] = (uint32_t) mpm_status_error_ssm;
					}
					if (mpm_errno != 0) {
						data32[0] = (uint32_t) mpm_status_nok;
						data32[1] = mpm_errno;
					}
				}
				break;

			case mpm_cmd_run:
				info_msg("received run command");
				data32[0] = (uint32_t) mpm_status_ok;

				find_slave_p(c2s->slave_name, slavep);
				if (!slavep) {
					error_msg("invalid slave name %s", c2s->slave_name);
					data32[0] = (uint32_t) mpm_status_invalid_slave_name;
				} else {
					mpm_errno = 0;
					if (mpm_ssm_event (slavep, run_ev, NULL)) {
						error_msg("ssm returned error");
						data32[0] = (uint32_t) mpm_status_error_ssm;
					}
					if (mpm_errno != 0) {
						data32[0] = (uint32_t) mpm_status_nok;
						data32[1] = mpm_errno;
					}
				}

				break;

			case mpm_cmd_run_withpreload:
				info_msg("received run with preload command");
				data32[0] = (uint32_t) mpm_status_ok;

				find_slave_p(c2s->slave_name, slavep);
				if (!slavep) {
					error_msg("invalid slave name %s", c2s->slave_name);
					data32[0] = (uint32_t) mpm_status_invalid_slave_name;
				} else {
					mpm_errno = 0;
					if (mpm_ssm_event (slavep, run_withpreload_ev, NULL)) {
						error_msg("ssm returned error");
						data32[0] = (uint32_t) mpm_status_error_ssm;
					}
					if (mpm_errno != 0) {
						data32[0] = (uint32_t) mpm_status_nok;
						data32[1] = mpm_errno;
					}
				}

				break;


			case mpm_cmd_reset:
				info_msg("received reset command");
				data32[0] = (uint32_t) mpm_status_ok;

				find_slave_p(c2s->slave_name, slavep);
				if (!slavep) {
					data32[0] = (uint32_t) mpm_status_invalid_slave_name;
				} else {
					mpm_errno = 0;
					if (mpm_ssm_event (slavep, reset_ev, NULL)) {
						error_msg("ssm returned error");
						data32[0] = (uint32_t) mpm_status_error_ssm;
					}
					if (mpm_errno != 0) {
						data32[0] = (uint32_t) mpm_status_nok;
						data32[1] = mpm_errno;
					}
				}
				break;

			case mpm_cmd_state:
				info_msg("received status command");
				data32[0] = (uint32_t) mpm_status_ok;

				find_slave_p(c2s->slave_name, slavep);
				if (!slavep) {
					data32[0] = (uint32_t) mpm_status_invalid_slave_name;
				} else {
					data32[1] = slavep->state;
				}
				break;

			case mpm_cmd_crash:
				info_msg("received crash command");

				find_slave_p(c2s->slave_name, slavep);
				if (!slavep) {
					error_msg("invalid slave name");
				} else {
					mpm_ssm_event (slavep, crash_ev, NULL);
				}

				goto loop_continue;

			case mpm_cmd_coredump:

				info_msg("received coredump command for %s filename %s",
					c2s->slave_name, msg_data(c2s));
				data32[0] = (uint32_t) mpm_status_ok;

				if (!msg_data(c2s)) {
					error_msg("no filename in load command");
					data32[0] = (uint32_t) mpm_status_nofile;
				}

				find_slave_p(c2s->slave_name, slavep);
				if (!slavep) {
					error_msg("invalid slave name %s", c2s->slave_name);
					data32[0] = (uint32_t) mpm_status_invalid_slave_name;
				} else {
					mpm_errno = 0;
					if (mpm_save_coredump (slavep, msg_data(c2s))) {
						error_msg("ssm returned error");
						data32[0] = (uint32_t) mpm_status_error_ssm;
					}
					if (mpm_errno != 0) {
						data32[0] = (uint32_t) mpm_status_nok;
						data32[1] = mpm_errno;
					}
				}
				break;

			case mpm_cmd_transport:

				info_msg("received transport command for %s, operation %s",
					c2s->slave_name, msg_data(c2s));
				data32[0] = (uint32_t) mpm_status_ok;

				if (!msg_data(c2s)) {
					error_msg("no operation in transport command");
					data32[0] = (uint32_t) mpm_status_nofile;
				}

				find_slave_p(c2s->slave_name, slavep);
				if (!slavep) {
					error_msg("invalid slave name %s", c2s->slave_name);
					data32[0] = (uint32_t) mpm_status_invalid_slave_name;
				} else {
					mpm_errno = 0;
					if (mpm_ssm_event (slavep, transport_ev, msg_data(c2s))) {
						error_msg("ssm returned error");
						data32[0] = (uint32_t) mpm_status_error_ssm;
					}
					if (mpm_errno != 0) {
						data32[0] = (uint32_t) mpm_status_nok;
						data32[1] = mpm_errno;
					}
				}
				break;

			default:
				info_msg("received unknown command %s", c2s->cmd);
				s2c->status = mpm_status_nok;
				error_msg("message with invalid command %d received", c2s->cmd);
				break;
		}

		if (sock_send(mpm_cfg.serv_sock_h, (char *)s2c, msg_length(s2c), &client_sock_addr))
		{
			error_msg("send data failed");
			goto loop_continue;
		}
loop_continue:
		/* Cleanups */
		length = 0;
		if (c2s)
			free (c2s);
		if (s2c)
			free (s2c);
		memset(&client_sock_addr, 0, sizeof(sock_name_t));
		memset(&client_addr, 0, sizeof(struct sockaddr_un));
	}
	return 0;

}

/* This is a separate task */
void *mpm_monitor (void *arg)
{
	int i;
	int status = -1;
	uint32_t value;
	int retval;
	int irq_enabled = 0;
	fd_set fds;
	sock_h h = 0;
	sock_name_t srv_name;
	char mpm_daemon_socket_name[] = MPM_DAEMON_SOCKET_NAME;
	client_to_server_msg_t *c2s = NULL;

	info_msg("starting monitor thread");

	srv_name.type = sock_name_e;
	srv_name.s.name = mpm_daemon_socket_name;
	msg_alloc(c2s, 0);
	if (!c2s) {
		goto close_n_exit;
	}
	c2s->cmd = mpm_cmd_crash;

	value = 1; /* enable interrupt */
	while(1) {
		FD_ZERO(&fds);
		irq_enabled = 0;
		for(i = 0; i < mpm_cfg.num_slaves; i++) {
			if (mpm_slave[i].uio_fd_info.fd_status == MPM_UIO_DEVICE_STATUS_CLOSE) {
				continue;
			}
			FD_SET(mpm_slave[i].uio_fd_info.fd, &fds);
			retval = write(mpm_slave[i].uio_fd_info.fd, &value, sizeof(uint32_t));
			if (retval != sizeof(uint32_t)) {
				error_msg("unable to enable interrupt for %s", mpm_slave[i].cfgp->name);
				goto close_n_exit;
			}
			irq_enabled++;
		}
		if (!irq_enabled) {
			error_msg("all uio fds are in closed state");
			retval = 0;
			goto close_n_exit;
		}
		retval = select(FD_SETSIZE, &fds, NULL, NULL, NULL);
		if (retval == -1) {
			error_msg("select failed for monitor (error: %s)",
				strerror(errno));
			goto close_n_exit;
		}

		info_msg("received a crash event");
		for(i = 0; i < mpm_cfg.num_slaves; i++) {

			if (mpm_slave[i].uio_fd_info.fd_status == MPM_UIO_DEVICE_STATUS_CLOSE) {
				continue;
			}
			if (!FD_ISSET(mpm_slave[i].uio_fd_info.fd, &fds)) {
				continue;
			}
			read(mpm_slave[i].uio_fd_info.fd, &value, sizeof(uint32_t));
			info_msg("received a crash event for %s (value %d)",
				mpm_slave[i].cfgp->name, value);

			c2s->cmd = mpm_cmd_crash;
			memset(c2s->slave_name, 0, MPM_MAX_NAME_LENGTH);
			strncpy(c2s->slave_name, mpm_slave[i].cfgp->name, MPM_MAX_NAME_LENGTH - 1);

			if (sock_send(h, (char *)c2s, msg_length(c2s), &srv_name) < 0) {
				error_msg("sock_send returned error");
			}

		}
	}

close_n_exit:

	error_msg("error in monitor thread, exiting (status %d)", status);
	check_n_free(c2s);

	return (void *)status;
}

