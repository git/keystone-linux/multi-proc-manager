/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <strings.h>
#include <fcntl.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <byteswap.h>
#include <stdint.h>

#include "linux/keystone_remoteproc.h"
#include "internal/rsc_types.h"
//#include <elf.h>
// #define LOADER_DEBUG
#include "mpmloc.h"
#include "dload.h"
#include "dload_endian.h"
#include "uioutils.h"

#define IDLE_INSTRUCTION_CODE	(0x0001E000)

/* TODO: Probably this needs to be part of dts */
#define L2_ENTRY_ADDRESS 0x8ffffc

BOOL profiling_on = 0;
BOOL debugging_on = 0;

int global_argc;
char **global_argv;

struct load_segment_data {
	LOADER_FILE_DESC *fp;
	struct DLOAD_MEMORY_REQUEST *targ_req;
};

static int mpm_load_segment (char *segp, uint32_t size, void *data)
{
	int retval = -1;
	struct load_segment_data *d = (struct load_segment_data *)data;

	memset(segp, 0, d->targ_req->segment->memsz_in_bytes);

	if (DLIF_fseek (d->fp, d->targ_req->offset, SEEK_SET) < 0) {
		error_msg("DLIF_fseek failed for offset %d",
		      		d->targ_req->offset);
		goto close_n_exit;
	}

	if (DLIF_fread (segp, d->targ_req->segment->objsz_in_bytes, 1, d->fp) < 0) {
		error_msg("DLIF_fread failed for size %d",
		       d->targ_req->segment->objsz_in_bytes);
		goto close_n_exit;
	}

	retval = 0;

close_n_exit:
	return retval;
}

static int mpm_write_data (char *segp, uint32_t size, void *data)
{
	int retval = -1;

	memcpy(segp, data, size);

	retval=0;
	return(retval);
}

static int mpm_read_data (char *segp, uint32_t size, void *data)
{
	int retval = -1;

	memcpy(data, segp, size);

	retval=0;
	return(retval);
}
static int mpm_write_trap (char *segp, uint32_t size, void *data, int wrong_endian)
{
	int retval = -1;
	uint32_t *datap = (uint32_t *) segp;
	int i;

	for (i = 0; i < MAX_INSTR_COUNT; i++) {
		datap[i] = IDLE_INSTRUCTION_CODE;
		if (wrong_endian) {
			DLIMP_change_endian32(&datap[i]);
		}
	}

	retval = 0;

close_n_exit:
	return retval;
}

static int mpm_write_trampoline (char *segp, uint32_t size, void *data, int wrong_endian)
{
	int retval = -1;
	uint32_t entry_point = (uint32_t) data;
	uint32_t *reg = (uint32_t *) segp;

	/* MVK.S2  ba_low, B2 */
	*reg++ = 0x100002a | ((entry_point & 0xffff) << 7);
	/* MVKH.S2 ba_high, B2 */
	*reg++ = 0x100006a | (((entry_point >> 16) & 0xffff) << 7);
	/* BNOP.S2 B2, 5 */
	*reg++ = 0x88a362;
	*reg++ = 0;
	*reg++ = 0;
	*reg++ = 0;
	*reg++ = 0;

	if (wrong_endian) {
		/* Slave core is big endian, swap bytes of 'trampoline'
		   code so Big endian slave can execute it
		*/
		reg = (uint32_t *)segp;
		DLIMP_change_endian32(reg++);
		DLIMP_change_endian32(reg++);
		DLIMP_change_endian32(reg++);
	}

	retval = 0;

close_n_exit:
	return retval;
}

int map_and_copy_segment (mpm_slave_t *sp, uint32_t addr, uint32_t size,
						map_seg_func fpr, void *data)
{
	uioutil_map_info_t map_info;
	uint32_t global_addr;
	char *uio_class_ptr;
	uint32_t map_base;
	int ret_val = -1;
	char *segp = 0;
	char *map = 0;
	int fd = -1;
	int pg_offset, mmap_length, min_len;
	int page_size = getpagesize();
	mpm_transport_mmap_t mcfg = {
		.mmap_prot	= (PROT_READ|PROT_WRITE),
		.mmap_flags	= MAP_SHARED,
	};

	if (!sp->tr_h) {
		error_msg("Invalid transport handle for %d",
		      sp->cfgp->name);
		goto close_n_exit;
	}
	segp = mpm_transport_mmap(sp->tr_h, addr, size, &mcfg);
	if (segp == MAP_FAILED) {
		error_msg("transport mmap failed for addr 0x%x size %d (err: %s)",
		       addr, size, strerror(errno));
		goto close_n_exit;
	}

	if((*fpr)(segp, size, data)) {
		error_msg("map segment function failed");
		goto close_n_exit;
	}
	ret_val = 0;

close_n_exit:
	if (segp > 0) {
		mpm_transport_munmap(sp->tr_h, segp, size);
	}
	return ret_val;
}

#define SECMGR_DEVICE_TYPE_MASK 0x7
#define SECMGR_SEC_OVERRIDE_MASK 0x20

/* Check chip security 
 Return value : -1 : Failed to get security status
  				 0 : Security status non secure or override 
				 1 : Security status secure
*/
int mpm_check_chip_security ()
{
	int ret_val = -1;
	char *segp = 0;
	char *map = 0;
	int fd = -1;
	int pg_offset, mmap_length, min_len;
	int page_size = getpagesize();
	uint32_t security_status;
	mpm_transport_mmap_t mcfg = {
		.mmap_prot	= (PROT_READ|PROT_WRITE),
		.mmap_flags	= MAP_SHARED,
	};
	FILE *fpr = 0;
	struct mem_record_t
	{
		uint32_t base;
		uint32_t size;
	};
	struct mem_record_t mem_record;	

	fpr = fopen("/proc/device-tree/soc/secmgr/mem", "rb");
	if (!fpr) {
		info_msg("No device tree entry for secmgr: Assume non secure device");
		ret_val = 0;
		return ret_val;
	}

	if(fread(&mem_record, sizeof(struct mem_record_t), 1, fpr) <= 0 ) {
		error_msg("Unable to read device mem entry");
		goto close_n_exit;
	}

	/* endian swap values */
	mem_record.base = __bswap_32(mem_record.base);
	mem_record.size = __bswap_32(mem_record.size);

	/* Open device to read */
	fd = open("/dev/secmgr", (O_RDWR | O_SYNC));
	if (fd < 0) {
		info_msg("No secmgr uio device: Assume non secure device");
		ret_val = 0;
		goto close_n_exit;
	}

	/* Read security Status */
	if(pread(fd, &security_status, sizeof(uint32_t), mem_record.base) < 0) {
		error_msg("Unable to pread security status ");
		ret_val = -1;
		goto close_n_exit;	
	}

	if (((security_status & SECMGR_DEVICE_TYPE_MASK) == 0x5) &&
		!(security_status & SECMGR_SEC_OVERRIDE_MASK)) {
		info_msg("Detected secure device %x", security_status);
		ret_val = 1;
	} else {
		info_msg("Detected non-secure device %x", security_status);
		ret_val = 0;
	}

close_n_exit:
	if (fpr) {
		fclose(fpr);
	}
	if (fd >= 0) {
		close(fd);
	}
	return ret_val;
}

int mpm_get_global_addr (mpm_slave_t *sp, uint32_t laddr, uint32_t size, uint32_t *gaddr)
{
	int i;
	mpm_slave_cfg_t *cfgp = sp->cfgp;

	for (i = 0; i < cfgp->num_mmap; i++) {

		if (cfgp->mmap[i]->local_addr <= laddr &&
		    cfgp->mmap[i]->local_addr + cfgp->mmap[i]->length >= laddr + size) {
			*gaddr = cfgp->mmap[i]->global_addr + laddr - cfgp->mmap[i]->local_addr;
			return 0;
		}
	}

	for (i = 0; i < cfgp->num_mmap; i++) {

		if (cfgp->mmap[i]->global_addr <= laddr &&
		    cfgp->mmap[i]->global_addr + cfgp->mmap[i]->length >= laddr + size) {
			*gaddr = laddr;
			return 0;
		}
	}

	error_msg("Can't find global address from local address 0x%x size %d in cfg for %s",
	       laddr, size, cfgp->name);
	return -1;
}

int mpm_rproc_boot(mpm_slave_t *sp, unsigned int operation, uint32_t boot_addr)
{
	int retval = -1;
	struct keystone_rproc_set_state_params rproc_set_state_params;

	rproc_set_state_params.state = operation;
	rproc_set_state_params.boot_addr = boot_addr;

	if (sp->uio_fd_info.fd_status == MPM_UIO_DEVICE_STATUS_OPEN)
	{

		retval = ioctl(sp->uio_fd_info.misc_fd, KEYSTONE_RPROC_IOC_SET_STATE,
			(unsigned long)&rproc_set_state_params);
		if(retval != 0)
		{
			error_msg(" Rproc user operation %s failed %x",
				(operation == KEYSTONE_RPROC_RUNNING) ?
					"RUNNING" : "OFFLINE",
				retval);
		}
		return(retval);

	}
	else
	{
		/* Error */
		error_msg(" ERROR: device not opened ");
		return(-1);
	}

}

int mpm_rproc_cpu_start(mpm_slave_t *sp, uint32_t boot_addr)
{
	int retval = -1;
	if (sp->uio_fd_info.fd_status == MPM_UIO_DEVICE_STATUS_OPEN)
	{

		retval = ioctl(sp->uio_fd_info.misc_fd, KEYSTONE_RPROC_IOC_DSP_BOOT, boot_addr);
		if(retval != 0)
		{
			error_msg(" Rproc user cpu start %d failed %x", boot_addr, retval);
		}
		return(retval);

	}
	else
	{
		/* Error */
		error_msg(" ERROR: device not opened ");
		return(-1);
	}

}
int mpm_rproc_cpu_reset(mpm_slave_t *sp)
{
	int retval = -1;
	if (sp->uio_fd_info.fd_status == MPM_UIO_DEVICE_STATUS_OPEN)
	{

		retval = ioctl(sp->uio_fd_info.misc_fd, KEYSTONE_RPROC_IOC_DSP_RESET);
		if(retval != 0)
		{
			error_msg(" Rproc user cpu reset failed %x", retval);
		}
		return(retval);

	}
	else
	{
		/* Error */
		error_msg(" ERROR: device not opened ");
		return(-1);
	}

}

int mpm_dlif_reset(mpm_slave_t *sp)
{
	char uio_class[MAX_DEVICE_NAME_LEN];
	int retval = -1;
	uint32_t read_value;
	int fd;
	uint32_t entry_point;
	uint32_t global_magic_addr;

	if (!sp ) {
		goto close_n_exit;
	}

	if (uioutil_get_device (sp->cfgp->name, uio_class, MAX_DEVICE_NAME_LEN)) {
		error_msg("Device for UIO name %s not found for %s",
			sp->cfgp->name, sp->image_name);
		mpm_errno = MPM_ERROR_UIO;
		goto close_n_exit;
	}

	if(sp->uio_fd_info.fd_status != MPM_UIO_DEVICE_STATUS_OPEN)
	{
		mpm_errno = MPM_ERROR_UIO;
		/* TODO: Can open here if required */
		error_msg("Not expected ....");
	}

	if (mpm_rproc_cpu_reset(sp)) {
		error_msg("Can't reset DSP");
		mpm_errno = MPM_ERROR_UIO;
		goto close_n_exit;
	}

	/* Wait for a while */
	usleep(10000);
/*	WORKAROUND: Start The following is work around for issue with Cache not
	getting cleared on reset */

	if( mpm_cfg.security_status == 1) {
		/* Start DSP : Setting start address to be 0*/
		if(mpm_rproc_cpu_start(sp, 0)) {
			error_msg("Can't start cpu");
			mpm_errno = MPM_ERROR_UIO;
			goto close_n_exit;
		}
		/* Wait for a while */
		usleep(10000);
	}

	if (!mpm_cfg.scratch_block_cfg.vaddr) {
		mpm_errno = MPM_ERROR_SCRATCH_MEM_ERROR;
		error_msg("scratch block vaddr NULL");
		goto close_n_exit;
	}

	if (mpm_write_trap(mpm_cfg.scratch_block_cfg.vaddr,
	    mpm_cfg.scratch_block_cfg.length, NULL, sp->wrong_endian)) {
		error_msg("write trap failed addr 0x%x, vaddr %p size 0x%x",
			sp->image_name, (uint32_t) mpm_cfg.scratch_block_cfg.alias, mpm_cfg.scratch_block_cfg.vaddr,
			(uint32_t) mpm_cfg.scratch_block_cfg.length);
		goto close_n_exit;
	}
	entry_point = (uint32_t)mpm_cfg.scratch_block_cfg.alias;

	if( mpm_cfg.security_status == 1 ) {
		if (mpm_get_global_addr (sp, L2_ENTRY_ADDRESS, 4, &global_magic_addr) < 0) {
			error_msg("entry point  %x global address not found ",
		 		sp->entry_point);
			mpm_errno = MPM_ERROR_UIO;
			goto close_n_exit;
		}
		/* Write to L2 location for running DSP */
		if (map_and_copy_segment(sp, (uint32_t)global_magic_addr, 4, mpm_write_data, (void *) &(entry_point))) {
			error_msg("Can't write start address to %s not found for %s",
				uio_class, sp->image_name);
			mpm_errno = MPM_ERROR_WRITE_ENTRY_POINT;
			goto close_n_exit;
		}
	} else {
		if(mpm_rproc_cpu_start(sp, entry_point)) {
			error_msg("Can't start cpu");
			mpm_errno = MPM_ERROR_UIO;
			goto close_n_exit;
		}
		usleep(10000);
	}

	if (mpm_rproc_cpu_reset(sp)) {
		error_msg("Can't reset DSP");
		mpm_errno = MPM_ERROR_UIO;
		goto close_n_exit;
	}

	/* Wait for a while */
	usleep(10000);
/*	WORKAROUND: End */
	retval = 0;

close_n_exit:
	return retval;
}

int mpm_dlif_stop(mpm_slave_t *sp)
{
	int retval = -1;



	if (sp->rsc_table) {
		debug_msg ("rproc offline operation");
		retval = mpm_rproc_boot(sp, KEYSTONE_RPROC_OFFLINE, 0);
		if(retval)
		{
			error_msg("Rproc offline operation fail");
			goto close_n_exit;
		}
		free(sp->rsc_table);
		sp->rsc_table = NULL;
	}

	retval = mpm_dlif_reset(sp);
	if (retval)
		error_msg("rproc reset fail");

close_n_exit:
	return retval;
}

int mpm_dlif_init(mpm_slave_t *sp)
{
	char dev_name[MAX_DEVICE_NAME_LEN];
	int i;
	int uiofd = 0, misc_fd = 0;
	int retval = -1;
	mpm_transport_open_t ocfg = {
		.open_mode	= (O_SYNC|O_RDWR),
	};

	/* Get the device for device specific memory access */
	if (uioutil_get_device (sp->cfgp->name, sp->uio_class, MAX_DEVICE_NAME_LEN)) {
		error_msg("Device for device UIO name %s not found ",
	               sp->cfgp->name);
		goto error_exit;
	}
	snprintf(dev_name, MAX_DEVICE_NAME_LEN, "/dev/%s", sp->uio_class);

	uiofd = open(dev_name, (O_RDWR | O_SYNC));
	if (uiofd < 0) {
		error_msg("Failed to open %s: Error  (%s)",
		       dev_name, strerror(errno));
		goto error_exit;
	}
	sp->uio_fd_info.fd = uiofd;
	snprintf(dev_name, MAX_DEVICE_NAME_LEN, "/dev/%s", sp->cfgp->name);

	misc_fd = open(dev_name, (O_RDWR | O_SYNC));
	if (misc_fd < 0) {
		error_msg("Failed to open %s: Error  (%s)",
		       dev_name, strerror(errno));
		goto error_exit;
	}

	sp->uio_fd_info.misc_fd = misc_fd;
	sp->uio_fd_info.fd_status = MPM_UIO_DEVICE_STATUS_OPEN;

	sp->tr_h = mpm_transport_open(sp->cfgp->name, &ocfg);
	if (!sp->tr_h) {
		error_msg("Failed to open %s transport: retvalue %p Error  (%s)",
		       sp->cfgp->name, sp->tr_h, strerror(errno));
		goto error_exit;
	}

        return(0);

error_exit:

	if (sp->uio_fd_info.fd_status == MPM_UIO_DEVICE_STATUS_OPEN) {
		if (uiofd)
			close (uiofd);
		if (misc_fd)
			close (misc_fd);
	}

	return retval;

}
int mpm_dlif_close(mpm_slave_t *sp)
{
	int retval= 0;


	if (sp->uio_fd_info.fd_status == MPM_UIO_DEVICE_STATUS_OPEN)
	{
		if (sp->rsc_table) {
			/* Stop rproc */
			retval |= mpm_rproc_boot(sp, KEYSTONE_RPROC_OFFLINE, 0);
		} else {
			retval |= mpm_rproc_cpu_reset(sp);
		}
		retval |= close (sp->uio_fd_info.fd);
		retval |= close (sp->uio_fd_info.misc_fd);
	}

	if(sp->rsc_table) {
		free(sp->rsc_table);
		sp->rsc_table = NULL;
	}

	return(retval);
}

void read_section_info_from_image(mpm_slave_t *sp, void **tableP, int *resource_table_present, int *gdb_server_present)
{
	char *elf_data = sp->filep;
	struct Elf32_Ehdr *ehdr;
	struct Elf32_Shdr *shdr;
	const char *name_table;
	char *temp_tableP;
	int i, ret = -1;
	uint32_t name_table_offset = 0;

	*resource_table_present = -1;

	ehdr = (struct Elf32_Ehdr *)elf_data;
	if (ehdr->e_ident[EI_DATA] != DLIMP_get_endian()) {
		/* ELF file endian-ness does not match Host CPU endian-ness so
		 * swap bytes in the headers
		 */
		sp->wrong_endian = 1;
		DLIMP_change_ehdr_endian(ehdr);
	} else {
		sp->wrong_endian = 0;
	}

	shdr = (struct Elf32_Shdr *)(elf_data + ehdr->e_shoff);
	name_table_offset = shdr[ehdr->e_shstrndx].sh_offset;
	if (sp->wrong_endian) {
		DLIMP_change_endian32(&name_table_offset);
	}
	name_table = elf_data + name_table_offset;

	/* look for the resource table and handle it */
	for (i = 0; i < ehdr->e_shnum; i++, shdr++) {

		if (sp->wrong_endian) {
			DLIMP_change_shdr_endian(shdr);
		}

		if (!strcmp(name_table + shdr->sh_name, ".note")) {
			sp->elf_note.present = 1;
			sp->elf_note.address = shdr->sh_addr;
			sp->elf_note.size    = shdr->sh_size;
			info_msg("note section addr 0x%x, size 0x%x",
				sp->elf_note.address, sp->elf_note.size)
		} else if (*resource_table_present == -1) {
			if (!strcmp(name_table + shdr->sh_name, ".resource_table")
				&& (shdr->sh_size != 0)) {
				struct fw_resource *table = (struct fw_resource *)
							(elf_data + shdr->sh_offset);
				temp_tableP = malloc(shdr->sh_size+sizeof(unsigned long));
				/* Skip any zero address or zero length resource table */
				if ( temp_tableP == NULL ) {
					error_msg("Malloc failed for resource table");
					return;
				}

				*tableP = (void *)temp_tableP;
				*((unsigned long *)(temp_tableP)) = shdr->sh_size;
#ifdef DEBUG
				info_msg(" Resource table %p, size %x", temp_tableP, shdr->sh_size);
#endif
				temp_tableP += sizeof(unsigned long);
				memcpy(temp_tableP, table, shdr->sh_size);
#ifdef DEBUG
		 		{
					int i;

					debug_msg(" Resource table contents: ");
					for(i=0;i< shdr->sh_size; i++)
						debug_msg("%x : ", temp_tableP[i]);
				}
#endif
				if (mpm_get_global_addr(sp, shdr->sh_addr, 4, 
					&sp->rsc_table_runaddr) < 0) {
					error_msg("rsc_table_runaddr  %x global address not found ",
						shdr->sh_addr);
					goto err_return;
				}
				if ( sp->rsc_table_runaddr == 0 ) {
					error_msg("rsc_table_run address 0");
					goto err_return;
				}
				*resource_table_present = 0;
			}
		}

		if (!strcmp(name_table + shdr->sh_name, ".gdb_server")) {
#ifdef DEBUG
			info_msg("GDB shared address = %x", shdr->sh_addr);
#endif
			sp->gdb_server_addr = shdr->sh_addr;
			*gdb_server_present = 0;
		}
	}
	return;
err_return:
	if(*tableP) {
		free(*tableP);
		*tableP = NULL;
	}
	return;
}

int DLOAD_set_resource_table(mpm_slave_t *sp)
{
	int retval = -1;
	if (sp->uio_fd_info.fd_status == MPM_UIO_DEVICE_STATUS_OPEN)
	{

		retval = ioctl(sp->uio_fd_info.misc_fd, KEYSTONE_RPROC_IOC_SET_RSC_TABLE, (unsigned long)(sp->rsc_table));
		if(retval != 0)
		{
			error_msg(" Set Resource table fail %x",retval);
		}
		return(retval);
	}
	else
	{
		/* Error */
		error_msg(" ERROR: device not opened ");
		return(-1);
	}
}
int DLOAD_set_rsc_table_runaddr(mpm_slave_t *sp, unsigned int rsc_table_runaddr)
{
	int retval = -1;
	if (sp->uio_fd_info.fd_status == MPM_UIO_DEVICE_STATUS_OPEN)
	{

		retval = ioctl(sp->uio_fd_info.misc_fd,
			KEYSTONE_RPROC_IOC_SET_LOADED_RSC_TABLE, rsc_table_runaddr);
		if(retval != 0)
		{
			error_msg(" Set rsc Table run addr fail %x",retval);
		}
		return(retval);
	}
	else
	{
		/* Error */
		error_msg(" ERROR: device not opened ");
		return(-1);
	}
}
int mpm_wakeup_secure_dsp(mpm_slave_t *sp)
{
	char uio_class[MAX_DEVICE_NAME_LEN];
	int retval = -1;
	uint32_t global_magic_addr;

	if (uioutil_get_device (sp->cfgp->name, uio_class, MAX_DEVICE_NAME_LEN)) {
		error_msg("Device for UIO name %s not found for %s",
		       sp->cfgp->name, sp->image_name);
		mpm_errno = MPM_ERROR_UIO;
		goto close_n_exit;
	}

	if(sp->uio_fd_info.fd_status != MPM_UIO_DEVICE_STATUS_OPEN)
	{
		/* TODO: Can open here if required */
		mpm_errno = MPM_ERROR_UIO;
		error_msg(" Not expected ....");
	}
	if(mpm_rproc_cpu_start(sp, 0)) {
		error_msg("Can't start cpu");
		mpm_errno = MPM_ERROR_UIO;
		goto close_n_exit;
	}

	usleep(10000);
	retval = 0;
close_n_exit:
	return retval;
}

int mpm_dlif_load (mpm_slave_t *sp)
{
	int fd = -1;
	char *fp = NULL;
	int retval = -1;
	struct stat stat;
	int32_t fl_handle;
	char dev_name[MAX_DEVICE_NAME_LEN];
	int uiofd = 0;
	int resource_table_present;
     int gdb_server_present;

	if (!sp) {
		return -1;
	}

	fd = open (sp->image_name, O_RDONLY);
	if (fd == -1) {
		error_msg("Image file open error (%s)",
		       strerror(errno));
		retval = -1;
		mpm_errno = MPM_ERROR_FILE_OPEN;
		goto close_n_exit;
	}

	if (fstat (fd, &stat) == -1) {
		error_msg("Error getting file stat for %s (%s)",
		       sp->image_name, strerror(errno));
		retval = -1;
		mpm_errno = MPM_ERROR_FILE_OPEN;
		goto close_n_exit;
	}

	if (!S_ISREG (stat.st_mode)) {
		error_msg("%s is not a regular file",
		       sp->image_name);
		mpm_errno = MPM_ERROR_FILE_OPEN;
		retval = -1;
		goto close_n_exit;
	}

	sp->filep = mmap (0, stat.st_size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
	if (sp->filep == MAP_FAILED) {
		error_msg("MMAP failed for file %s (%s)",
		       sp->image_name, strerror(errno));
		retval = -1;
		mpm_errno = MPM_ERROR_FILE_OPEN;
		goto close_n_exit;
	}

	sp->readp = sp->filep;
	sp->image_size = stat.st_size;
	sp->dl_handle = (void *) DLOAD_create(sp);

	switch (sp->cfgp->transport) {
	case shared_memory:

		break;
	default:
		error_msg("Unsupported transport %d for %s",
		       sp->cfgp->transport, sp->image_name);
		break;
	}


	if(mpm_cfg.security_status == 1) {
		/* Wake up DSP as the code download to be persistent
		in secure DSP, it  should be out of reset */
		if(mpm_wakeup_secure_dsp(sp) < 0)
		{
			error_msg("Unable to wake up secure dsp");
			goto close_n_exit;
		}
	}

	DLOAD_initialize (sp->dl_handle);

	fl_handle = DLOAD_load((DLOAD_HANDLE)sp->dl_handle, (LOADER_FILE_DESC *)sp);
	if (!fl_handle) {
		mpm_errno = MPM_ERROR_IMAGE_LOAD;
		error_msg("Image loading failed for file %s",
		       sp->image_name);
		retval = -1;
		goto close_n_exit;
	}
	if (DLOAD_prepare_for_execution((DLOAD_HANDLE)sp->dl_handle, (uint32_t) fl_handle,
					(TARGET_ADDRESS *)&sp->entry_point, 0, NULL) != TRUE) {
		mpm_errno = MPM_ERROR_IMAGE_LOAD;
		retval = -1;
		goto close_n_exit;
	}

	resource_table_present = -1;
	gdb_server_present = -1;
	read_section_info_from_image(sp, &sp->rsc_table, &resource_table_present, &gdb_server_present);

	if(resource_table_present == 0)
	{
		/* Set resource table to remote proc */
		retval = DLOAD_set_resource_table(sp);

		if(retval != 0)
		{
			mpm_errno = MPM_ERROR_RESOURCE_TABLE_SETTING;
			/* error setting resource table */
			error_msg(" ERROR: setting resource table %d ", retval);
		}
		retval = DLOAD_set_rsc_table_runaddr(sp, sp->rsc_table_runaddr);

		if(retval != 0)
		{
			/* error setting vring addr */
			error_msg(" ERROR: setting vring addr %d ", retval);
			mpm_errno = MPM_ERROR_RESOURCE_TABLE_SETTING;
		}

	}

     /* set the gdb server section for gdbttyx */
     if(gdb_server_present == 0){

          debug_msg(" Got gdb server in image ");
          
          int gdb_fd = -1;
          char gdb_addr[9];
          char gdb_filename[13];
          snprintf(gdb_filename, 13, "/dev/gdbtty%d", sp->cfgp->core_id);
          snprintf(gdb_addr, 9, "%x", sp->gdb_server_addr);

          gdb_fd = open (gdb_filename, O_WRONLY);

          if(gdb_fd >= 0){                 
               write(gdb_fd, gdb_addr, 9);
               close(gdb_fd);
          }
     }
	retval = 0;

close_n_exit:

	if (sp->filep > 0) {
		munmap (sp->filep, sp->image_size);
		sp->filep = 0;
	}
	if (fd >= 0) close (fd);

	return retval;

}

int mpm_dlif_run (mpm_slave_t *sp)
{
	char uio_class[MAX_DEVICE_NAME_LEN];
	uioutil_map_info_t map_info;
	uint32_t gaddr;
	int retval = -1;
	uint32_t read_value;
	uint32_t global_magic_addr;
	int timeout_count=0;

	if (!sp || !sp->entry_point) {
		goto close_n_exit;
	}

	if( mpm_cfg.security_status != 1 ) {
		/* Install trampoline code to support non-aligned entrypoint */
		if (mpm_get_global_addr (sp, sp->entry_point, 4, &gaddr) < 0) {
			error_msg("entry point  %x global address not found ",
			       sp->entry_point);
			mpm_errno = MPM_ERROR_NO_ENTRY_POINT;
			goto close_n_exit;
		}


		if (!mpm_cfg.scratch_block_cfg.vaddr) {
			mpm_errno = MPM_ERROR_SCRATCH_MEM_ERROR;
			error_msg("scratch block vaddr NULL");
			goto close_n_exit;
		}
		if (mpm_write_trampoline(mpm_cfg.scratch_block_cfg.vaddr,
			mpm_cfg.scratch_block_cfg.length,
			(void *)gaddr, sp->wrong_endian)) {
			error_msg("write entry point failed addr 0x%x",
			sp->image_name, (uint32_t) sp->entry_point);
			goto close_n_exit;
		}
	}

	if (uioutil_get_device (sp->cfgp->name, uio_class, MAX_DEVICE_NAME_LEN)) {
		error_msg("Device for UIO name %s not found for %s",
		       sp->cfgp->name, sp->image_name);
		mpm_errno = MPM_ERROR_UIO;
		goto close_n_exit;
	}

	if(sp->uio_fd_info.fd_status != MPM_UIO_DEVICE_STATUS_OPEN)
	{
		/* TODO: Can open here if required */
		mpm_errno = MPM_ERROR_UIO;
		error_msg(" Not expected ....");
	}

	/* Start rproc */
	if (sp->rsc_table) {

		info_msg("calling mpm_rproc_boot",
			__func__);

		mpm_rproc_boot(sp, KEYSTONE_RPROC_RUNNING, mpm_cfg.scratch_block_cfg.alias);

	} else {
		if(mpm_rproc_cpu_start(sp, mpm_cfg.scratch_block_cfg.alias)) {
			error_msg("Can't start cpu");
			mpm_errno = MPM_ERROR_UIO;
			goto close_n_exit;
		}
	}

	usleep(10000);

	if(mpm_cfg.security_status == 1) {
		if (mpm_get_global_addr (sp, L2_ENTRY_ADDRESS, 4, &global_magic_addr) < 0) {
			error_msg("entry point  %x global address not found ",
			       sp->entry_point);
			mpm_errno = MPM_ERROR_UIO;
			goto close_n_exit;
		}
		/* Write to L2 location for running DSP */
		if (map_and_copy_segment(sp, (uint32_t)global_magic_addr, 4, mpm_write_data, (void *) &sp->entry_point)) {
			error_msg("Can't write start address to %s not found for %s",
				uio_class, sp->image_name);
			mpm_errno = MPM_ERROR_WRITE_ENTRY_POINT;
			goto close_n_exit;
		}
		do
		{
			/* Read L2 Magic address location */
			if (map_and_copy_segment(sp, (uint32_t)global_magic_addr, 4, mpm_read_data, (void *) &read_value)) {
				error_msg("Can't write start address to %s not found for %s",
					uio_class, sp->image_name);
				mpm_errno = MPM_ERROR_READ_ENTRY_POINT;
				goto close_n_exit;
			}
			usleep(1000);
		}while ((read_value != 0) 
			&& (++timeout_count <= MPM_DOWNLOAD_TIMEOUT_MS));
		if (timeout_count > MPM_DOWNLOAD_TIMEOUT_MS) {
			mpm_errno = MPM_ERROR_DOWNLOAD_TIMEOUT;
			goto close_n_exit;	
		}
	}
        usleep(10000);
	retval = 0;

close_n_exit:
	return retval;
}

int mpm_dlif_run_withpreload(mpm_slave_t *sp)
{
	char uio_class[MAX_DEVICE_NAME_LEN];
	uioutil_map_info_t map_info;
	uint32_t gaddr;
	int retval = -1;
	uint32_t read_value;
	uint32_t global_magic_addr;

	if (!sp || !sp->entry_point) {
		goto close_n_exit;
	}

	/* Start rproc */
	if (sp->rsc_table) {
		info_msg("calling mpm_rproc_boot",
			       __func__);

		mpm_rproc_boot(sp, KEYSTONE_RPROC_RUNNING, sp->entry_point);
	}

	if (mpm_get_global_addr (sp, L2_ENTRY_ADDRESS, 4, &global_magic_addr) < 0) {
		error_msg("entry point  %x global address not found ",
		       sp->entry_point);
		mpm_errno = MPM_ERROR_UIO;
		goto close_n_exit;
	}
	/* Write to L2 location for running DSP */
	if (map_and_copy_segment(sp, (uint32_t)global_magic_addr, 4, mpm_write_data, (void *) &sp->entry_point)) {
		error_msg("Can't write start address to %s not found for %s",
			uio_class, sp->image_name);
		mpm_errno = MPM_ERROR_WRITE_ENTRY_POINT;
		goto close_n_exit;
	}
#if 0
	do
	{
		/* Read L2 Magic address location */
		if (map_and_copy_segment(sp, (uint32_t)global_magic_addr, 4, mpm_read_data, (void *) &read_value)) {
			error_msg("Can't write start address to %s not found for %s",
				uio_class, sp->image_name);
			mpm_errno = MPM_ERROR_READ_ENTRY_POINT;
			goto close_n_exit;
		}
	}while (read_value != 0 );
#endif
	retval = 0;

close_n_exit:
	return retval;
}

/*****************************************************************************/
/* Client Provided File I/O                                                  */
/*****************************************************************************/
/*****************************************************************************/
/* DLIF_FSEEK() - Seek to a position in specified file.                      */
/*****************************************************************************/
int DLIF_fseek(LOADER_FILE_DESC *stream, int32_t offset, int origin)
{
	mpm_slave_t *sp = (mpm_slave_t *)stream;
	char *new_p;
/*
#if LOADER_DEBUG
	  if (debugging_on)
	  debug_msg("file %s, offset %d, origin %d",
	  sp->image_name, offset, origin);
#endif
*/
	if (!sp || !sp->filep) {
		return -EBADF;
	}

	switch (origin) {
	case SEEK_SET:
		new_p = sp->filep + offset;
		if ((new_p - sp->filep) > sp->image_size) {
			error_msg("file %s, offset %d, origin %d exceeds file size",
			       sp->image_name, offset, origin);
			return -1;

		}
		sp->readp = new_p;
		break;
	case SEEK_CUR:
		new_p = sp->readp + offset;
		if ((new_p - sp->filep) > sp->image_size) {
			error_msg("file %s, offset %d, origin %d exceeds file size",
			       sp->image_name, offset, origin);
			return -1;

		}
		sp->readp = new_p;
		break;
	case SEEK_END:
		new_p = sp->filep + sp->image_size + offset;
		if ((new_p - sp->filep) > sp->image_size) {
			error_msg("file %s, offset %d, origin %d exceeds file size",
			       sp->image_name, offset, origin);
			return -1;

		}
		sp->readp = new_p;
		break;
	default:
		return -EINVAL;
		break;
	}
	return 0;
}

/*****************************************************************************/
/* DLIF_FTELL() - Return the current position in the given file.             */
/*****************************************************************************/
int32_t DLIF_ftell(LOADER_FILE_DESC *stream)
{
	mpm_slave_t *sp = (mpm_slave_t *)stream;
	int offset;

	  if (debugging_on)
	  debug_msg("file %s", sp->image_name);

	if (!sp || !sp->filep) {
		return -1;
	}

	offset = sp->readp - sp->filep;
	if (offset > sp->image_size) {
		return -1;
	}
	return offset;
}

/*****************************************************************************/
/* DLIF_FREAD() - Read data from file into a host-accessible data buffer     */
/*      that can be accessed through "ptr".                                  */
/*****************************************************************************/
size_t DLIF_fread(void *ptr, size_t size, size_t nmemb,
		  LOADER_FILE_DESC *stream)
{
	mpm_slave_t *sp = (mpm_slave_t *)stream;
	size_t total_size = size * nmemb;


	  if (debugging_on)
	  debug_msg("file %s, size %d, nmemb %d", __func__,
	  sp->image_name, (int)size, (int)nmemb);

	if (!sp || !sp->filep) {
		return -1;
	}

	if (sp->readp - sp->filep + total_size > sp->image_size) {
		total_size = sp->image_size - (sp->readp - sp->filep);
	}
	if (total_size)
		memcpy (ptr, sp->readp, total_size);

	sp->readp += total_size;

	return total_size;
}

/*****************************************************************************/
/* DLIF_FCLOSE() - Close a file that was opened on behalf of the core        */
/*      loader. Core loader has ownership of the file pointer, but client    */
/*      has access to file system.                                           */
/*****************************************************************************/
int32_t DLIF_fclose(LOADER_FILE_DESC *fd)
{
	mpm_slave_t *sp = (mpm_slave_t *)fd;

	if (debugging_on)
		debug_msg("file %s", sp->image_name);

	if (!sp) return -1;

	if (sp->filep) {
		munmap (sp->filep, sp->image_size);
		sp->filep = 0;
	} else {
		return -1;
	}
	return 0;
}

/*****************************************************************************/
/* Client Provided Host Memory Management                                    */
/*****************************************************************************/
/*****************************************************************************/
/* DLIF_MALLOC() - Allocate host memory suitable for loader scratch space.   */
/*****************************************************************************/
void* DLIF_malloc(size_t size)
{

	if (debugging_on)
		debug_msg("size %d", (int)size);

	return malloc(size*sizeof(uint8_t));
}

/*****************************************************************************/
/* DLIF_FREE() - Free host memory previously allocated with DLIF_malloc().   */
/*****************************************************************************/
void DLIF_free(void* ptr)
{
	if(ptr) free(ptr);
}

/*****************************************************************************/
/* DLIF_ALLOCATE() - Return the load address of the segment/section          */
/*      described in its parameters and record the run address in            */
/*      run_address field of DLOAD_MEMORY_REQUEST.                           */
/*                                                                           */
/*      This function loads the segments to target memory.                   */
/*****************************************************************************/
BOOL DLIF_allocate(void* client_handle, struct DLOAD_MEMORY_REQUEST *targ_req)
{
	mpm_slave_t *sp = (mpm_slave_t *)client_handle;
	struct load_segment_data data;
	BOOL ret_val = FALSE;

	if (debugging_on)
		debug_msg("DLIF_allocate: buffer %p", targ_req->segment->target_address);

	if (!sp || !sp->filep) {
		error_msg("sp is NULL or filep is NULL %p",
		       sp);
		goto close_n_exit;
	}

	data.fp = (LOADER_FILE_DESC *)sp;
	data.targ_req = targ_req;

	if (map_and_copy_segment(sp, (uint32_t) targ_req->segment->target_address,
		(uint32_t) targ_req->segment->memsz_in_bytes, mpm_load_segment, (void *) &data)) {
		error_msg("map and copy failed for image %s with addr 0x%x, size 0x%x",
		       	sp->image_name, (uint32_t) targ_req->segment->target_address,
		       	(uint32_t) targ_req->segment->memsz_in_bytes);
		goto close_n_exit;
	}

	ret_val = TRUE;

close_n_exit:

	return ret_val;

}

/*****************************************************************************/
/* DLIF_RELEASE() - Unmap or free target memory that was previously          */
/*      allocated by DLIF_allocate().                                        */
/*****************************************************************************/
BOOL DLIF_release(void* client_handle, struct DLOAD_MEMORY_SEGMENT* ptr)
{
	if (debugging_on)
		debug_msg("DLIF_release: %d bytes starting at %p",
		       ptr->memsz_in_bytes, ptr->target_address);

	return 1;
}

/*****************************************************************************/
/* DLIF_COPY() - Copy data from file to host-accessible memory.              */
/*      Returns a host pointer to the data in the host_address field of the  */
/*      DLOAD_MEMORY_REQUEST object.                                         */
/*****************************************************************************/
BOOL DLIF_copy(struct DLOAD_MEMORY_REQUEST* targ_req)
{
	if (debugging_on)
		debug_msg("DLIF_copy: from %p bytes to %p",
		       targ_req->segment->target_address,
		       targ_req->host_address);
	return 1;
}

/*****************************************************************************/
/* DLIF_READ() - Read content from target memory address into host-          */
/*      accessible buffer.                                                   */
/*****************************************************************************/
BOOL DLIF_read(void *ptr, size_t size, size_t nmemb, TARGET_ADDRESS src)
{
	if (debugging_on)
		debug_msg("DLIF_read: from %p bytes sz %d",
		       src, (int)nmemb);

	return 1;
}

/*****************************************************************************/
/* DLIF_WRITE() - Write updated (relocated) segment contents to target       */
/*      memory.                                                              */
/*****************************************************************************/
BOOL DLIF_write(struct DLOAD_MEMORY_REQUEST* req)
{
	/*------------------------------------------------------------------------*/
	/* Nothing to do since we are relocating directly into target memory.     */
	/*------------------------------------------------------------------------*/
	return 1;
}

/*****************************************************************************/
/* DLIF_MEMCPY() - Write updated (relocated) segment contents to target      */
/*      memory.                                                              */
/*****************************************************************************/
BOOL DLIF_memcpy(void *to, void *from, size_t size)
{
	if (debugging_on)
		debug_msg("DLIF_memcpy: from %p to %p",
		       from, to);

}

/*****************************************************************************/
/* DLIF_EXECUTE() - Transfer control to specified target address.            */
/*****************************************************************************/
int32_t DLIF_execute(TARGET_ADDRESS exec_addr)
{
	/*------------------------------------------------------------------------*/
	/* This call will only work if the host and target are the same instance. */
	/* The compiler may warn about this conversion from an object to a        */
	/* function pointer.                                                      */
	/*------------------------------------------------------------------------*/
	/* Currently it is used only for remote target download and execution
	 in remote target not supported*/
	error_msg("Dynamic loading currently not supported: Ignoring Exec addr %p",
		       exec_addr);
//	return ((int32_t(*)())(exec_addr))();
	return(0);
}


/*****************************************************************************/
/* Client Provided Communication Mechanisms to assist with creation of       */
/* DLLView debug information.  Client needs to know exactly when a segment   */
/* is being loaded or unloaded so that it can keep its debug information     */
/* up to date.                                                               */
/*****************************************************************************/
/*****************************************************************************/
/* DLIF_LOAD_DEPENDENT() - Perform whatever maintenance is needed in the     */
/*      client when loading of a dependent file is initiated by the core     */
/*      loader.  Open the dependent file on behalf of the core loader,       */
/*      then invoke the core loader to get it into target memory. The core   */
/*      loader assumes ownership of the dependent file pointer and must ask  */
/*      the client to close the file when it is no longer needed.            */
/*                                                                           */
/*      If debug support is needed under the Braveheart model, then create   */
/*      a host version of the debug module record for this object.  This     */
/*      version will get updated each time we allocate target memory for a   */
/*      segment that belongs to this module.  When the load returns, the     */
/*      client will allocate memory for the debug module from target memory  */
/*      and write the host version of the debug module into target memory    */
/*      at the appropriate location.  After this takes place the new debug   */
/*      module needs to be added to the debug module list.  The client will  */
/*      need to update the tail of the DLModules list to link the new debug  */
/*      module onto the end of the list.                                     */
/*                                                                           */
/*****************************************************************************/
int DLIF_load_dependent(void* client_handle, const char* so_name)
{

	return 0;
}

/*****************************************************************************/
/* DLIF_UNLOAD_DEPENDENT() - Perform whatever maintenance is needed in the   */
/*      client when unloading of a dependent file is initiated by the core   */
/*      loader.  Invoke the DLOAD_unload() function to get the core loader   */
/*      to release any target memory that is associated with the dependent   */
/*      file's segments.                                                     */
/*****************************************************************************/
void DLIF_unload_dependent(void* client_handle, uint32_t file_handle)
{

}

/*****************************************************************************/
/* Client Provided API Functions to Support Logging Warnings/Errors          */
/*****************************************************************************/

/*****************************************************************************/
/* DLIF_WARNING() - Write out a warning message from the core loader.        */
/*****************************************************************************/
void DLIF_warning(LOADER_WARNING_TYPE wtype, const char *fmt, ...)
{
	va_list ap;
	va_start(ap,fmt);
	printf("<< D L O A D >> WARNING: ");
	vprintf(fmt,ap);
	va_end(ap);
}

/*****************************************************************************/
/* DLIF_ERROR() - Write out an error message from the core loader.           */
/*****************************************************************************/
void DLIF_error(LOADER_ERROR_TYPE etype, const char *fmt, ...)
{
	char buf[128];
	va_list ap;
	va_start(ap,fmt);
	error_msg("<< D L O A D >> ERROR: ");
	vsnprintf(buf, 128, fmt, ap);
	va_end(ap);
	error_msg(buf);
}

/*****************************************************************************/
/* DLIF_trace() - Write out a trace from the core loader.                    */
/*****************************************************************************/
void DLIF_trace(const char *fmt, ...)
{
	char buf[128];
	va_list ap;
	va_start(ap,fmt);
	vsnprintf(buf, 128, fmt, ap);
	va_end(ap);
	debug_msg(buf);
}

/*****************************************************************************
 * END API FUNCTION DEFINITIONS
 *****************************************************************************/

/*****************************************************************************/
/* DLIF_assign_dsbt_indices()                                                */
/*                                                                           */
/*    When the core loader completes allocation of the top-level object      */
/*    being loaded and the allocation for all dependent files, this function */
/*    is called to bind objects that have just been allocated to their DSBT  */
/*    index (as determined by the client). We will first honor any specific  */
/*    index requests that have been made. Then remaining DSBT entries will   */
/*    be assigned in the order that they were encountered during the load    */
/*    to each available slot in the master DSBT.                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*    It is assumed that AL_initialize has been called to set up the initial */
/*    state of the client's model of the DSBT master.                        */
/*                                                                           */
/*    Error conditions should have been detected during registration of each */
/*    DSBT index request. I don't think there are any error/warning          */
/*    situations that need to be handled within this function.               */
/*                                                                           */
/*****************************************************************************/
void DLIF_assign_dsbt_indices(void)
{
}

/*****************************************************************************/
/* DLIF_update_all_dsbts()                                                   */
/*                                                                           */
/*    Update all DSBTs for the application and all libraries that use the    */
/*    DSBT model. Each DSBT index request entry was provided with the        */
/*    address and size of the DSBT contained in the loaded application.      */
/*    The client simply needs to copy the content of its master copy of the  */
/*    DSBT to each module's own DSBT. The client will check the size of      */
/*    each module's DSBT to see if it is big enough to hold the master copy  */
/*    of the DSBT before actually copying the master to the module's DSBT.   */
/*    An error will be emitted if a module's allocated DSBT is not big       */
/*    enough to hold the master DSBT.                                        */
/*                                                                           */
/*****************************************************************************/
BOOL DLIF_update_all_dsbts()
{
	return TRUE;
}

/*****************************************************************************/
/* DLIF_get_dsbt_index()                                                     */
/*                                                                           */
/*    Find specified file handle among the list of DSBT request entries.     */
/*    Then return the DSBT index that has been assigned to that file         */
/*    handle. Emit an error if the file handle is not found among the list   */
/*    of DSBT request entries or if a DSBT assignment has not been made      */
/*    for the specified file yet.                                            */
/*                                                                           */
/*****************************************************************************/
int32_t DLIF_get_dsbt_index(int32_t file_handle)
{
	return DSBT_INDEX_INVALID;
}

/*****************************************************************************/
/* DLIF_register_dsbt_index_request()                                        */
/*                                                                           */
/*    Register a request for a DSBT index from a dynamic executable or a     */
/*    dynamic library. An executable must make a specific request for the    */
/*    0th slot in the DSBT. Dynamic libraries can make a specific index      */
/*    request or have the client assign an index on its behalf when the      */
/*    allocation and relocation of symbols is completed for a top-level      */
/*    load (load invoked by client's "load" command, for example).           */
/*                                                                           */
/*    If a specific request is made for an index that has already been       */
/*    assigned or specifically requested by an earlier request, then an      */
/*    error will be emitted and the loader core should fail the load.        */
/*                                                                           */
/*    The information provided with the request will include the requesting  */
/*    module's so_name and file handle, along with the index requested and   */
/*    the index assigned.                                                    */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*    It is assumed that AL_initialize has been called to set up the initial */
/*    state of the client's model of the DSBT master.                        */
/*                                                                           */
/*****************************************************************************/
BOOL DLIF_register_dsbt_index_request(DLOAD_HANDLE handle,
				      const char *requestor_name,
				      int32_t     requestor_file_handle,
				      int32_t     requested_dsbt_index)
{
	return TRUE;
}
