/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <strings.h>
#include <fcntl.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <dirent.h>

#include "mpmloc.h"

int mpm_service_transport(mpm_slave_t *p, char *op)
{
	int ret;
	mpm_transport_open_t ocfg = {
		.open_mode	= (O_SYNC|O_RDWR),
		.msec_timeout = 10000,
		.interface_mode = MPM_TRANSPORT_INTERFACE_MODE_TX_AND_RX,
	};
	if (strcmp(op, "open")==0)
	{
		debug_msg("opening transport name %s",p->cfgp->name);
		ret = mpm_transport_peripheral_enable(p->cfgp->name, &ocfg);
		debug_msg("ret value = %d",ret);
		return ret;
	}
	if (strcmp(op, "open_tx_only")==0)
	{
		debug_msg("opening tx only transport name %s",p->cfgp->name);
		ocfg.interface_mode = MPM_TRANSPORT_INTERFACE_MODE_TX_ONLY;
		ret = mpm_transport_peripheral_enable(p->cfgp->name, &ocfg);
		debug_msg("ret value = %d",ret);
		return ret;
	}

	else if (strcmp(op, "close")==0)
	{
		debug_msg("closing transport name %s",p->cfgp->name);
		ret = mpm_transport_peripheral_disable(p->cfgp->name);
		debug_msg("ret value = %d",ret);
		return ret;
	}
	else {
		error_msg("Invalid transport command %s\n", op);
		return -1;
	}
}
