/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <strings.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>


#include "mpmloc.h"

mpm_slave_t *mpm_slave = NULL;

static void mpm_ssm_state_idle(mpm_slave_t *sp, mpm_event_e event, void * data);
static void mpm_ssm_state_running(mpm_slave_t *sp, mpm_event_e event, void * data);
static void mpm_ssm_state_loaded(mpm_slave_t *sp, mpm_event_e event, void * data);
static void mpm_ssm_state_reset(mpm_slave_t *sp, mpm_event_e event, void * data);
static void mpm_ssm_state_crashed(mpm_slave_t *sp, mpm_event_e event, void * data);
static void mpm_ssm_state_error(mpm_slave_t *sp, mpm_event_e event, void * data);

int mpm_load_slave (mpm_slave_t *sp, char *filename);
int mpm_load_withpreload_slave (mpm_slave_t *sp, char *data);
int mpm_run_slave (mpm_slave_t *sp, void *data);
int mpm_run_withpreload_slave (mpm_slave_t *sp, void *data);
int mpm_reset_slave (mpm_slave_t *sp, void *data);
int mpm_transport_slave (mpm_slave_t *sp, char *op);

ccup_state_entry mpm_slave_state_table[last_state] =
{
	{mpm_ssm_state_idle,	0,	idle_state,	"idle"},
	{mpm_ssm_state_reset,	0,	reset_state,	"reset"},
	{mpm_ssm_state_loaded,	0,	loaded_state,	"loaded"},
	{mpm_ssm_state_running,	0,	running_state,	"running"},
	{mpm_ssm_state_crashed,	0,	crashed_state,	"crashed"},
	{mpm_ssm_state_error,	0,	error_state,	"error"}
};

mpm_event_entry_t mpm_slave_event_table[last_ev] =
{
	{entry_ev,	"entry"},

	{load_ev,	"load"},
	{run_ev,	"run"},
	{crash_ev,	"crash"},
	{reset_ev,	"reset"},
	{transport_ev, "transport"},
	{load_withpreload_ev, "load_withpreload"},
	{run_withpreload_ev, "run_withpreload"}
	
};

int mpm_ssm_init(void)
{
	int i;

	if (mpm_slave) {
		error_msg ("It should be called only once");
		return -1;
	}

	mpm_slave = calloc(mpm_cfg.num_slaves, sizeof(mpm_slave_t));
	if (!mpm_slave) {
		error_msg ("Can't allocate memory for mpm_slave (%s)",
				strerror(errno));
		return -1;
	}
        memset((void *)mpm_slave, 0, mpm_cfg.num_slaves*sizeof(mpm_slave_t));

	for (i = 0; i < mpm_cfg.num_slaves; i++) {
		mpm_slave[i].state = idle_state;
		mpm_slave[i].cfgp = &mpm_cfg.slave[i];
	}
	for (i = 0; i < mpm_cfg.num_slaves; i++) {
		mpm_dlif_init(&mpm_slave[i]);
	}
	return 0;
}

int mpm_ssm_close(void)
{
	int i;
	int ret_value=0;

	for (i = 0; i < mpm_cfg.num_slaves; i++) {
		ret_value |= mpm_dlif_close(&mpm_slave[i]);
	}
	free(mpm_slave);
		
	return(ret_value);
}
void mpm_ssm_goto (mpm_slave_t *sp, mpm_state_e state, void *data)
{
	sp->state = state;
	mpm_slave_state_table[state].func(sp, entry_ev, data);
}

int mpm_ssm_event (mpm_slave_t *sp, mpm_event_e event, void *data)
{
	mpm_state_e state = sp->state;

	if (event > last_ev) {
		error_msg ("SSM received invalid event %d for %s",
				event, sp->cfgp->name);
		return -1;
	}

	mpm_slave_state_table[state].func(sp, event, data);

	return 0;
}

/* State machine functions */

void mpm_ssm_state_idle (mpm_slave_t *sp, mpm_event_e event, void * data)
{
	int ret;

	switch(event)
	{
	case entry_ev:
		break;

	case load_ev:
		if (mpm_dlif_reset(sp)) {
			mpm_ssm_goto (sp, error_state, NULL);
		}
		if (!mpm_load_slave (sp, (char *) data)) {
			mpm_ssm_goto (sp, loaded_state, NULL);
		} else {
			mpm_ssm_goto (sp, error_state, NULL);
		}
		break;

	case crash_ev:
	case run_ev:
	case run_withpreload_ev:
		error_msg ("received unexpected %s event for %s",
				mpm_slave_event_table[event].name,
				sp->cfgp->name);
		break;

	case reset_ev:
		if (mpm_dlif_reset(sp)) {
			mpm_ssm_goto (sp, error_state, NULL);
		}
		mpm_ssm_goto (sp, reset_state, NULL);
		break;

	case transport_ev:
		ret = mpm_transport_slave(sp, (char *) data);
		if (ret < 0)
			mpm_errno = MPM_ERROR_TRANSPORT_OPERATION_FAIL;
		break;

	case load_withpreload_ev:
		if (mpm_dlif_reset(sp)) {
			mpm_ssm_goto (sp, error_state, NULL);
		}
		if (!mpm_load_withpreload_slave (sp, (char *) data)) {
			mpm_ssm_goto (sp, loaded_state, NULL);
		} else {
			mpm_ssm_goto (sp, error_state, NULL);
		}
		break;

	default:
		error_msg ("received invalid event %d for %s",
				event, sp->cfgp->name);
		break;
	}
}

void mpm_ssm_state_reset (mpm_slave_t *sp, mpm_event_e event, void * data)
{
	int ret;

	switch(event)
	{
	case entry_ev:
		break;

	case load_ev:
		if (!mpm_load_slave (sp, (char *) data)) {
			mpm_ssm_goto (sp, loaded_state, NULL);
		} else {
			mpm_ssm_goto (sp, error_state, NULL);
		}
		break;

	case transport_ev:
		ret = mpm_transport_slave(sp, (char *) data);
		if (ret < 0)
			mpm_errno = MPM_ERROR_TRANSPORT_OPERATION_FAIL;
		break;
		break;

	case load_withpreload_ev:
		if (!mpm_load_withpreload_slave (sp, (char *) data)) {
			mpm_ssm_goto (sp, loaded_state, NULL);
		} else {
			mpm_ssm_goto (sp, error_state, NULL);
		}
		break;

	case reset_ev:
		mpm_reset_slave (sp, data);
		mpm_ssm_goto (sp, reset_state, NULL);
		break;

	case crash_ev:
	case run_ev:
		error_msg ("error: received unexpected %s event for %s: No action",
				mpm_slave_event_table[event].name,
				sp->cfgp->name);
		break;

	default:
		error_msg ("received invalid event %d for %s",
				event, sp->cfgp->name);
		break;
	}
}

void mpm_ssm_state_loaded (mpm_slave_t *sp, mpm_event_e event, void * data)
{
	int ret;

	switch(event)
	{
	case entry_ev:
		/* Do IPC stuff */
		break;

	case load_ev:
		if (!mpm_load_slave (sp, (char *) data)) {
			mpm_ssm_goto (sp, loaded_state, NULL);
		} else {
			mpm_ssm_goto (sp, error_state, NULL);
		}
		break;

	case run_withpreload_ev:
		if (!mpm_run_withpreload_slave (sp, (char *) data)) {
			mpm_ssm_goto (sp, running_state, NULL);
		} else {
			mpm_ssm_goto (sp, error_state, NULL);
		}
		break;

	case run_ev:
		if (!mpm_run_slave (sp, data)) {
			mpm_ssm_goto (sp, running_state, NULL);
		} else {
			mpm_ssm_goto (sp, error_state, NULL);
		}
		break;
		
	case transport_ev:
		ret = mpm_transport_slave(sp, (char *) data);
		if (ret < 0)
			mpm_errno = MPM_ERROR_TRANSPORT_OPERATION_FAIL;
		break;
		break;

	case reset_ev:
		mpm_reset_slave (sp, data);
		mpm_ssm_goto (sp, reset_state, NULL);
		break;

	case crash_ev:
		error_msg ("received unexpected %s event for %s\n",
				mpm_slave_event_table[event].name,
				sp->cfgp->name);
		break;

	default:
		error_msg ("received invalid event %d for %s\n",
				event, sp->cfgp->name);
		break;
	}
}

void mpm_ssm_state_running (mpm_slave_t *sp, mpm_event_e event, void * data)
{
	int ret;

	switch(event)
	{
	case entry_ev:
		/* start runtime services */
		break;

	case load_ev:
	case run_ev:
		error_msg ("received unexpected %s event for %s\n",
				mpm_slave_event_table[event].name,
				sp->cfgp->name);
		mpm_errno = MPM_ERROR_SSM_UNEXPECTED_EVENT;
		break;

	case crash_ev:
		mpm_ssm_goto (sp, crashed_state, NULL);
		break;

	case reset_ev:
		mpm_reset_slave (sp, data);
		mpm_ssm_goto (sp, reset_state, NULL);
		break;

	case transport_ev:
		ret = mpm_transport_slave(sp, (char *) data);
		if (ret < 0)
			mpm_errno = MPM_ERROR_TRANSPORT_OPERATION_FAIL;
		break;
		break;

	default:
		error_msg ("received invalid event %d for %s\n",
				event, sp->cfgp->name);
		mpm_errno = MPM_ERROR_SSM_INVALID_EVENT;
		break;
	}
}

void mpm_ssm_state_crashed (mpm_slave_t *sp, mpm_event_e event, void * data)
{
	int retval;
	char callback_string[2*MAX_FILE_NAME_LEN] = {0};

	switch(event)
	{
	case entry_ev:
		/* send crash notification */
		if (strlen(sp->cfgp->crash_callback) > 0) {
			retval = access(sp->cfgp->crash_callback, F_OK | R_OK | X_OK);
			if (retval == -1) {
				error_msg("can't execute crash callback script %s (err: %s)",
					sp->cfgp->crash_callback, strerror(errno));
				break;
			}
			sprintf(callback_string, "%s %s",
				sp->cfgp->crash_callback, sp->cfgp->name);

			system(callback_string);

			error_msg("called crashdump script %s",
				callback_string);
		}
		break;

	case load_ev:
	case run_ev:
	case crash_ev:
	case transport_ev:
		error_msg ("received unexpected %s event for %s\n",
				mpm_slave_event_table[event].name,
				sp->cfgp->name);
		mpm_errno = MPM_ERROR_SSM_UNEXPECTED_EVENT;
		break;

	case reset_ev:
		mpm_reset_slave (sp, data);
		mpm_ssm_goto (sp, reset_state, NULL);
		break;

	default:
		error_msg ("received invalid event %d for %s\n",
				event, sp->cfgp->name);
		mpm_errno = MPM_ERROR_SSM_INVALID_EVENT;
		break;
	}
}

void mpm_ssm_state_error (mpm_slave_t *sp, mpm_event_e event, void * data)
{
	switch(event)
	{
	case entry_ev:
		/* send error notification */
		error_msg ("entered error state for %s\n",
				sp->cfgp->name);
		break;

	case load_ev:
	case run_ev:
	case crash_ev:
	case transport_ev:
		error_msg ("received unexpected %s event for %s\n",
				mpm_slave_event_table[event].name,
				sp->cfgp->name);
		break;

	case reset_ev:
		mpm_reset_slave (sp, data);
		mpm_ssm_goto (sp, reset_state, NULL);
		break;

	default:
		error_msg ("received invalid event %d for %s\n",
				event, sp->cfgp->name);
		break;
	}

}

int mpm_load_withpreload_slave (mpm_slave_t *sp, char *data)
{
	char preload_filename[MAX_FILE_NAME_LEN];
	char load_filename[MAX_FILE_NAME_LEN];

	debug_msg("loading %s %s", sp->cfgp->name, sp);
	sscanf(data,"%[^ ]%s", preload_filename, load_filename);
	debug_msg("loading with preload %s, %s", preload_filename, load_filename);	

	if (strlen (preload_filename) > MAX_FILE_NAME_LEN) {
		error_msg ("Image preload filename should be less then %d, received %d\n",
				MAX_FILE_NAME_LEN, (int) strlen(preload_filename));
		mpm_errno = MPM_ERROR_INVALID_NAME_LENGTH;
		return -1;
	}

	if (strlen (load_filename) > MAX_FILE_NAME_LEN) {
		error_msg ("Image load filename should be less then %d, received %d\n",
				MAX_FILE_NAME_LEN, (int) strlen(load_filename));
		mpm_errno = MPM_ERROR_INVALID_NAME_LENGTH;
		return -1;
	}

	/* Just do the preload first */
	strcpy (sp->image_name, preload_filename);

	if (mpm_dlif_load(sp)) {
		error_msg("Image loading failed for %s",
				sp->image_name);
		return -1;
	}

	/* run the preloaded code */
	debug_msg("running %s", sp->cfgp->name);
	if (mpm_dlif_run(sp)) {
		error_msg("Image running failed for %s\n",
				sp->image_name);
		return -1;
	}

	/* Load now the main dsp application image */
	strcpy (sp->image_name, load_filename);

	if (mpm_dlif_load(sp)) {
		error_msg("Image loading failed for %s",
				sp->image_name);
		return -1;
	}

	return 0;
}


int mpm_load_slave (mpm_slave_t *sp, char *filename)
{
	debug_msg("loading %s", sp->cfgp->name);
	if (strlen (filename) > MAX_FILE_NAME_LEN) {
		error_msg ("Image filename should be less then %d, received %d\n",
				MAX_FILE_NAME_LEN, (int) strlen(filename));
		mpm_errno = MPM_ERROR_INVALID_NAME_LENGTH;
		return -1;
	}
	strcpy (sp->image_name, filename);

	if (mpm_dlif_load(sp)) {
		error_msg("Image loading failed for %s : image ",
				sp->cfgp->name, sp->image_name);
		return -1;
	}

	return 0;
}

int mpm_run_slave (mpm_slave_t *sp, void *data)
{
	debug_msg("running %s", sp->cfgp->name);
	if (mpm_dlif_run(sp)) {
		error_msg("Image running failed for %s: image %s\n",
				sp->cfgp->name, sp->image_name);
		return -1;
	}
	return 0;
}

int mpm_run_withpreload_slave (mpm_slave_t *sp, void *data)
{
	debug_msg("running %s", sp->cfgp->name);
	if (mpm_dlif_run_withpreload(sp)) {
		error_msg("Image running with preload failed for %s\n",
				sp->cfgp->name);
		return -1;
	}
	return 0;
}

int mpm_reset_slave (mpm_slave_t *sp, void *data)
{
	error_msg("resetting %s", sp->cfgp->name);
	if(mpm_dlif_stop(sp)) {
		error_msg("Reset DSP failed for %s",
				sp->cfgp->name);
		return -1;
	}

	return 0;
}

int mpm_transport_slave (mpm_slave_t *sp, char *op)
{
	debug_msg("setting up transport for %s", sp->cfgp->name);

	if (mpm_service_transport(sp,op)) {
		error_msg("Transport %s failed for %s\n", op,
				sp->cfgp->name);
		return -1;
	}

	return 0;
}
