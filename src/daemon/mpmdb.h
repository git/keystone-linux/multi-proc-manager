/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __MPMDB_H__
#define __MPMDB_H__

#include <stdio.h>
#include <stdint.h>
#include "sockutils.h"

#define JSON_PARSER_VERSION "1.0.0.0"

#define MAX_FILE_NAME_LEN	128
#define MPM_MAX_NAME_LENGTH	32
#define MPM_MAX_MMAP_SECTIONS	16
#define MPM_MAX_SEGMENTS	64
#define MPM_MAX_SLAVES		32
#define MPM_MAX_TRANS_PARAMS 5
#define MPM_MAX_TRANSPORTS		32

typedef enum mpm_interface_tag {
	log_daemon,	/*stderr, stdout, syslog, default*/
	log_file,	/*<filename not matching above>*/
} mpm_interface_e;

typedef  struct mpm_mmap_tag {
	char name[MPM_MAX_NAME_LENGTH];
	uint64_t local_addr;
	uint64_t global_addr;
	uint64_t length;
} mpm_mmap_t;

typedef enum mpm_transport_tag {
	shared_memory,
	srio,
	pcie,
	hyperlink,
	ethernet,
} mpm_transport_e;

typedef  enum mpm_sharing_tag {
	explicit,
	implicit,
} mpm_sharing_e;


typedef  struct mpm_slave_cfg_tag {
	char name[MPM_MAX_NAME_LENGTH];
	uint32_t core_id;
	mpm_transport_e transport;
	mpm_sharing_e sharing;
	uint32_t cluster_id;
	int num_mmap;
	mpm_mmap_t *mmap[MPM_MAX_MMAP_SECTIONS];
	char crash_callback[MAX_FILE_NAME_LEN];
	uint32_t transport_params[MPM_MAX_TRANS_PARAMS];
} mpm_slave_cfg_t;

typedef struct scratch_block_cfg_tag {
	uint32_t alias;
	uint32_t length;
	off_t paddr;
	void *vaddr;
} scratch_block_cfg_t;

typedef  struct mpm_cfg_tag {
#if 0
	mpm_interface_e inputif;
#endif
	mpm_interface_e outputif;
	int num_segments;
	int num_slaves;
	char *cmdfile;
	FILE *logfile_p;
	sock_h serv_sock_h;
	scratch_block_cfg_t scratch_block_cfg;
	uint32_t socket_perm;
	uint32_t security_status;
	mpm_mmap_t segment[MPM_MAX_SEGMENTS];
	mpm_slave_cfg_t slave[MPM_MAX_SLAVES];
} mpm_cfg_t;

extern mpm_cfg_t mpm_cfg;


#define find_slave_p(slave_name, slave_p) \
do { \
	int count; \
	for (count = 0; count < mpm_cfg.num_slaves; count++) { \
		if (!strcmp(slave_name, mpm_slave[count].cfgp->name)) break; \
	} \
	if (count == mpm_cfg.num_slaves) { \
		slave_p = NULL; \
	} else { \
		slave_p = &mpm_slave[count]; \
	} \
} while(0)

#endif /*__MPMDB_H__*/
