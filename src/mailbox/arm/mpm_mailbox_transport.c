/*
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 
 * 
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <mpm_transport.h>
#include "mpm_mailbox_loc.h"


void *mpm_mailbox_transport_open( char *slave_name)
{
  mpm_transport_h transport_handle = (mpm_transport_h)NULL;

  if ( slave_name && strlen(slave_name) > 0 )
  {
    mpm_transport_open_t mpm_transport_cfg;

    mpm_transport_cfg.open_mode = (S_IRUSR|S_IWUSR);

    transport_handle = mpm_transport_open(slave_name, &mpm_transport_cfg);
  }
  return ((void *)transport_handle);
}


void mpm_mailbox_transport_close(void *transport_handle)
{
  if ( transport_handle )
    mpm_transport_close((mpm_transport_h)transport_handle);
}


int32_t mpm_mailbox_transport_read( void *transport_handle, uint32_t addr, void *buf, uint32_t size)
{
  int32_t ret_val = 0;

  if(transport_handle == NULL)
  {
    memcpy(buf,(void *)(intptr_t)addr,size);
  } else 
  {
    mpm_transport_read_t mpm_transport_rcfg;

    ret_val = mpm_transport_read((mpm_transport_h)transport_handle, addr, size, buf, &mpm_transport_rcfg);
  }
  return (ret_val);
}

int32_t mpm_mailbox_transport_write( void *transport_handle, uint32_t addr, void *buf, uint32_t size)
{
  int32_t ret_val = 0;

  if(transport_handle == NULL)
  {
    memcpy((void *)(intptr_t)addr,(void *)buf,size);
  }
  else {
    mpm_transport_write_t mpm_transport_wcfg;

    ret_val = mpm_transport_write((mpm_transport_h)transport_handle, addr, size, buf, &mpm_transport_wcfg);
  }
  return (ret_val);
}

void *mpm_mailbox_transport_map( void *transport_handle, uint32_t addr, uint32_t size)
{
  void *ptr = NULL;

  if ( transport_handle )
  {
    mpm_transport_mmap_t mpm_transport_mcfg;

    mpm_transport_mcfg.mmap_prot   = (PROT_READ|PROT_WRITE);
    mpm_transport_mcfg.mmap_flags  = MAP_SHARED;

    ptr = mpm_transport_mmap((mpm_transport_h)transport_handle, addr, size, &mpm_transport_mcfg);
  }
  return (ptr);
}


void mpm_mailbox_transport_unmap( void *transport_handle, void *ptr, uint32_t size)
{
  if ( transport_handle )
    mpm_transport_munmap((mpm_transport_h)transport_handle, ptr, size);
}


void mpm_mailbox_notify(mpm_mailbox_inst_t *inst)
{
    return;
}

