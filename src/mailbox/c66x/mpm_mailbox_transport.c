/*
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 
 * 
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

//#include <mpm_transport.h>
#include "mpm_mailbox_loc.h"

void *mpm_mailbox_transport_open( char *slave_name)
{
  return (NULL);
}


void mpm_mailbox_transport_close(void *transport_handle)
{
  return;
}


int32_t mpm_mailbox_transport_read( void *transport_handle, uint32_t addr, void *buf, uint32_t size)
{
  int32_t ret_val = 0;

  if(transport_handle == NULL)
  {
    memcpy(buf,(void *)(intptr_t)addr,size);
  } else 
  {
    ret_val = -1;
  }
  return (ret_val);
}

int32_t mpm_mailbox_transport_write( void *transport_handle, uint32_t addr, void *buf, uint32_t size)
{
  int32_t ret_val = 0;

  if(transport_handle == NULL)
  {
    memcpy((void *)(intptr_t)addr,(void *)buf,size);
  }
  else {
    ret_val = -1;
  }
  return (ret_val);
}

void *mpm_mailbox_transport_map( void *transport_handle, uint32_t addr, uint32_t size)
{
  void *ptr = NULL;

  return (ptr);
}


void mpm_mailbox_transport_unmap( void *transport_handle, void *ptr, uint32_t size)
{
  return;
}


void mpm_mailbox_notify(mpm_mailbox_inst_t *inst)
{
  return;
}

