/*
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 
 * 
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



#ifndef _MPM_MAILBOX_LOC_H
#define _MPM_MAILBOX_LOC_H
#include "stdint.h"
#include "mpm_mailbox.h"

#define MPM_MAILBOX_SLOT_OWNER_LOCAL    0xBABEFACE
#define MPM_MAILBOX_SLOT_OWNER_REMOTE   0xC00FFEEE

typedef struct mpm_mailbox_slot_header_s {
  uint32_t trans_id;             /* transaction id   */
  uint32_t payload_size;          /* size of payload */
  uint32_t owner;                /* owner code */
} mpm_mailbox_slot_header_t;

typedef struct mpm_mailbox_slot_s {
  mpm_mailbox_slot_header_t  slot_header;    /* Slot header */
  uint8_t                    mailbox_payload[1];    /* Number of payload bytes set to dummy 1: Actual size dynamically configured */
} mpm_mailbox_slot_t;

typedef struct mpm_mailbox_header_s {
  uint32_t owner_code;           /* Owner code Local or remote */
  uint32_t write_index;           /* Write index */
  uint32_t read_index;            /* Read index */
} mpm_mailbox_header_t;

typedef struct mpm_mailbox_s {
  mpm_mailbox_header_t mailbox_header;
  mpm_mailbox_slot_t   slots[1];      /* Number of slots set to dummy 1: Actual size dynamically configured  */
} mpm_mailbox_t;

typedef struct mpm_mailbox_inst_s {
  uint32_t mem_location;       /* Location of memory local or remote */
  uint32_t direction;          /* Direction: Send or recieve from local perspective */
  uint32_t mem_start_addr;     /* Location of mailbox */
  uint32_t mem_size;           /* size of Mailbox */
  uint32_t max_payload_size;   /* Maximum payload size */
  void    *transport_handle;   /* transport handle */
  uint32_t depth;              /* Depth of mailbox */
  uint32_t slot_size;          /* Size of one mailbox slot */
  uint32_t write_counter;      /* Number of writes to mailbox: Applicable only to Send mailbox   */
  uint32_t read_counter;       /* Number of reads to mailbox: Applicable only to receive mailbox */
} mpm_mailbox_inst_t ;

typedef struct mpm_mailbox_context_s {
  uint32_t local_node_id;       /* Local node id */
}mpm_mailbox_context_t;

void mpm_mailbox_notify(mpm_mailbox_inst_t *inst);

void   *mpm_mailbox_transport_open  ( char *slave_name);
void    mpm_mailbox_transport_close ( void *transport_handle);
int32_t mpm_mailbox_transport_read  ( void *transport_handle, uint32_t addr, void *buf, uint32_t size);
int32_t mpm_mailbox_transport_write ( void *transport_handle, uint32_t addr, void *buf, uint32_t size);
void   *mpm_mailbox_transport_map   ( void *transport_handle, uint32_t addr, uint32_t size);
void    mpm_mailbox_transport_unmap ( void *transport_handle, void *ptr, uint32_t size);

#endif /* _MPM_MAILBOX_LOC_H */

/*** nothing past this point ***/

