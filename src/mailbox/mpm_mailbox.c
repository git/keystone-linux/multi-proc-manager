/*
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 
 * 
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



#include "mpm_mailbox_loc.h"
#include "mpm_mailbox.h"
#include <string.h>
#include <stdio.h>


/**
 *  @brief Function mpm_mailbox_put_header() Write mailbox header from local buffer
 *  @param[in]     inst             Mailbox instance
 *  @param[in]     mailbox_header    Mailbox Header pointer
 *  @retval        0 for success, -1 for failure
 *  @pre
 *  @post
 */
int32_t mpm_mailbox_put_header(mpm_mailbox_inst_t *inst, mpm_mailbox_header_t *mailbox_header)
{
    void *transport_handle;
    int32_t ret_val;
    mpm_mailbox_header_t *mailbox_addr = (mpm_mailbox_header_t *)(intptr_t)inst->mem_start_addr;

    if(inst->mem_location == MPM_MAILBOX_MEMORY_LOCATION_REMOTE)
    {
      transport_handle = inst->transport_handle;
    } else
    {
      /* Indicates local copy */
      transport_handle = NULL;
    }
    ret_val = mpm_mailbox_transport_write(transport_handle, (uint32_t)(intptr_t)mailbox_addr,(void *)mailbox_header, sizeof(mpm_mailbox_header_t));

    return(ret_val);
}

/**
 *  @brief Function mpm_mailbox_get_header() Read mailbox header to local buffer
 *  @param[in]     inst             Mailbox instance
 *  @param[in]     mailbox_header    Mailbox Header pointer
 *  @retval        0 for success, -1 for failure
 *  @pre
 *  @post
 */
int32_t mpm_mailbox_get_header(mpm_mailbox_inst_t *inst, mpm_mailbox_header_t *mailbox_header)
{
    void *transport_handle;
    int32_t ret_val;
    mpm_mailbox_header_t *mailbox_addr = (mpm_mailbox_header_t *)(intptr_t)inst->mem_start_addr;

    if(inst->mem_location == MPM_MAILBOX_MEMORY_LOCATION_REMOTE)
    {
      transport_handle = inst->transport_handle;
    } else
    {
      /* Indicates local copy */
      transport_handle = NULL;
    }
    ret_val = mpm_mailbox_transport_read(transport_handle, (uint32_t)(intptr_t)mailbox_addr,(void *)mailbox_header, sizeof(mpm_mailbox_header_t));

    return(ret_val);
}
/**
 *  @brief Function mpm_mailbox_put_write_index() Write write index into mailbox header
 *  @param[in]     inst             Mailbox instance
 *  @param[in]     write_index       Write index pointer
 *  @retval        0 for success, -1 for failure
 *  @pre
 *  @post
 */
int32_t mpm_mailbox_put_write_index(mpm_mailbox_inst_t *inst, uint32_t *write_index)
{
    void *transport_handle;
    mpm_mailbox_t *mailbox_addr = (mpm_mailbox_t *)(intptr_t)inst->mem_start_addr;
    int32_t ret_val;

    if(inst->mem_location == MPM_MAILBOX_MEMORY_LOCATION_REMOTE)
    {
      transport_handle = inst->transport_handle;
    } else
    {
      /* Indicates local copy */
      transport_handle = NULL;
    }
     /* Write back write index */
    ret_val = mpm_mailbox_transport_write(transport_handle, (uint32_t)(intptr_t)(&mailbox_addr->mailbox_header.write_index), (void *)write_index, sizeof(uint32_t));

    return(ret_val);
 }
/**
 *  @brief Function mpm_mailbox_get_slot_header() Get slot header from mailbox
 *  @param[in]     inst               Mailbox instance
 *  @param[in]     slot_index          Slot index
 *  @param[in]     slot_header  Slot header pointer
 *  @retval        0 for success, -1 for failure
 *  @pre
 *  @post
 */
int32_t mpm_mailbox_get_slot_header(mpm_mailbox_inst_t *inst, uint32_t slot_index, mpm_mailbox_slot_header_t *slot_header)
{
    void *transport_handle;
    int32_t ret_val;
    mpm_mailbox_t *mailbox_addr = (mpm_mailbox_t *)(intptr_t)inst->mem_start_addr;

    if(inst->mem_location == MPM_MAILBOX_MEMORY_LOCATION_REMOTE)
    {
      transport_handle = inst->transport_handle;
    } else
    {
      /* Indicates local copy */
      transport_handle = NULL;
    }
    ret_val = mpm_mailbox_transport_read(transport_handle, (((uint32_t)(intptr_t)&mailbox_addr->slots[0])
        +(slot_index*inst->slot_size)),
        (void *)slot_header, sizeof(mpm_mailbox_slot_header_t));

    return(ret_val);
}

/**
 *  @brief Function mpm_mailbox_put_slot_header() Put slot header to mailbox
 *  @param[in]     inst               Mailbox instance
 *  @param[in]     slot_index          Slot index
 *  @param[in]     slot_header  Slot header pointer
 *  @retval        0 for success, -1 for failure
 *  @pre
 *  @post
 */
int32_t mpm_mailbox_put_slot_header(mpm_mailbox_inst_t *inst, uint32_t slot_index,
  mpm_mailbox_slot_header_t *slot_header)
{
    void *transport_handle;
    int32_t ret_val;
    mpm_mailbox_t *mailbox_addr = (mpm_mailbox_t *)(intptr_t)inst->mem_start_addr;

    if(inst->mem_location == MPM_MAILBOX_MEMORY_LOCATION_REMOTE)
    {
      transport_handle = inst->transport_handle;
    } else
    {
      /* Indicates local copy */
      transport_handle = NULL;
    }
    ret_val = mpm_mailbox_transport_write(transport_handle, (uint32_t)(intptr_t)(((uint8_t *)&mailbox_addr->slots[0])
        +(slot_index*inst->slot_size)),
        (void *)slot_header, sizeof(mpm_mailbox_slot_header_t));

    return(ret_val);
}
/**
 *  @brief Function mpm_mailbox_get_payload() Get mailbox payload from slot
 *  @param[in]     inst               Mailbox instance
 *  @param[in]     slot_index          Slot index
 *  @param[in]     slot_header  Slot header pointer
 *  @param[in]     buf                Payload buffer
 *  @retval        0 for success, -1 for failure
 *  @pre
 *  @post
 */
int32_t mpm_mailbox_get_payload(mpm_mailbox_inst_t *inst, uint32_t slot_index, mpm_mailbox_slot_header_t *slot_header,
    uint8_t *buf)
{
    void *transport_handle;
    int32_t ret_val=0;
    mpm_mailbox_t *mailbox_addr = (mpm_mailbox_t *)(intptr_t)inst->mem_start_addr;

    if(inst->mem_location == MPM_MAILBOX_MEMORY_LOCATION_REMOTE)
    {
      transport_handle = inst->transport_handle;
    } else
    {
      /* Indicates local copy */
      transport_handle = NULL;
    }
    ret_val = mpm_mailbox_transport_read(transport_handle, (((uint32_t)(intptr_t)&mailbox_addr->slots[0])
        +(slot_index*inst->slot_size)+sizeof(mpm_mailbox_slot_header_t)),
        buf, slot_header->payload_size);

    return(ret_val);
}

/**
 *  @brief Function mpm_mailbox_put_payload() Put mailbox payload into slot
 *  @param[in]     inst               Mailbox instance
 *  @param[in]     slot_index          Slot index
 *  @param[in]     slot_header  Slot header pointer
 *  @param[in]     buf                Payload buffer
 *  @retval        0 for success, -1 for failure
 *  @pre
 *  @post
 */
int32_t mpm_mailbox_put_payload(mpm_mailbox_inst_t *inst, uint32_t slot_index, mpm_mailbox_slot_header_t *slot_header,
    uint8_t *buf)
{
    void *transport_handle;
    int32_t ret_val;
    mpm_mailbox_t *mailbox_addr = (mpm_mailbox_t *)(intptr_t)inst->mem_start_addr;
    if(inst->mem_location == MPM_MAILBOX_MEMORY_LOCATION_REMOTE)
    {
      transport_handle = inst->transport_handle;
    } else
    {
      /* Indicates local copy */
      transport_handle = NULL;
    }
    ret_val = mpm_mailbox_transport_write(transport_handle, (((uint32_t)(intptr_t)&mailbox_addr->slots[0])
        +(slot_index*inst->slot_size)+sizeof(mpm_mailbox_slot_header_t)),
        buf, slot_header->payload_size);

    return(ret_val);
}

/**
 *  @brief Function mpm_mailbox_alloc_slot() Allocate slot for writing mail
 *  @param[in]     inst               Mailbox instance
 *  @param[in]     slot_header  Slot header pointer
 *  @retval        slot index, -1 for failure
 *  @pre
 *  @post
 */
int32_t mpm_mailbox_alloc_slot(mpm_mailbox_inst_t *inst, mpm_mailbox_slot_header_t *slot_header)
{
    int32_t    slot_index;
    uint32_t slot_owner;
    mpm_mailbox_header_t mailbox_header;
    int32_t ret_val;

    slot_owner = MPM_MAILBOX_SLOT_OWNER_REMOTE;
#ifdef MPM_MAILBOX_VERBOSE
    printf("\t\t\t\t\tmpm_mailbox_alloc_slot() \n");
#endif
    ret_val = mpm_mailbox_get_header(inst, &mailbox_header);
    if(ret_val)
       return(ret_val);

    slot_index = mailbox_header.write_index;
    /* Check if the next mailbox is free */
      ret_val = mpm_mailbox_get_slot_header(inst, slot_index, slot_header);
      if(ret_val)
        return(MPM_MAILBOX_ERR_FAIL);
      if(slot_header->owner == slot_owner)
        return(MPM_MAILBOX_ERR_MAIL_BOX_FULL);

#ifdef MPM_MAILBOX_VERBOSE
    printf("\t\t\t\t\t\twrite_index: %2d, read_index: %2d  (%08X | DSP:%d, CORE:%d) \n",mailbox_header.write_index, \
        mailbox_header.read_index, ,MPM_MAILBOX_ID_2_REMOTE_DSP_ID(mpm_mailbox_id),MPM_MAILBOX_ID_2_CORE_ID(mpm_mailbox_id));
#endif
    return(slot_index);
}


/**
 *  @brief Function mpm_mailbox_free_slot() Free slot
 *  @param[in]     inst               Mailbox instance
 *  @param[in]     slot_header  previously read Slot header pointer
 *  @param[in]     slot_index          Slot index
 *  @retval        0 for success, -1 for failure
 *  @pre
 *  @post
 */
int32_t mpm_mailbox_free_slot(mpm_mailbox_inst_t *inst, mpm_mailbox_slot_header_t *slot_header_shadow, int32_t slot_index)
{
    uint32_t slot_owner;
    mpm_mailbox_header_t mailbox_header_shadow;
    mpm_mailbox_slot_header_t *slot_header_p;
    int32_t ret_val;
    void *transport_handle;
    mpm_mailbox_t *mailbox_addr = (mpm_mailbox_t *)(intptr_t)inst->mem_start_addr;

    slot_owner = MPM_MAILBOX_SLOT_OWNER_LOCAL;
    mailbox_header_shadow.read_index = slot_index;
    inst->read_counter++;
    mailbox_header_shadow.read_index++;
    if(mailbox_header_shadow.read_index >= inst->depth)
      mailbox_header_shadow.read_index=0;

    slot_header_shadow->owner = slot_owner;

    if(inst->mem_location == MPM_MAILBOX_MEMORY_LOCATION_REMOTE)
    {
      transport_handle = inst->transport_handle;
    } else
    {
      /* Indicates local copy */
      transport_handle = NULL;
    }
    /* Write back read counter */
    ret_val = mpm_mailbox_transport_write(transport_handle, (uint32_t)(intptr_t)(&mailbox_addr->mailbox_header.read_index),
        &mailbox_header_shadow.read_index, sizeof(uint32_t));
    if(ret_val)
      return(ret_val);
    slot_header_p = (mpm_mailbox_slot_header_t *)(((uint8_t *)(&mailbox_addr->slots[0]))+(slot_index*inst->slot_size));
    /* Free mailbox slot */
    ret_val = mpm_mailbox_transport_write(transport_handle,
        (uint32_t)(intptr_t)(&slot_header_p->owner),
        &slot_owner,sizeof(uint32_t));

    return(ret_val);
}


/**
 *  @brief Function mpm_mailbox_recv_slot() Get the mail slot to be read next
 *  @param[in]     inst               Mailbox instance
 *  @param[in]     slot_header  Slot header pointer
 *  @retval        slot index, -1 for failure
 *  @pre
 *  @post
 */
int32_t mpm_mailbox_recv_slot(mpm_mailbox_inst_t *inst, mpm_mailbox_slot_header_t *slot_header)
{
    int32_t slot_index;
    uint32_t slot_owner;
    mpm_mailbox_header_t mailbox_header;

    slot_owner = MPM_MAILBOX_SLOT_OWNER_LOCAL;

#ifdef MPM_MAILBOX_VERBOSE
    printf("\t\t\t\t\tmpm_mailbox_recv_slot(0x%08x) \n", mpm_mailbox_id);
#endif
    if(mpm_mailbox_get_header(inst, &mailbox_header) != 0)
       return(MPM_MAILBOX_READ_ERROR);
    slot_index = mailbox_header.read_index;

    mpm_mailbox_get_slot_header(inst, slot_index, slot_header);
    if(slot_header->owner == slot_owner)
    {
      return(MPM_MAILBOX_ERR_EMPTY);
    }

#ifdef MPM_MAILBOX_VERBOSE
    printf("\t\t\t\t\t\twriteCount: %2d, readCount: %2d  (%08X | DSP:%d, CORE:%d) \n",mailbox->write_index, \
           mailbox->read_index, mpm_mailbox_id,MPM_MAILBOX_ID_2_REMOTE_DSP_ID(mpm_mailbox_id),MPM_MAILBOX_ID_2_CORE_ID(mpm_mailbox_id));
#endif

    return(slot_index);
}
/**
 *  @brief Function mpm_mailbox_send_slot() Put slot into maibox
 *  @param[in]     inst               Mailbox instance
 *  @param[in]     slot_header  Slot header pointer
 *  @param[in]     buf                Payload buffer
 *  @param[in]     slot_index            Slot index
 *  @retval        slot index, -1 for failure
 *  @pre
 *  @post
 */
int32_t mpm_mailbox_send_slot(mpm_mailbox_inst_t *inst, mpm_mailbox_slot_header_t *slot_header,
  uint8_t *buf, int32_t slot_index)
{
    uint32_t slot_owner;
    mpm_mailbox_header_t mailbox_header;

    slot_owner = MPM_MAILBOX_SLOT_OWNER_REMOTE;

    mailbox_header.write_index= slot_index;
    inst->write_counter++;
    mailbox_header.write_index++;
    if(mailbox_header.write_index >= inst->depth)
      mailbox_header.write_index=0;
    slot_header->owner = slot_owner;
    mpm_mailbox_put_payload(inst, slot_index, slot_header, buf);
    mpm_mailbox_put_slot_header(inst, slot_index, slot_header);
    mpm_mailbox_put_write_index(inst,  &mailbox_header.write_index);
//    mpm_mailbox_PutSend(inst, mailbox, slot_index);
    mpm_mailbox_notify(inst);
    return 0;
}

/******************************************************************************/
/*  Public APIs                                                               */
/******************************************************************************/

/**
 *  @brief Function mpm_mailbox_get_size() Get size needed for Mailbox instance
 *  @retval        size in bytes needed for mailbox instance
 *  @pre
 *  @post
 */
uint32_t mpm_mailbox_get_alloc_size(void)
{
  return(sizeof(mpm_mailbox_inst_t));
}


/**
 *  @brief Function mpm_mailbox_get_mem_size() Get size needed for Mailbox memory
 *  @param[in]     max_payload_size   Maximum size of a mpm_mailbox_ message.
 *  @param[in]     mpm_mailbox_depth      Maximum number of messages that the 
 *                                    mpm_mailbox_ can hold at one time.
 *  @retval        size in bytes needed for mailbox memory
 *  @pre
 *  @post
 */
uint32_t mpm_mailbox_get_mem_size(uint32_t max_payload_size, uint32_t mpm_mailbox_depth)
{
    uint32_t slot_size, mem_size;

    slot_size = sizeof(mpm_mailbox_slot_header_t)+ max_payload_size;
    /* Adjust for alignment */
    slot_size = ((slot_size + (sizeof(uint32_t)-1))/sizeof(uint32_t)) * sizeof(uint32_t);

    mem_size = (slot_size * mpm_mailbox_depth) + sizeof(mpm_mailbox_header_t);

    return ( mem_size );
}

/**
 *  @brief Function mpm_mailbox_create() Creates a mpm_mailbox_
 *  @param[out]    mpm_mailbox_handle  Returned mailbox handle pointer
 *  @param[in]     remote_node_id    Node id of remote node
 *  @param[in]     mem_location   memory location local or remote
 *  @param[in]     direction      send or receive
 *  @param[in]     mpm_mailbox_config MailBox configuration
 *  @retval        0 for success, -1 for failure
 *  @pre   mpm_mailbox_ init should be executed before callint mpm_mailbox_create
 *  @post 
 */
int32_t mpm_mailbox_create(void *mpm_mailbox_handle, char *remote_slave_name,
  uint32_t mem_location, uint32_t direction, mpm_mailbox_config_t *mpm_mailbox_config)
{
    int32_t  i;
    uint32_t slot_owner, max_slots;
    mpm_mailbox_inst_t *inst = (mpm_mailbox_inst_t *)mpm_mailbox_handle;
    uint32_t slot_size;
    mpm_mailbox_header_t mailbox_header;
    mpm_mailbox_slot_header_t slot_header;
    int32_t ret_val;

   if(inst == NULL)
     return(-1);

   memset(mpm_mailbox_handle, 0, sizeof(mpm_mailbox_inst_t));

   /* Check sizes of structure */
    slot_size = sizeof(mpm_mailbox_slot_header_t)+ mpm_mailbox_config->max_payload_size;
    /* Adjust for alignment */
    slot_size = ((slot_size + (sizeof(uint32_t)-1))/sizeof(uint32_t)) * sizeof(uint32_t);
    inst->depth = (mpm_mailbox_config->mem_size - sizeof(mpm_mailbox_header_t))/slot_size;
    inst->mem_size = mpm_mailbox_config->mem_size;
    /* Store variables to mpm_mailbox_inst */
    inst->slot_size = slot_size;
    inst->max_payload_size  = mpm_mailbox_config->max_payload_size;
    
    inst->mem_location = mem_location;
    if ( inst->mem_location == MPM_MAILBOX_MEMORY_LOCATION_REMOTE )
    {
      if ( !remote_slave_name || strlen(remote_slave_name) == 0 )
        return -1;

      inst->transport_handle = mpm_mailbox_transport_open(remote_slave_name);

      if ( inst->transport_handle == NULL )
        return -1;
    }
    inst->mem_start_addr = mpm_mailbox_config->mem_start_addr;

    inst->direction = direction;

#ifdef MPM_MAILBOX_VERBOSE
    printf("mpm_mailbox_create(): remote_node = 0x%08X\n",remote_node);
#endif
    /* zero headers */
    memset(&mailbox_header, 0, sizeof(mpm_mailbox_header_t) );
    memset(&slot_header, 0, sizeof(mpm_mailbox_slot_header_t) );

    if (direction == MPM_MAILBOX_DIRECTION_RECEIVE) {

      slot_owner = MPM_MAILBOX_SLOT_OWNER_LOCAL;

      max_slots = inst->depth;

      for ( i=0; i<max_slots; i++) {
        slot_header.owner = slot_owner;
        ret_val = mpm_mailbox_put_slot_header(inst, i, &slot_header);
        if(ret_val)
          return(ret_val);
      }
      mailbox_header.owner_code = slot_owner;
      ret_val = mpm_mailbox_put_header(inst, &mailbox_header);
      if(ret_val)
        return(ret_val);
    }
    return 0;
}

/**
 *  @brief Function mpm_mailbox_open() Opens a mpm_mailbox_; This is a blocking call, wait till the remote is ready
 *  @param[in]     mpm_mailbox_handle  mpm_mailbox_ Handle
 *  @retval        0 for success, -1 for failure
 *  @pre  
 *  @post 
 */
int32_t mpm_mailbox_open(void *mpm_mailbox_handle)
{
    mpm_mailbox_header_t mailbox_header;
    
    if ( mpm_mailbox_handle == NULL)
        return -1; 

    do {
        if(mpm_mailbox_get_header((mpm_mailbox_inst_t *)mpm_mailbox_handle, &mailbox_header) != 0)
          return(-1);
    } while(mailbox_header.owner_code != MPM_MAILBOX_SLOT_OWNER_LOCAL);
    return (0);
}

/**
 *  @brief Function mpm_mailbox_write() Writes into a mpm_mailbox_ to deliver to remote
 *  @param[in]     mpm_mailbox_handle  mpm_mailbox_ Handle
 *  @param[in]     *buf          Mailbox Payload buffer pointer
 *  @param[in]     size          Mailbox Payload buffer size
 *  @param[in]     trans_id      transaction ID for the mail
 *  @retval        0 for success, -1 for failure
 *  @pre  
 *  @post 
 */
int32_t mpm_mailbox_write (void *mpm_mailbox_handle, uint8_t *buf, uint32_t size, uint32_t trans_id)
{
    int32_t slot_index;
    mpm_mailbox_inst_t *inst = (mpm_mailbox_inst_t *)mpm_mailbox_handle;
    mpm_mailbox_slot_header_t slot_header;
    
    if(size > inst->max_payload_size)
        return -1;

    slot_index = mpm_mailbox_alloc_slot(inst, &slot_header);
    if(slot_index < 0)
      return(slot_index);

    slot_header.payload_size = size;
    slot_header.trans_id = trans_id;

    mpm_mailbox_send_slot(mpm_mailbox_handle, &slot_header, buf, slot_index);

    return 0;
}

/**
 *  @brief Function mpm_mailbox_read() Reads from a mpm_mailbox_. This is a blocking call
 *  @param[in]     mpm_mailbox_id    Unique ID of the mpm_mailbox_
 *  @param[in]     *buf          Mailbox Payload buffer pointer
 *  @param[in]     *size         Mailbox Payload buffer size
 *  @param[in]     *trans_id     transaction ID for the mail
 *  @retval        0 for success, -1 for failure
 *  @pre  
 *  @post 
 */
int32_t mpm_mailbox_read (void *mpm_mailbox_handle, uint8_t *buf, uint32_t *size, uint32_t *trans_id)
{
    int32_t slot_index;
    mpm_mailbox_slot_header_t slot_header;
    mpm_mailbox_inst_t *inst= (mpm_mailbox_inst_t *)mpm_mailbox_handle;
    int32_t ret_val;

    slot_index = mpm_mailbox_recv_slot(inst, &slot_header); /* blocking call */

    if(slot_index < 0)
    {
      return(slot_index);
    }

    /* Get payload from slot */
    mpm_mailbox_get_payload(inst, slot_index, &slot_header, buf);
    *size = slot_header.payload_size;
    *trans_id = slot_header.trans_id;
    /* Free the slot read */
    ret_val = mpm_mailbox_free_slot(mpm_mailbox_handle,&slot_header,slot_index);
    if(ret_val)
      return(MPM_MAILBOX_ERR_FAIL);

    return 0;
}


/**
 *  @brief Function mpm_mailbox_query() Polls mpm_mailbox_es for any available messages to read. Non-blocking
 *  @param[in]     mpm_mailbox_handle  mpm_mailbox_ Handle
 *  @retval        Number of messages in mailbox; negative error on failure
 *  @pre  
 *  @post 
 */
int32_t mpm_mailbox_query (void *mpm_mailbox_handle)
{

    uint32_t    mpm_mailbox_cnt;
    mpm_mailbox_inst_t *inst = (mpm_mailbox_inst_t *)mpm_mailbox_handle;
    mpm_mailbox_header_t mailbox_header;
    uint32_t read_index, write_index;
    mpm_mailbox_slot_header_t slot_header;
    int32_t ret_val;

    if(mpm_mailbox_get_header(inst, &mailbox_header)!= 0)
      return(-1);

    read_index = mailbox_header.read_index;
    write_index = mailbox_header.write_index;
    if(read_index > write_index)
    {
      mpm_mailbox_cnt = (write_index-read_index)+inst->depth;
    } else
    {
      mpm_mailbox_cnt = (write_index - read_index);
    }

    /* If this actually is the mailbox full case */
    if(!mpm_mailbox_cnt)
    {
      uint32_t prev_read_slot_idx;

      prev_read_slot_idx = (mailbox_header.read_index == 0) ? (inst->depth - 1) : (mailbox_header.read_index - 1);

      /* Check the slot at read index */
      ret_val = mpm_mailbox_get_slot_header(inst, prev_read_slot_idx, &slot_header);
      if(ret_val)
        return(MPM_MAILBOX_ERR_FAIL);

      if(slot_header.owner==MPM_MAILBOX_SLOT_OWNER_REMOTE)
        mpm_mailbox_cnt = inst->depth;
    }

    return mpm_mailbox_cnt;
}

/* nothing past this point */
