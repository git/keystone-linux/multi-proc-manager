#!/bin/bash
set -e
set -o pipefail

VERSION_NAME=3_00_01_00
# Version number can be overriden by script
if [ ! -z MPM_VERSION_NAME ]; then
VERSION_NAME=$MPM_VERSION_NAME
fi
VERSION=${VERSION_NAME//_/.}


cd $( dirname $0 )

[ ! -d ./build ] || rm -rf build
[ ! -d ./artifacts ] || rm -rf artifacts*

mkdir -pv {build,artifacts}
mkdir -pv artifacts/{output,logs}

echo "mpm_${VERSION}.orig.tar.gz" > ./artifacts/build_targets
echo "mpm_${VERSION}.debian.tar.gz" > ./artifacts/build_targets

cd ..

make clean 2>&1 | tee ./mkrel/artifacts/logs/make.log
make all c66x 2>&1 | tee -a ./mkrel/artifacts/logs/make.log
make test  test_c66x 2>&1 | tee ./mkrel/artifacts/logs/make_test.log

tar -czvf ./mkrel/build/mpm.tar.gz --files-from=filelist.txt 2>&1 | tee ./mkrel/artifacts/logs/package.log

cd ./mkrel/build

mkdir -p ./mpm_${VERSION_NAME}
cd ./mpm_${VERSION_NAME}

tar -xvf ../mpm.tar.gz

tar -czvf ../../artifacts/output/mpm_${VERSION}.debian.tar.gz ./debian
rm -rf ./debian

cd ..

tar -czvf ../artifacts/output/mpm_${VERSION}.orig.tar.gz mpm_${VERSION_NAME}

cd ..

tar -czvf artifacts.tgz artifacts/


